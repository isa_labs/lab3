library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use std.textio.all;
use ieee.std_logic_textio.all;


-- Instruction memory for DLX
-- Memory filled by a process which reads from a file
-- file name is "test.asm.mem"
entity IRAM is
	generic(RAM_DEPTH : integer := 48;
					I_SIZE : integer := 32);
	port (Rst  : in  std_logic;
				Addr : in  std_logic_vector(I_SIZE - 1 downto 0);
				Dout : out std_logic_vector( I_SIZE - 1 downto 0));
end IRAM;

architecture IRam_Bhe of IRAM is

	type RAMtype is array (0 to RAM_DEPTH - 1) of integer;-- std_logic_vector(I_SIZE - 1 downto 0);

	signal IRAM_mem : RAMtype;

begin  -- IRam_Bhe

	output_connections: for byte in 0 to I_SIZE/8 - 1 generate
		Dout( 8*(byte+1)-1 downto 8*byte ) <= conv_std_logic_vector(IRAM_mem(conv_integer(unsigned(Addr)) + byte),8);
	end generate;

	-- purpose: This process is in charge of filling the Instruction RAM with the firmware
	-- type   : combinational
	-- inputs : Rst
	-- outputs: IRAM_mem
	FILL_MEM_P: process (Rst)
		file mem_fp: text;
		variable file_line : line;
		variable index : integer := 0;
		variable tmp_data_u : std_logic_vector( I_SIZE-1 downto 0);
	begin  -- process FILL_MEM_P
		if (Rst = '0') then
		 file_open(mem_fp,"iram.mem",READ_MODE);
		 while (not endfile(mem_fp)) loop
				readline(mem_fp,file_line);
				hread(file_line,tmp_data_u);
				if index < RAM_DEPTH then
					for j in 0 to I_SIZE/8 - 1 loop
						IRAM_mem(index + j) <= conv_integer(unsigned(tmp_data_u( 8*(j+1)-1 downto 8*j )));
					end loop;   
					index := index + 4;
				end if;
			end loop;
		end if;
	end process FILL_MEM_P;

end IRam_Bhe;
