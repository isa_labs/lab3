library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity dataMemory is
	generic(DMEM_DEPTH : integer := 48;
					DATA_SIZE : integer := 64;
					ADD_SIZE  : integer := 64);
	port (Rst  : in  std_logic;
				Addr : in std_logic_vector( ADD_SIZE-1 downto 0);
				Din : in  std_logic_vector( DATA_SIZE-1 downto 0);
				read_en : in std_logic;
				write_en : in std_logic;
				Dout : out std_logic_vector( DATA_SIZE-1 downto 0));
end dataMemory;

architecture arch of dataMemory is

	type DMEMORYtype is array (0 to DMEM_DEPTH - 1) of integer;

	signal DMEM_memory : DMEMORYtype;

begin  

	FILL_MEM_P: process (Rst,Din,Addr,read_en,write_en)
		file mem_fp: text;
		variable file_line : line;
		variable index : integer := 0;
		variable tmp_data_u : std_logic_vector( DATA_SIZE-1 downto 0);
	begin  -- process FILL_MEM_P
		if (Rst = '0') then
			file_open(mem_fp,"dmem.txt",READ_MODE);
		-- file_open(mem_fp,"C:\Users\JURGEN\Desktop\gitlab_dlx\dlx-project\simulation\dmem.txt",READ_MODE);
			while (not endfile(mem_fp)) loop
				readline(mem_fp,file_line);
				hread(file_line,tmp_data_u);
				if index < DMEM_DEPTH then
					for j in 0 to DATA_SIZE/8 - 1 loop
						DMEM_memory(index + j) <= to_integer(unsigned(tmp_data_u( 8*(j+1)-1 downto 8*j  )));
					end loop;   
					index := index + 8;
				end if;
			end loop;
		elsif (read_en = '1') then
			for j in 0 to DATA_SIZE/8 - 1 loop
				Dout( 8*(j+1) - 1 downto 8*j ) <= std_logic_vector(to_signed(DMEM_memory(to_integer(unsigned(Addr)) + j),8));
			end loop;
		elsif (write_en = '1') then
			for j in 0 to DATA_SIZE/8 - 1 loop
				DMEM_memory(to_integer(unsigned(Addr)) + j) <= to_integer(unsigned(Din( 8*(j+1)-1 downto 8*j  )));
			end loop;
		end if;	
	end process FILL_MEM_P;

end arch;
