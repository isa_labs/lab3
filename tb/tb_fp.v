//`timescale 1ns

module tb_fp();

   wire CLK_i;
   wire write_en_i;
   wire read_en_i;
   wire [63:0] dram_addr_i;
   wire [63:0] dram_read_i;
   wire [63:0] dram_write_i;
   wire [31:0] iram_addr_i;
   wire [31:0] iram_in_i;
   wire RST_n_i;
  
   clk_gen CG (
      //.END_SIM(END_SIM_i),
      .CLK(CLK_i),
      .RST_n(RST_n_i)
   );

   dataMemory  #( 
      .DMEM_DEPTH(48),
      .DATA_SIZE(64),
      .ADD_SIZE(64)
   ) DR (
      .Rst(RST_n_i),
      .Addr(dram_addr_i),
      .Din(dram_write_i),
      .read_en(read_en_i),
      .write_en(write_en_i),
      .Dout(dram_read_i)
   );

   IRAM #(
      .RAM_DEPTH(64),
      .I_SIZE(32)
   ) IR (
      .Rst(RST_n_i),
      .Addr(iram_addr_i),
      .Dout(iram_in_i)
   );

   DLX #(
      .DMEM_DEPTH(48),
      .DATA_SIZE(64),
      .IRAM_depth(64),
      .INSTRUCTION_SIZE(32),
      .ADD_SIZE_IRAM(32),
      .ADD_SIZE_DRAM(64),
      .opcode_size(7),
      .funct3_size(3),
      .funct7_size(7),
      .InstrType_size(8),
      .regsize(64),
      .regdepth(32),
      .alu_opcode_size(4)
   ) UUT (
      .clk(CLK_i),
      .rst(RST_n),
      .dram_read(dram_read_i),
      .iram_in(iram_in_i),
      .read_en(read_en_i),
      .write_en(write_en_i),
      .dram_addr(dram_addr_i),
      .dram_write(dram_write_i),
      .iram_addr(iram_addr_i)
   );

endmodule
