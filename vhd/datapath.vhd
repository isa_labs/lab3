library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use WORK.myTypes.all;

entity datapath is  -- datatpath of DLX without memory, memory connected exernally
	generic        (regsize : integer := 64;
					regdepth : integer := 32;
					ir_size: integer := 32;
					opcode_length : integer := 7;
					alu_opcode_length : integer := 4;
					funct3_length : integer := 3;
					funct7_length : integer := 7;
					IRAM_depth : integer := 64;
					ADD_SIZE  : integer := 32);
	 
	port (clk : in std_logic;
	
				-- PIPELINE REGS RESET CONTROL SIGNAL INPUTS(ACTIVE LOW)
				PC_rst : in std_logic;
				f_d_rst	: in std_logic;  
				d_e_rst	: in std_logic; 
				e_m_rst	: in std_logic; 
				m_wb_rst : in std_logic; 
				
				-- PIPELINE REGS ENABLE CONTROL SIGNAL INPUTS
				PC_en : in std_logic; 
				f_d_en : in std_logic;
				d_e_en : in std_logic; 
				e_m_en : in std_logic; 
				m_wb_en	: in std_logic;

				-- fetch inputs
				if_mux_1 : in std_logic;
				in_F_D_IR : in std_logic_vector(ir_size-1 downto 0); -- from external IRAM
				
			    -- fetch outputs
				  out_pc : out std_logic_vector(ir_size-1 downto 0); --input of Instr Ram
			      
				-- decode inputs
				RF_en : in std_logic; -- reg file enable
				RF_rst : in std_logic;  
				RF_read1 : in std_logic; -- 1 if read at port 1 of the register file
				RF_read2 : in std_logic; -- 1 if read at port 2 of the register file
				SEL : in std_logic_vector(2 downto 0);
				zero_mux_1: in std_logic_vector(2 downto 0);
				zero_mux_2: in std_logic_vector(2 downto 0);
				
				-- execute inputs
				ex_mux : in std_logic;
				ALU_mux_top : in std_logic_vector(2 downto 0); 
				ALU_mux_bottom : in std_logic_vector(2 downto 0); 
				lui_auipc_mux: in std_logic_vector(1 downto 0); 
				ALU_opcode : in std_logic_vector(alu_opcode_length - 1 downto 0);
				ALU_rst : in std_logic;
				
				-- memory stage inputs
				mem_mux : in std_logic_vector(1 downto 0); -- for forwarding  
			    
				IN_LMD : in std_logic_vector(regsize - 1 downto 0); -- the output of the external data memory 
				data_in_mem : out std_logic_vector(regsize - 1 downto 0); --inputs of the data memory
				add_in_mem : out std_logic_vector( regsize - 1 downto 0);
			    
				
				-- write back inputs
				wb_data_mux : in std_logic;
				wb_jal_data_mux : in std_logic;
				 RF_write : in std_logic; -- 1 if write the register file
				 
				-- Control signals outputs
				
				zero : out std_logic; -- zero of the zero comparator in the decode stage
		
				instruction_opcode : out std_logic_vector( opcode_length-1 downto 0); -- output opcode of IRAM
				IF_ID_opcode : out std_logic_vector( opcode_length-1 downto 0); -- from the IF/ID instruction register
				ID_EX_opcode : out std_logic_vector( opcode_length-1 downto 0); -- from the ID/EX instruction register
				ID_EX_funct3	 : out std_logic_vector( funct3_length-1 downto 0);  -- from the ID/EX instruction register
                 ID_EX_funct7	 : out std_logic_vector( funct7_length-1 downto 0);
				 
				EM_Rd3_eq_FD_Rs1	: out std_logic; -- from comp_forwarding
				MW_Rd1_eq_FD_Rs1	: out std_logic;
				MW_Rd3_eq_FD_Rs1	: out std_logic;
				EM_Rd2_eq_FD_Rs1 	: out std_logic;
				MW_Rd2_eq_FD_Rs1	: out std_logic;
				MW_Rd3_eq_EM_Rs2	: out std_logic;
				MW_Rd2_eq_EM_Rs2  : out std_logic;
				EM_Rd3_eq_DE_Rs1	: out std_logic; --
				EM_Rd3_eq_DE_Rs2	: out std_logic; 
				MW_Rd3_eq_DE_Rs1	: out std_logic;
				MW_Rd3_eq_DE_Rs2	: out std_logic;
				EM_Rd2_eq_DE_Rs1	: out std_logic;
				EM_Rd2_eq_DE_Rs2	: out std_logic;
				MW_Rd2_eq_DE_Rs1	: out std_logic;
				MW_Rd2_eq_DE_Rs2	: out std_logic;
				MW_Rd3_eq_EM_Rs1	: out std_logic;
				MW_Rd2_eq_EM_Rs1  : out std_logic;
				MW_Rd1_eq_EM_Rs1	: out std_logic; -- Rd1 of jump register or jump and link register 
				EM_Rd3_eq_FD_Rs2 : out std_logic; 
				EM_Rd2_eq_FD_Rs2 : out std_logic;
				MW_Rd1_eq_FD_Rs2 : out std_logic; 
				MW_Rd3_eq_FD_Rs2 : out std_logic;
				MW_Rd2_eq_FD_Rs2 : out std_logic;
				DE_Rd3_eq_FD_Rs1: out std_logic;
				DE_Rd3_eq_FD_Rs2: out std_logic;

				rd2_ID_EX_eq_rs1_IF_ID:	out std_logic; -- from comp_stall
				rd2_ID_EX_eq_rs2_IF_ID:	out std_logic;
				rd2_ID_EX_eq_rs1_IR: out std_logic;
				rd2_IF_ID_eq_rs1_IR: out std_logic;
				rd3_IF_ID_eq_rs1_IR: out std_logic);
end datapath;

architecture struct of datapath is 

	component fetch_stage is
	generic(N : integer := 32;
					IRAM_depth : integer := 64);
	port (clk : in std_logic;
				PC_rst : in std_logic;
				PC_en : in std_logic;
                ADD_CALCULATED_PC : in std_logic_vector(N-1 downto 0);
				if_mux_1 : in std_logic;

                PC: out std_logic_vector(2*N-1 downto 0);
				PC_plus_four : out std_logic_vector(N-1 downto 0);
				out_PC  :	out std_logic_vector(N-1 downto 0));  --goes into the input of the Instruction Ram	
				
	end component;
	
	component if_id_pipe is
	generic(regsize : integer := 32;
				 	opcode_size : integer := 7);
	port (in_f_d_PC : in std_logic_vector(2*regsize-1 downto 0);
		 in_f_d_PC_plus_four : in std_logic_vector(regsize-1 downto 0); -- direct output from the fetch adder
				in_f_d_IR : in std_logic_vector(regsize - 1 downto 0);
				clk : in std_logic;
				f_d_rst : in std_logic;
				f_d_en : in std_logic;

				out_f_d_PC : in std_logic_vector(2*regsize-1 downto 0);
		        out_f_d_PC_plus_four : in std_logic_vector(regsize-1 downto 0); -- direct output from the fetch adder
				out_f_d_IR : in std_logic_vector(regsize - 1 downto 0);
				out_f_d_opcode : out std_logic_vector(opcode_size - 1 downto 0));
	end component;

	component  decode_stage is
		generic(regsize : integer := 32;
						regdepth : integer := 32);
		port (clk : in std_logic;

					OUT_f_d_PC : in std_logic_vector(2*regsize - 1 downto 0);
					OUT_f_d_PC_plus_four : in std_logic_vector(regsize - 1 downto 0);
					OUT_f_d_IR : in std_logic_vector(regsize -1 downto 0);

					zero_mux_1 : in std_logic_vector(2 downto 0);  --for forwarding
					zero_mux_2 : in std_logic_vector(2 downto 0);  --for forwarding
					EX_MEM_ALU_OUT : in std_logic_vector(2*regsize-1 downto 0);
					MEM_WB_ALU_OUT : in std_logic_vector(2*regsize-1 downto 0);
					MEM_WB_LMD : in std_logic_vector(2*regsize-1 downto 0);
					ID_EX_jal_out : in std_logic_vector(2*regsize-1 downto 0);
                    EX_MEM_jal_out :in std_logic_vector(2*regsize-1 downto 0);
					MEM_WB_jal_out : in std_logic_vector(2*regsize-1 downto 0);
					
					SEL : in std_logic_vector(2 downto 0);    --for sign extension
					
					RF_rd1 : in std_logic;
					RF_rd2 : in std_logic;
					RF_wr : in std_logic;  --register file control signals
					RF_en : in std_logic;
					RF_rst : in std_logic;
					IN_data_RF : in std_logic_vector(2*regsize - 1 downto 0);
					IN_add_wr_RF : in std_logic_vector(log2(regsize) - 1 downto 0);

					zero : out std_logic;
					data_in_A : out std_logic_vector(2*regsize - 1 downto 0);
					data_in_B : out std_logic_vector(2*regsize - 1 downto 0);
					data_in_Imm : out std_logic_vector(2*regsize - 1 downto 0);
					IN_d_e_IR : out std_logic_vector(regsize - 1 downto 0);
					in_ID_EX_jal : out std_logic_vector(2*regsize - 1 downto 0);
					in_d_e_PC : out std_logic_vector(2*regsize - 1 downto 0);
					ADD_CALCULATED_PC: out std_logic_vector(regsize - 1 downto 0));
 	end component;
	
	component id_ex_pipe is
	generic(regsize : integer := 32;
					opcode_size : integer := 7;
					funct3_size : integer := 3;
					funct7_size : integer := 7);
					
	port (
				in_d_e_IR : in std_logic_vector(regsize - 1 downto 0);
				IN_REG_A : in std_logic_vector(2*regsize-1 downto 0);
				IN_REG_B : in std_logic_vector(2*regsize-1 downto 0);
				IN_REG_IMM : in std_logic_vector(2*regsize-1 downto 0);
				in_d_e_PC: in std_logic_vector(2*regsize-1 downto 0);
				in_d_e_jal: in std_logic_vector(2*regsize-1 downto 0);
				
				clk : in std_logic;
				d_e_rst : in std_logic;
				d_e_en : in std_logic;

                out_d_e_PC: out std_logic_vector(2*regsize-1 downto 0);
				out_d_e_jal: out std_logic_vector(2*regsize-1 downto 0);
				out_d_e_IR : out std_logic_vector( regsize - 1 downto 0);
				OUT_REG_A : out std_logic_vector(2*regsize-1 downto 0);
				OUT_REG_B : out std_logic_vector(2*regsize-1 downto 0);
				OUT_REG_IMM : out std_logic_vector(2*regsize-1 downto 0);
				out_id_ex_opcode : out std_logic_vector( opcode_size -1 downto 0);
				out_id_ex_funct3 : out std_logic_vector( funct3_size -1 downto 0);
				out_id_ex_funct7 : out std_logic_vector( funct7_size -1 downto 0)
				);
	end component;

 	component execute_stage is
	generic(regsize : integer := 32;
					opcode_length : integer := 7);
	port (     ID_EX_jal_out : in std_logic_vector(2*regsize - 1 downto 0);
	            ID_EX_PC_out : in std_logic_vector(2*regsize - 1 downto 0);
				OUT_d_e_IR : in std_logic_vector( regsize -1 downto 0);
				out_A : in std_logic_vector(2*regsize -1 downto 0);
				out_B : in std_logic_vector(2*regsize -1 downto 0);
				out_Imm : in std_logic_vector(2*regsize -1 downto 0);

				EX_MEM_ALU_OUT : in std_logic_vector(regsize-1 downto 0);
				MEM_WB_ALU_OUT : in std_logic_vector(regsize-1 downto 0);
				MEM_WB_LMD : in std_logic_vector(regsize-1 downto 0);
                EX_MEM_jal_out :in std_logic_vector(2*regsize-1 downto 0);
				MEM_WB_jal_out : in std_logic_vector(2*regsize-1 downto 0);
				
				ALU_opcode : in std_logic_vector(opcode_length -1 downto 0);
				ALU_rst : in std_logic;

				ex_mux : in std_logic;
				ALU_mux_bottom : in std_logic_vector(2 downto 0);
				ALU_mux_top : in std_logic_vector(2 downto 0);
				lui_auipc_mux : in std_logic_vector(1 downto 0);

				IN_ex_mem_jal : out std_logic_vector(2*regsize - 1 downto 0);
				IN_e_m_IR : out std_logic_vector(regsize -1 downto 0);
				out_ALU : out std_logic_vector(2*regsize - 1 downto 0);
				in_e_m_B : out std_logic_vector(2*regsize - 1 downto 0));
	end component;
	
	component ex_mem_pipe is
	generic(regsize : integer := 32);
	port (
				in_e_m_IR : in std_logic_vector(regsize - 1 downto 0);
		 		IN_REG_ALUOUT : in std_logic_vector(2*regsize-1 downto 0);
		 		IN_REG_B_EX_MEM : in std_logic_vector(2*regsize-1 downto 0);
				in_ex_mem_jal : in std_logic_vector(2*regsize-1 downto 0);

				clk : in std_logic;
				e_m_rst : in std_logic;
				e_m_en : in std_logic;

				out_e_m_IR : out std_logic_vector( regsize - 1 downto 0);
				OUT_REG_ALUOUT : out std_logic_vector(2*regsize-1 downto 0);
				OUT_REG_B_EX_MEM : out std_logic_vector(2*regsize-1 downto 0);
				EX_MEM_jal_out : in std_logic_vector(2*regsize-1 downto 0)
				);
	end component;

	component memory_stage is
	generic(regsize : integer := 32);
	port (
				OUT_e_m_IR : in std_logic_vector( regsize - 1 downto 0);
				out_ALUOUT : in std_logic_vector( 2*regsize - 1 downto 0);
				out_e_m_B : in std_logic_vector( 2*regsize - 1 downto 0);
				EX_MEM_jal_out :in std_logic_vector(2*regsize-1 downto 0);
				
                MEM_WB_ALU_OUT : in std_logic_vector(regsize - 1 downto 0);
				MEM_WB_LMD : in std_logic_vector(regsize-1 downto 0);
				MEM_WB_jal_out : in std_logic_vector(2*regsize-1 downto 0);

				mem_mux : in std_logic_vector(1 downto 0);
               
			   IN_m_wb_IR : out std_logic_vector( regsize - 1 downto 0);
			     IN_m_wb_jal : out std_logic_vector( 2*regsize - 1 downto 0);
				 in_m_wb_aluout : out std_logic_vector( 2*regsize - 1 downto 0);
				data_in_mem : out std_logic_vector(2*regsize - 1 downto 0);
				add_in_mem : out std_logic_vector( 2*regsize - 1 downto 0));
	end component;
	
	component mem_wb_pipe is
	generic(regsize : integer := 32);
	port (in_m_wb_jal : in std_logic_vector(2*regsize-1 downto 0);
				in_m_wb_IR : in std_logic_vector(regsize - 1 downto 0);
				IN_ALUOUT_M_WB : in std_logic_vector(2*regsize-1 downto 0);
				IN_LMD : in std_logic_vector(2*regsize-1 downto 0);
				
				clk : in std_logic;
				m_WB_rst : in std_logic;
				m_wb_en : in std_logic;

				MEM_WB_jal_out : out std_logic_vector(2*regsize-1 downto 0);
				out_m_wb_IR : out std_logic_vector( regsize - 1 downto 0);
				OUT_ALUOUT_M_WB : out std_logic_vector(regsize-1 downto 0);
				OUT_LMD : out std_logic_vector(regsize-1 downto 0));
	end component;

	component writeBack_stage is
	generic(regsize : integer := 32;
					regdepth : integer := 32);
	port (MEM_WB_jal_out : in std_logic_vector(2*regsize - 1 downto 0);
				OUT_m_wb_IR : in std_logic_vector( regsize -1 downto 0);
				OUT_m_wb_ALUOUT : in std_logic_vector(2*regsize - 1 downto 0);
				OUT_m_wb_LMD : in std_logic_vector(2*regsize - 1 downto 0);

				wb_data_mux : in std_logic;
				wb_jal_data_mux : in std_logic;
			
				out_wb_data_mux : out std_logic_vector(regsize - 1 downto 0);
				out_wb_add_mux : out std_logic_vector(log2(regdepth)-1 downto 0));
	end component;

	
	component comp_forwarding is
	generic(N : integer := 32;
					add_size : integer := 5);
	port (f_d_IR : in std_logic_vector(0 to N-1);
				d_e_IR : in std_logic_vector( N-1 downto 0);
				e_m_IR : in std_logic_vector(N-1 downto 0);
				m_w_IR : in std_logic_vector(N-1 downto 0);

				EM_Rd3_eq_FD_Rs1 : out std_logic;
				MW_Rd1_eq_FD_Rs1 : out std_logic;
				MW_Rd3_eq_FD_Rs1 : out std_logic;
				EM_Rd2_eq_FD_Rs1 : out std_logic;
				MW_Rd2_eq_FD_Rs1 : out std_logic;
				MW_Rd3_eq_EM_Rs2 : out std_logic;
				MW_Rd2_eq_EM_Rs2 : out std_logic;
				EM_Rd3_eq_DE_Rs1 : out std_logic; -- 1 if ex_mem_IR11..7 = id_ex_IR19..15
				EM_Rd3_eq_DE_Rs2 : out std_logic; -- 1 if ex_mem_IR11..7 = id_ex_IR24..20
				MW_Rd3_eq_DE_Rs1 : out std_logic;
				MW_Rd3_eq_DE_Rs2 : out std_logic;
				EM_Rd2_eq_DE_Rs1 : out std_logic;
				EM_Rd2_eq_DE_Rs2 : out std_logic;
				MW_Rd2_eq_DE_Rs1 : out std_logic;
				MW_Rd2_eq_DE_Rs2 : out std_logic;
				MW_Rd3_eq_EM_Rs1 : out std_logic;
				MW_Rd2_eq_EM_Rs1 : out std_logic;
				MW_Rd1_eq_EM_Rs1 : out std_logic; 
				
				EM_Rd3_eq_FD_Rs2 : out std_logic; 
				EM_Rd2_eq_FD_Rs2 : out std_logic;
				MW_Rd1_eq_FD_Rs2 : out std_logic; 
				MW_Rd3_eq_FD_Rs2 : out std_logic;
				MW_Rd2_eq_FD_Rs2 : out std_logic;
				DE_Rd3_eq_FD_Rs1: out std_logic;
				DE_Rd3_eq_FD_Rs2: out std_logic);
	end component;   
																		 
	component comp_stall is
		generic(N : integer := 32;
						add_size : integer := 5);
		port (IRAM_output : in std_logic_vector(0 to N-1);
					f_d_IR : in std_logic_vector(0 to N-1);
					d_e_IR : in std_logic_vector(0 to N-1);
					
					rd2_ID_EX_eq_rs1_IF_ID : out std_logic; -- output of comparators 
					rd2_ID_EX_eq_rs2_IF_ID : out std_logic;
					rd2_ID_EX_eq_rs1_IR : out std_logic;
					rd2_IF_ID_eq_rs1_IR : out std_logic;
					rd3_IF_ID_eq_rs1_IR : out std_logic);
	end component;                         

	-- fetch signals
	
	signal ADD_CALCULATED_PC_sig : std_logic_vector(regsize-1 downto 0);  -- = out_adder_address 
	signal in_f_d_PC_sig :  std_logic_vector(regsize-1 downto 0);
	signal in_f_d_PC_plus_four_sig :  std_logic_vector(ir_size-1 downto 0); -- direct output from the fetch adder
	signal out_PC_sig  : std_logic_vector(ir_size-1 downto 0);  --goes into the input of the Instruction Ram	
																					
	-- fetch decode pipeline registers signals
	     signal out_Iram_sig : std_logic_vector(ir_size - 1 downto 0); --signal to connect the output of the Iram with the input of the if_id_IR
	
	  signal out_f_d_IR_sig : std_logic_vector(ir_size - 1 downto 0);
	  signal out_f_d_PC_sig :  std_logic_vector(regsize-1 downto 0);
	signal    out_f_d_PC_plus_four_sig :  std_logic_vector(ir_size-1 downto 0); -- direct output from the fetch adder
    signal	out_f_d_opcode_sig :  std_logic_vector(opcode_length - 1 downto 0);
	
	
	
	-- decode signals
	
		 signal	 EX_MEM_ALU_OUT_sig : std_logic_vector(regsize-1 downto 0);
		 signal	 MEM_WB_ALU_OUT_sig :  std_logic_vector(regsize-1 downto 0);
		 signal	 MEM_WB_LMD_sig : std_logic_vector(regsize-1 downto 0);
		 signal	  ID_EX_jal_out_sig :  std_logic_vector(regsize-1 downto 0);
         signal     EX_MEM_jal_out_sig : std_logic_vector(regsize-1 downto 0);
		signal		MEM_WB_jal_out_sig :  std_logic_vector(regsize-1 downto 0);
		
		 signal	data_in_A_sig : std_logic_vector(regsize - 1 downto 0);
		 signal	data_in_B_sig :  std_logic_vector(regsize - 1 downto 0);
		 signal	data_in_Imm_sig : std_logic_vector(regsize - 1 downto 0);
		 signal	IN_d_e_IR_sig : std_logic_vector(ir_size - 1 downto 0);
		 signal	in_ID_EX_jal_sig :  std_logic_vector(regsize - 1 downto 0);
		 signal	in_d_e_PC_sig :  std_logic_vector(regsize - 1 downto 0);


	-- decode execute pipeline register signals
	          
        signal  out_d_e_PC_sig:  std_logic_vector(regsize-1 downto 0);
		signal	out_d_e_jal_sig: std_logic_vector(regsize-1 downto 0);
		signal	out_d_e_IR_sig :  std_logic_vector( ir_size - 1 downto 0);
		signal	OUT_REG_A_sig:  std_logic_vector(regsize-1 downto 0);
		signal	OUT_REG_B_sig :  std_logic_vector(regsize-1 downto 0);
		signal	OUT_REG_IMM_sig : std_logic_vector(regsize-1 downto 0);
		signal	out_id_ex_opcode_sig :  std_logic_vector( opcode_length -1 downto 0);
		signal	out_id_ex_funct3_sig :  std_logic_vector( funct3_size -1 downto 0);
		signal	out_id_ex_funct7_sig :  std_logic_vector( funct7_size -1 downto 0);

	-- execute stage signals
	
	signal	IN_ex_mem_jal_sig : std_logic_vector(regsize - 1 downto 0);
		signal	IN_e_m_IR_sig :  std_logic_vector(ir_size -1 downto 0);
		signal	out_ALU_sig :  std_logic_vector(regsize - 1 downto 0);
		signal	in_e_m_B_sig :  std_logic_vector(regsize - 1 downto 0);

	-- execute memory pipeline register signals
	     
		signal	out_e_m_IR_sig : std_logic_vector( ir_size - 1 downto 0);
		signal	OUT_REG_ALUOUT_sig : std_logic_vector(regsize-1 downto 0);
		signal	OUT_REG_B_EX_MEM_sig : std_logic_vector(regsize-1 downto 0);
		

	-- memory stage signals	
	            
		signal	 IN_m_wb_IR_sig :  std_logic_vector( ir_size - 1 downto 0);
		signal	 IN_m_wb_jal_sig :  std_logic_vector( regsize - 1 downto 0);
		signal  in_m_wb_aluout_sig :  std_logic_vector( regsize - 1 downto 0);
		signal  data_in_mem_sig :  std_logic_vector(regsize - 1 downto 0);
		signal	add_in_mem_sig :  std_logic_vector( regsize - 1 downto 0);
 
	-- memory writeback pipeline register signals
       signal IN_LMD_sig : std_logic_vector(regsize-1 downto 0); --signal to connect the the output of the data memory 	
	
	signal	out_m_wb_IR_sig :  std_logic_vector( ir_size - 1 downto 0);
	signal	OUT_ALUOUT_M_WB_sig :  std_logic_vector(regsize-1 downto 0);
	signal	OUT_LMD_sig :  std_logic_vector(regsize-1 downto 0);
	
	-- writeback stage signals 
	signal	out_wb_data_mux_sig :  std_logic_vector(regsize - 1 downto 0);
	signal	out_wb_add_mux_sig : std_logic_vector(log2(regdepth)-1 downto 0);

  
 

begin

 	fetch : fetch_stage 
		generic map (ir_size, regsize)
		port map (clk,PC_rst,PC_en,ADD_CALCULATED_PC_sig,if_mux_1,in_f_d_PC_sig,in_f_d_PC_plus_four_sig,out_PC_sig);
		
 	if_id_pipe_regs : if_id_pipe 
		generic map (ir_size,opcode_length)
		port map (in_f_d_PC_sig,in_f_d_PC_plus_four_sig,out_Iram_sig,clk,f_d_rst,f_d_en,out_f_d_PC_sig,out_f_d_PC_plus_four_sig,out_f_d_IR_sig,out_f_d_opcode_sig);
		
	decode : decode_stage 
		generic map (ir_size, regdepth)
		port map (clk,out_f_d_PC_sig,out_f_d_PC_plus_four_sig,out_f_d_IR_sig,zero_mux_1,zero_mux_2,EX_MEM_ALU_OUT_sig,MEM_WB_ALU_OUT_sig,MEM_WB_LMD_sig,ID_EX_jal_out_sig,EX_MEM_jal_out_sig,MEM_WB_jal_out_sig,SEL,RF_read1,RF_read2,RF_write,RF_en ,RF_rst,out_wb_data_mux_sig,out_wb_add_mux_sig,zero,data_in_A_sig ,data_in_B_sig ,data_in_Imm_sig ,IN_d_e_IR_sig ,in_ID_EX_jal_sig,in_d_e_PC_sig ,ADD_CALCULATED_PC_sig);
		
	id_ex_pipe_regs : id_ex_pipe 
		generic map (ir_size,funct3_size,funct7_size)
		port map (IN_d_e_IR_sig,data_in_A_sig,data_in_B_sig,data_in_Imm_sig,in_d_e_PC_sig,in_ID_EX_jal_sig,clk,d_e_rst,d_e_en, out_d_e_PC_sig,out_d_e_jal_sig,out_d_e_IR_sig ,OUT_REG_A_sig,OUT_REG_B_sig ,OUT_REG_IMM_sig ,out_id_ex_opcode_sig,out_id_ex_funct3_sig,out_id_ex_funct7_sig);
				
	execute : execute_stage
		generic map (ir_size, opcode_length)
		port map ( ID_EX_jal_out_sig ,out_d_e_PC_sig,out_d_e_IR_sig,OUT_REG_A_sig,OUT_REG_B_sig,OUT_REG_IMM_sig,EX_MEM_ALU_OUT_sig ,MEM_WB_ALU_OUT_sig,MEM_WB_LMD_sig , EX_MEM_jal_out_sig ,MEM_WB_jal_out_sig,ALU_opcode,ALU_rst,ex_mux, ALU_mux_bottom ,ALU_mux_top,lui_auipc_mux, IN_ex_mem_jal_sig,IN_e_m_IR_sig ,out_ALU_sig ,in_e_m_B_sig );

	ex_mem_pipe_regs : ex_mem_pipe 
		generic map (ir_size)
		port map (IN_e_m_IR_sig,out_ALU_sig,in_e_m_B_sig ,IN_ex_mem_jal_sig,clk ,e_m_rst,e_m_en, out_e_m_IR_sig,OUT_REG_ALUOUT_sig ,OUT_REG_B_EX_MEM_sig,EX_MEM_jal_out_sig );

	memory : memory_stage
		generic map (ir_size)
		port map (out_e_m_IR_sig,OUT_REG_ALUOUT_sig,OUT_REG_B_EX_MEM_sig,EX_MEM_jal_out_sig,MEM_WB_ALU_OUT_sig,MEM_WB_LMD_sig ,MEM_WB_jal_out_sig,mem_mux, IN_m_wb_IR_sig,  IN_m_wb_jal_sig, in_m_wb_aluout_sig,data_in_mem_sig , add_in_mem_sig); 
			   			    
	mem_wb_pipe_regs : mem_wb_pipe 
		generic map (ir_size)
		port map (IN_m_wb_jal_sig,IN_m_wb_IR_sig,in_m_wb_aluout_sig,IN_LMD_sig,clk,m_WB_rst,m_wb_en,MEM_WB_jal_out_sig,out_m_wb_IR_sig ,OUT_ALUOUT_M_WB_sig ,OUT_LMD_sig );
			
    write_back : writeBack_stage 
		generic map (ir_size, regdepth)
		port map (MEM_WB_jal_out_sig,out_m_wb_IR_sig,OUT_ALUOUT_M_WB_sig,OUT_LMD_sig,wb_data_mux,wb_jal_data_mux,out_wb_data_mux_sig,out_wb_add_mux_sig);

	comparator_for_forwarding : comp_forwarding 
		generic map (ir_size, log2(regdepth))
		port map (out_f_d_IR_sig,out_d_e_IR_sig,out_e_m_IR_sig,out_m_wb_IR_sig,EM_Rd3_eq_FD_Rs1,MW_Rd1_eq_FD_Rs1,MW_Rd3_eq_FD_Rs1,EM_Rd2_eq_FD_Rs1,MW_Rd2_eq_FD_Rs1,MW_Rd3_eq_EM_Rs2,MW_Rd2_eq_EM_Rs2,EM_Rd3_eq_DE_Rs1,EM_Rd3_eq_DE_Rs2,MW_Rd3_eq_DE_Rs1 ,MW_Rd3_eq_DE_Rs2,EM_Rd2_eq_DE_Rs1,EM_Rd2_eq_DE_Rs2,MW_Rd2_eq_DE_Rs1, MW_Rd2_eq_DE_Rs2,MW_Rd3_eq_EM_Rs1, MW_Rd2_eq_EM_Rs1, MW_Rd1_eq_EM_Rs1, EM_Rd3_eq_FD_Rs2, EM_Rd2_eq_FD_Rs2,MW_Rd1_eq_FD_Rs2, MW_Rd3_eq_FD_Rs2, MW_Rd2_eq_FD_Rs2,DE_Rd3_eq_FD_Rs1,DE_Rd3_eq_FD_Rs2);	

	comparator_for_stall : comp_stall 
		generic map (ir_size, log2(regdepth))
		port map (out_Iram_sig,out_f_d_IR_sig,out_d_e_IR_sig,rd2_ID_EX_eq_rs1_IF_ID,rd2_ID_EX_eq_rs2_IF_ID,rd2_ID_EX_eq_rs1_IR,rd2_IF_ID_eq_rs1_IR,rd3_IF_ID_eq_rs1_IR);
                      
	instruction_opcode <= out_Iram_sig(opcode_length - 1 downto 0);
	out_pc <= out_pc_sig;
	out_Iram_sig <= in_F_D_IR;
	
	IN_LMD_sig <= IN_LMD; 
	data_in_mem <= data_in_mem_sig;
	add_in_mem <= add_in_mem_sig;
	

end struct;