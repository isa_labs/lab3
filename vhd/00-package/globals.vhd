library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package myTypes is

	function log2(arg : integer) return integer;

	constant OP_CODE_SIZE : integer := 7;   -- OPCODE field size
	constant FUNCT3_SIZE    : integer := 3;  -- FUNC field size
	constant FUNCT7_SIZE    : integer := 7;
	constant ALU_OPC_SIZE : integer := 4;
	constant REG_SIZE	  	: integer := 64;
	constant RF_ADD_SIZE  : integer := 5;
	constant NBIT 		  	: integer := 7;		-- Number of bit of the instruction pipeline
	
	constant add_rtype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0110011#,OP_CODE_SIZE));
	constant sub_rtype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0110011#,OP_CODE_SIZE));
	constant xor_rtype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0110011#,OP_CODE_SIZE));
	constant slt_rtype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0110011# ,OP_CODE_SIZE));
	constant srai_itype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0010011#,OP_CODE_SIZE));
	constant andi_itype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0010011#,OP_CODE_SIZE));
	constant addi_itype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0010011#,OP_CODE_SIZE));
	constant auipc_itype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0010111#,OP_CODE_SIZE));
	constant lui_itype : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0110111#,OP_CODE_SIZE));
	constant lw_load : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0000011#,OP_CODE_SIZE));
	constant sw_store : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#0100011#,OP_CODE_SIZE));
	constant beq_branch : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#1100011#,OP_CODE_SIZE));
	constant jal_jumpandlink : std_logic_vector(OP_CODE_SIZE-1 downto 0) :=  std_logic_vector(to_unsigned(2#1101111#,OP_CODE_SIZE));
	
	
--ALU operation codes	
	constant ALU_ADD : std_logic_vector( ALU_OPC_SIZE-1 downto 0) 	:= "0110"; --msb 01 add,comp
	constant ALU_SUB : std_logic_vector( ALU_OPC_SIZE-1 downto 0) 	:= "0111"; --msb 01 add,comp
	constant ALU_SLT : std_logic_vector(ALU_OPC_SIZE-1 downto 0) 	:= "0101"; -- 
	constant ALU_AND : std_logic_vector( ALU_OPC_SIZE-1 downto 0) 	:= "1100"; -- msb 11 logic unit
    constant ALU_XOR : std_logic_vector(ALU_OPC_SIZE-1 downto 0) 	:= "1111";
    constant ALU_SRA : std_logic_vector(ALU_OPC_SIZE-1 downto 0) 	:= "1010"; --msb 10 shift unit
	constant ALU_NOP : std_logic_vector(ALU_OPC_SIZE-1 downto 0)	:= "0000"; 


   constant funct3_add : std_logic_vector(FUNCT3_SIZE-1 downto 0)	:= "000";  --add/addi
   constant funct3_srai : std_logic_vector(FUNCT3_SIZE-1 downto 0)	:= "101";  --srai
   constant funct3_andi : std_logic_vector(FUNCT3_SIZE-1 downto 0)	:= "111";  --andi
   constant funct3_slt : std_logic_vector(FUNCT3_SIZE-1 downto 0)	:= "010";  --slt
   constant funct3_xor : std_logic_vector(FUNCT3_SIZE-1 downto 0)	:= "100";  --xor
   
   constant funct7_add : std_logic_vector(FUNCT7_SIZE-1 downto 0)	:= "0000000";  --add
   constant funct7_sub : std_logic_vector(FUNCT7_SIZE-1 downto 0)	:= "0100000";  --sub

end myTypes;

package body myTypes is

	-- a function to compute the base 2 logarithm of the argument
	function log2(arg : integer) return integer is
		variable temp : integer := arg;
		variable result : integer := 0;
	begin
		while temp > 1 loop 		
			result := result + 1; 
			temp := temp / 2;
		end loop;
		return result;
	end function log2;

end myTypes;
