library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity DLX is
	generic(DMEM_DEPTH : integer := 48; --for the memory
					DATA_SIZE : integer := 64;
					IRAM_depth : integer := 64;
					INSTRUCTION_SIZE :integer := 32;
					ADD_SIZE_IRAM: integer := 32;
					ADD_SIZE_DRAM: integer := 64;
					opcode_size : integer := 7;  -- Op Code Size
					funct3_size	: integer := 3;
					funct7_size	: integer := 7;
					InstrType_size: integer := 8; --Number of bits of the instruction pipeline
					regsize : integer := 64;
					regdepth : integer := 32;
					alu_opcode_size : integer := 4);
	port (clk: in std_logic;
				rst: in std_logic;						--reset of the control unit
				dram_read: in std_logic_vector(DATA_SIZE-1 downto 0);
				iram_in: in std_logic_vector(INSTRUCTION_SIZE-1 downto 0);
				read_en: out std_logic;
				write_en: out std_logic;
				dram_addr: out std_logic_vector(ADD_SIZE_DRAM-1 downto 0);
				dram_write: out std_logic_vector(DATA_SIZE-1 downto 0);
				iram_addr: out std_logic_vector(ADD_SIZE_IRAM-1 downto 0)); 
end DLX;


architecture struct of DLX is
	
	component datapath is  -- datatpath of DLX without memory, memory connected exernally
	generic        (regsize : integer := 64;
					regdepth : integer := 32;
					ir_size: integer := 32;
					opcode_length : integer := 7;
					alu_opcode_length : integer := 4;
					funct3_length : integer := 3;
					funct7_length : integer := 7;
					IRAM_depth : integer := 64;
					ADD_SIZE  : integer := 32);
	 
	port (clk : in std_logic;
	
				-- PIPELINE REGS RESET CONTROL SIGNAL INPUTS(ACTIVE LOW)
				PC_rst : in std_logic;
				f_d_rst	: in std_logic;  
				d_e_rst	: in std_logic; 
				e_m_rst	: in std_logic; 
				m_wb_rst : in std_logic; 
				
				-- PIPELINE REGS ENABLE CONTROL SIGNAL INPUTS
				PC_en : in std_logic; 
				f_d_en : in std_logic;
				d_e_en : in std_logic; 
				e_m_en : in std_logic; 
				m_wb_en	: in std_logic;

				
			
				-- fetch inputs
				if_mux_1 : in std_logic;
				in_F_D_IR : in std_logic_vector(ir_size downto 0); -- from external IRAM
				
			    -- fetch outputs
				  out_pc : out std_logic_vector(ir_size downto 0); --input of Instr Ram
			      
				-- decode inputs
				RF_en : in std_logic; -- reg file enable
				RF_rst : in std_logic;  
				RF_read1 : in std_logic; -- 1 if read at port 1 of the register file
				RF_read2 : in std_logic; -- 1 if read at port 2 of the register file
				SEL : in std_logic_vector(2 downto 0);
				zero_mux_1: in std_logic_vector(2 downto 0);
				zero_mux_2: in std_logic_vector(2 downto 0);
				
				-- execute inputs
				ex_mux : in std_logic;
				ALU_mux_top : in std_logic_vector(2 downto 0); 
				ALU_mux_bottom : in std_logic_vector(2 downto 0); 
				lui_auipc_mux: in std_logic_vector(1 downto 0); 
				ALU_opcode : in std_logic_vector(alu_opcode_length - 1 downto 0);
				ALU_rst : in std_logic;
				
				-- memory stage inputs
				mem_mux : in std_logic_vector(1 downto 0); -- for forwarding  
			    
				IN_LMD : in std_logic_vector(regsize - 1 downto 0); -- the output of the external data memory 
				data_in_mem : out std_logic_vector(regsize - 1 downto 0); --inputs of the data memory
				add_in_mem : out std_logic_vector( regsize - 1 downto 0);  -- to be connected with the external data memory
			    
				
				-- write back inputs
				wb_data_mux : in std_logic;
				wb_jal_data_mux : in std_logic;
				 RF_write : in std_logic; -- 1 if write the register file
				 
				
				-- Control signals outputs
				
				zero : out std_logic; -- zero of the zero comparator in the decode stage
	
				instruction_opcode : out std_logic_vector( opcode_length-1 downto 0); -- output opcode of IRAM
				IF_ID_opcode : out std_logic_vector( opcode_length-1 downto 0); -- from the IF/ID instruction register
				ID_EX_opcode : out std_logic_vector( opcode_length-1 downto 0); -- from the ID/EX instruction register
				ID_EX_funct3	 : out std_logic_vector( funct3_length-1 downto 0);  -- from the ID/EX instruction register
                 ID_EX_funct7	 : out std_logic_vector( funct7_length-1 downto 0);
				 
				EM_Rd3_eq_FD_Rs1	: out std_logic; -- from comp_forwarding
				MW_Rd1_eq_FD_Rs1	: out std_logic;
				MW_Rd3_eq_FD_Rs1	: out std_logic;
				EM_Rd2_eq_FD_Rs1 	: out std_logic;
				MW_Rd2_eq_FD_Rs1	: out std_logic;
				MW_Rd3_eq_EM_Rs2	: out std_logic;
				MW_Rd2_eq_EM_Rs2  : out std_logic;
				EM_Rd3_eq_DE_Rs1	: out std_logic; -- 1 if ex_mem_IR16..20 = id_ex_IR6..10
				EM_Rd3_eq_DE_Rs2	: out std_logic; -- 1 if ex_mem_IR16..20 = id_ex_IR11..15
				MW_Rd3_eq_DE_Rs1	: out std_logic;
				MW_Rd3_eq_DE_Rs2	: out std_logic;
				EM_Rd2_eq_DE_Rs1	: out std_logic;
				EM_Rd2_eq_DE_Rs2	: out std_logic;
				MW_Rd2_eq_DE_Rs1	: out std_logic;
				MW_Rd2_eq_DE_Rs2	: out std_logic;
				MW_Rd3_eq_EM_Rs1	: out std_logic;
				MW_Rd2_eq_EM_Rs1  : out std_logic;
				MW_Rd1_eq_EM_Rs1	: out std_logic; -- Rd1 of jump register or jump and link register 
				EM_Rd3_eq_FD_Rs2 : out std_logic; 
				EM_Rd2_eq_FD_Rs2 : out std_logic;
				MW_Rd1_eq_FD_Rs2 : out std_logic; 
				MW_Rd3_eq_FD_Rs2 : out std_logic;
				MW_Rd2_eq_FD_Rs2 : out std_logic;
				DE_Rd3_eq_FD_Rs1: out std_logic;
				DE_Rd3_eq_FD_Rs2: out std_logic;

				rd2_ID_EX_eq_rs1_IF_ID:	out std_logic; -- from comp_stall
				rd2_ID_EX_eq_rs2_IF_ID:	out std_logic;
				rd2_ID_EX_eq_rs1_IR: out std_logic;
				rd2_IF_ID_eq_rs1_IR: out std_logic;
				rd3_IF_ID_eq_rs1_IR: out std_logic);
	end component;

	component cu_fsm is
	generic(opcode_size : integer := 7;  -- Op Code Size
					InstrType_size : integer := 8; -- Number of bits of the instruction pipeline
					reg_size : integer := 64;
					funct3_size: integer := 3;
			    funct7_size: integer := 7;
					alu_opcode_size : integer := 4);
	port (-- INPUTS
				clk : in std_logic;
				rst : in std_logic;

				instruction_opcode : in std_logic_vector(opcode_size-1 downto 0); -- output of IRAM
				IF_ID_opcode : in std_logic_vector(opcode_size-1 downto 0); -- from the IF/ID instruction register
				ID_EX_opcode : in std_logic_vector(opcode_size-1 downto 0); -- from the ID/EX instruction register
				ID_EX_funct3  : in std_logic_vector(funct3_size -1  downto 0);
		    ID_EX_funct7  : in std_logic_vector(funct7_size - 1 downto 0);

				-- for branching
				
				zero : in std_logic; -- output of the  zero comparator in decode stage

				-- for forwarding
				EM_Rd3_eq_DE_Rs1	: in std_logic;
				EM_Rd3_eq_DE_Rs2	: in std_logic;
				MW_Rd3_eq_DE_Rs1	: in std_logic;
				MW_Rd3_eq_DE_Rs2	: in std_logic;
				EM_Rd2_eq_DE_Rs1	: in std_logic;
				EM_Rd2_eq_DE_Rs2	: in std_logic;
				MW_Rd2_eq_DE_Rs1	: in std_logic;
				MW_Rd2_eq_DE_Rs2	: in std_logic;
				MW_Rd3_eq_EM_Rs1	: in std_logic;
				MW_Rd2_eq_EM_Rs1  : in std_logic;
				MW_Rd1_eq_EM_Rs1	: in std_logic;
				EM_Rd3_eq_FD_Rs1	: in std_logic;
				MW_Rd1_eq_FD_Rs1	: in std_logic;
				MW_Rd3_eq_FD_Rs1	: in std_logic;
				EM_Rd2_eq_FD_Rs1 	: in std_logic;
				MW_Rd2_eq_FD_Rs1 	: in std_logic;
				MW_Rd3_eq_EM_Rs2 	: in std_logic;
				MW_Rd2_eq_EM_Rs2 	: in std_logic;
				EM_Rd3_eq_FD_Rs2 : in std_logic; 
				EM_Rd2_eq_FD_Rs2 : in std_logic;
				MW_Rd1_eq_FD_Rs2 : in std_logic; 
				MW_Rd3_eq_FD_Rs2 : in std_logic;
				MW_Rd2_eq_FD_Rs2 : in std_logic;
				DE_Rd3_eq_FD_Rs1: in std_logic;
				DE_Rd3_eq_FD_Rs2: in std_logic;

				-- for stalling
				rd2_ID_EX_eq_rs1_IF_ID : in std_logic; -- output of comparators
				rd2_ID_EX_eq_rs2_IF_ID : in std_logic;
				rd2_ID_EX_eq_rs1_IR :	in std_logic;
				rd2_IF_ID_eq_rs1_IR : in std_logic;
				rd3_IF_ID_eq_rs1_IR : in std_logic;

				-- all pipeline rst and program counter enable
				PC_rst : out std_logic;
				f_d_rst	: out std_logic;
				d_e_rst	: out std_logic;
				e_m_rst	: out std_logic;
				m_wb_rst : out std_logic;
				PC_en : out std_logic;

				-- branch outputs
			  if_mux_1 : out std_logic;

				-- fetch outputs
				
				IF_ID_en : out std_logic;

				-- decode outputs
				RF_read1 : out std_logic; 	-- 1 if read at port 1 of the register file
				RF_read2 : out std_logic; 	-- 1 if read at port 2 of the register file
				RF_en : out std_logic; 			-- reg file enable
				RF_rst : out std_logic;
				SEL : out STD_LOGIC_VECTOR(2 downto 0);   --select which operation in the sign extender
				zero_mux1 : out std_logic_vector(2 downto 0); -- for forwarding
				zero_mux2 : out std_logic_vector(2 downto 0); -- for forwarding
				ID_EX_en : out std_logic;

				-- execute outputs
				ex_mux : out std_logic;
				ALU_rst : out std_logic;
				ALU_OPCODE : out	std_logic_vector( alu_opcode_size - 1 downto 0);
				ALU_MUX_top : out std_logic_vector(2 downto 0); -- for forwarding
				ALU_MUX_bottom : out std_logic_vector(2 downto 0);
				lui_auipc_mux:out std_logic_vector(1 downto 0);
				EX_MEM_en : out std_logic;

				-- memory outputs
				mem_rst : out std_logic;
				mem_rd_en	: out std_logic; -- 1 if read data memory
				mem_wr_en : out std_logic; -- 1 if write data memory
				mem_mux : out std_logic_vector(1 downto 0);
				MEM_WB_en : out std_logic;

				-- write back outputs
				wb_data_mux : out std_logic;
				wb_jal_data_mux : out std_logic; --the second mux
				RF_write : out std_logic); -- 1 if write the register file
	end component;

	--component IRAM is
	--	generic(RAM_DEPTH : integer := 48;
	--					I_SIZE : integer := 32);
	--	port (Rst  : in  std_logic;
	--				Addr : in  std_logic_vector(I_SIZE - 1 downto 0);
	--				Dout : out std_logic_vector( I_SIZE - 1 downto 0));
	--end component;

	--component dataMemory is
	--	generic(DMEM_DEPTH : integer := 48;
	--					DATA_SIZE : integer := 64;
	--					ADD_SIZE  : integer := 64);
	--	port (Rst  : in  std_logic;
	--				Addr : in std_logic_vector( ADD_SIZE-1 downto 0);
	--				Din : in  std_logic_vector(DATA_SIZE-1 downto 0);
	--				read_en : in std_logic;
	--				write_en : in std_logic;
	--				Dout : out std_logic_vector( DATA_SIZE-1 downto 0));
	--end component;

	signal PC_rst	: std_logic;
	signal f_d_rst : std_logic;  
	signal d_e_rst : std_logic; 
	signal e_m_rst : std_logic; 
	signal m_wb_rst	: std_logic; 
	
	-- PIPELINE REGS ENABLE CONTROL SIGNAL INPUTS
	signal PC_en : std_logic; 
	signal IF_ID_en : std_logic;
	signal ID_EX_en	: std_logic; 
	signal EX_MEM_en : std_logic; 
	signal MEM_WB_en : std_logic;

	-- branch signals
	signal if_mux_1 :  std_logic;
	
	--fetch signals
	signal in_F_D_IR :  std_logic_vector(INSTRUCTION_SIZE-1 downto 0); -- from external IRAM
	signal out_pc_sig :  std_logic_vector(INSTRUCTION_SIZE -1 downto 0); --input of Instr Ram

	-- decode inputs
	signal RF_en : std_logic; -- reg file enable
	signal RF_rst : std_logic;  
	signal RF_read1	: std_logic; -- 1 if read at port 1 of the register file
	signal RF_read2	: std_logic; -- 1 if read at port 2 of the register file
	signal SEL : std_logic_vector(2 downto 0);
	signal zero_mux1 : std_logic_vector(2 downto 0);
	signal zero_mux2 : std_logic_vector(2 downto 0);
		
	-- execute inputs
	signal ex_mux : std_logic;
	signal ALU_mux_top : std_logic_vector(2 downto 0); 
	signal ALU_mux_bottom : std_logic_vector(2 downto 0); 
	signal lui_auipc_mux: std_logic_vector(1 downto 0);
	signal ALU_opcode : std_logic_vector(alu_opcode_size - 1 downto 0);
	signal ALU_rst : std_logic;
	
	-- memory inputs
	signal mem_rd_en : std_logic; -- 1 if read data memory
	signal mem_wr_en : std_logic; -- 1 if write data memory
	signal mem_mux : std_logic_vector(1 downto 0); -- for forwarding  
	signal IN_LMD : std_logic_vector(regsize - 1 downto 0); --output of data memory
	signal data_in_mem :  std_logic_vector(regsize - 1 downto 0); --inputs of the data memory
	signal	add_in_mem :  std_logic_vector( regsize - 1 downto 0);
	signal mem_rst : std_logic;

	-- write back inputs
	signal wb_data_mux : std_logic;
	signal wb_jal_data_mux 	: std_logic;
	signal RF_write	: std_logic; -- 1 if write the register file
		
	-- Control signals outputs
	signal zero : std_logic; -- zero of the zero comparator in the decode stage
	signal PC_iram : std_logic_vector(regsize - 1 downto 0);

	signal EM_Rd3_eq_FD_Rs1	:  std_logic; -- from comp_forwarding
	signal	MW_Rd1_eq_FD_Rs1:  std_logic;
	signal	MW_Rd3_eq_FD_Rs1:  std_logic;
	signal	EM_Rd2_eq_FD_Rs1 :  std_logic;
	signal	MW_Rd2_eq_FD_Rs1:  std_logic;
	signal	MW_Rd3_eq_EM_Rs2:  std_logic;
	signal	MW_Rd2_eq_EM_Rs2 :  std_logic;
	signal	EM_Rd3_eq_DE_Rs1:  std_logic; 
	signal	EM_Rd3_eq_DE_Rs2:  std_logic; 
	signal	MW_Rd3_eq_DE_Rs1: std_logic;
	signal	MW_Rd3_eq_DE_Rs2:  std_logic;
	signal	EM_Rd2_eq_DE_Rs1:  std_logic;
	signal	EM_Rd2_eq_DE_Rs2:  std_logic;
	signal	MW_Rd2_eq_DE_Rs1:  std_logic;
	signal	MW_Rd2_eq_DE_Rs2:  std_logic;
	signal	MW_Rd3_eq_EM_Rs1:  std_logic;
	signal	MW_Rd2_eq_EM_Rs1 : std_logic;
	signal	MW_Rd1_eq_EM_Rs1:  std_logic; -- Rd1 of jump register or jump and link register 
	signal	EM_Rd3_eq_FD_Rs2 :  std_logic; 
	signal	EM_Rd2_eq_FD_Rs2 :  std_logic;
	signal	MW_Rd1_eq_FD_Rs2 :  std_logic; 
	signal	MW_Rd3_eq_FD_Rs2 :  std_logic;
	signal	MW_Rd2_eq_FD_Rs2 :  std_logic;
	signal	DE_Rd3_eq_FD_Rs1: std_logic;
	signal	DE_Rd3_eq_FD_Rs2: std_logic;
 

	signal rd2_ID_EX_eq_rs1_IF_ID :	std_logic; -- from comp_stall
	signal rd2_ID_EX_eq_rs2_IF_ID : std_logic;
	signal rd2_ID_EX_eq_rs1_IR : std_logic;
	signal rd2_IF_ID_eq_rs1_IR : std_logic;
	signal rd3_IF_ID_eq_rs1_IR : std_logic;

	signal instruction_opcode : std_logic_vector(opcode_size-1 downto 0); -- output of IRAM
	signal IF_ID_opcode : std_logic_vector(opcode_size-1 downto 0); -- from the IF/ID instruction register
	signal ID_EX_opcode : std_logic_vector(opcode_size-1 downto 0); -- from the ID/EX instruction register
	signal ID_EX_funct3	: std_logic_vector(funct3_size-1 downto 0);  -- from the ID/EX instruction register
  signal ID_EX_funct7	: std_logic_vector(funct7_size-1 downto 0);
		
begin 

	iram_addr <= out_pc_sig;
	in_F_D_IR <= iram_in;

	--IRAMi : IRAM
	--	generic map(IRAM_depth,
	--							INSTRUCTION_SIZE)
	--	port map (rst,
	--						out_pc_sig,
	--						in_F_D_IR );

	dram_addr <= add_in_mem;
	dram_write <= data_in_mem;
	read_en <= mem_rd_en;
	write_en <= mem_wr_en;
	IN_LMD <= dram_read;

	--DRAMi : dataMemory
	--	generic map(DMEM_DEPTH,
	--							DATA_SIZE,
	--							ADD_SIZE_DRAM)
	--	port map (rst,
	--						add_in_mem,
	--						data_in_mem,
	--						mem_rd_en,
	--						mem_wr_en,
	--						IN_LMD);

	datapath_instance : datapath
		generic map(regsize,
								regdepth,
								INSTRUCTION_SIZE,
								opcode_size,
								alu_opcode_size,
								funct3_size,
								funct7_size,
								IRAM_depth,
								ADD_SIZE_IRAM)
		port map (clk,

							PC_rst,
							f_d_rst,
							d_e_rst,
							e_m_rst,
							m_wb_rst,

							PC_en,
							IF_ID_en,
							ID_EX_en,
							EX_MEM_en,
							MEM_WB_en,

							if_mux_1, 
							in_F_D_IR,
							
							out_pc_sig,

							RF_en, 
							RF_rst, 
							RF_read1, 
							RF_read2, 
							SEL,
							zero_mux1,
							zero_mux2,

							ex_mux, 
							ALU_mux_top, 
							ALU_mux_bottom,
                            lui_auipc_mux,							
							ALU_opcode, 
							ALU_rst,
 
							mem_mux, 
							IN_LMD,  
							data_in_mem,
							add_in_mem,

							wb_data_mux, 
							wb_jal_data_mux, 
							RF_write,	

							zero,
						
							instruction_opcode,
							IF_ID_opcode,
							ID_EX_opcode,
							ID_EX_funct3,
							ID_EX_funct7,

							EM_Rd3_eq_FD_Rs1,	
				            MW_Rd1_eq_FD_Rs1,	
				            MW_Rd3_eq_FD_Rs1,	
				            EM_Rd2_eq_FD_Rs1, 	
				            MW_Rd2_eq_FD_Rs1,	
				            MW_Rd3_eq_EM_Rs2,	
				            MW_Rd2_eq_EM_Rs2 , 
			              	EM_Rd3_eq_DE_Rs1,	
			            	EM_Rd3_eq_DE_Rs2,	 
			            	MW_Rd3_eq_DE_Rs1,	
			            	MW_Rd3_eq_DE_Rs2,	
			            	EM_Rd2_eq_DE_Rs1,	
			            	EM_Rd2_eq_DE_Rs2,	
			            	MW_Rd2_eq_DE_Rs1,	
				            MW_Rd2_eq_DE_Rs2,	
				            MW_Rd3_eq_EM_Rs1,	
			             	MW_Rd2_eq_EM_Rs1,  
			            	MW_Rd1_eq_EM_Rs1,	 
				            EM_Rd3_eq_FD_Rs2 ,
				            EM_Rd2_eq_FD_Rs2 ,
				            MW_Rd1_eq_FD_Rs2 ,
			            	MW_Rd3_eq_FD_Rs2, 
			            	MW_Rd2_eq_FD_Rs2, 
			            	DE_Rd3_eq_FD_Rs1,
			            	DE_Rd3_eq_FD_Rs2,

							rd2_ID_EX_eq_rs1_IF_ID,
							rd2_ID_EX_eq_rs2_IF_ID, 
							rd2_ID_EX_eq_rs1_IR,
							rd2_IF_ID_eq_rs1_IR, 
							rd3_IF_ID_eq_rs1_IR);

	control_unit : cu_fsm 
		generic map (opcode_size,InstrType_size,regsize, funct3_size, funct7_size,alu_opcode_size)
		port map (clk, 
		           rst,
				   instruction_opcode, 
		           IF_ID_opcode,
		           ID_EX_opcode,
				   ID_EX_funct3, 
				   ID_EX_funct7, 
				   zero,
				   EM_Rd3_eq_FD_Rs1,	
				   MW_Rd1_eq_FD_Rs1,	
				   MW_Rd3_eq_FD_Rs1,	
				   EM_Rd2_eq_FD_Rs1, 	
				   MW_Rd2_eq_FD_Rs1,	
				   MW_Rd3_eq_EM_Rs2,	
				   MW_Rd2_eq_EM_Rs2 , 
			       EM_Rd3_eq_DE_Rs1,	
			       EM_Rd3_eq_DE_Rs2,	 
			       MW_Rd3_eq_DE_Rs1,	
			       MW_Rd3_eq_DE_Rs2,	
			       EM_Rd2_eq_DE_Rs1,	
			       EM_Rd2_eq_DE_Rs2,	
			       MW_Rd2_eq_DE_Rs1,	
				   MW_Rd2_eq_DE_Rs2,	
				   MW_Rd3_eq_EM_Rs1,	
			       MW_Rd2_eq_EM_Rs1,  
			       MW_Rd1_eq_EM_Rs1,	 
				   EM_Rd3_eq_FD_Rs2 ,
				   EM_Rd2_eq_FD_Rs2 ,
				   MW_Rd1_eq_FD_Rs2 ,
			       MW_Rd3_eq_FD_Rs2, 
			       MW_Rd2_eq_FD_Rs2,
				   DE_Rd3_eq_FD_Rs1,
				   DE_Rd3_eq_FD_Rs2,
				   rd2_ID_EX_eq_rs1_IF_ID, 
				   rd2_ID_EX_eq_rs2_IF_ID, 
				   rd2_ID_EX_eq_rs1_IR, 
				   rd2_IF_ID_eq_rs1_IR,
				   rd3_IF_ID_eq_rs1_IR,
				   PC_rst, 
				   f_d_rst,
				   d_e_rst, 
				   e_m_rst,
				   m_wb_rst,
				   PC_en,				   
				   if_mux_1, 
				   IF_ID_en,
				   RF_read1,
				   RF_read2, 
				   RF_en,
				   RF_rst, 
				   SEL,
				   zero_mux1,
				   zero_mux2,
				   ID_EX_en,
				   ex_mux, 
				   ALU_rst,
				   ALU_OPCODE, 
				   ALU_MUX_top,
				   ALU_MUX_bottom,
				   lui_auipc_mux,
				   EX_MEM_en, 
				   mem_rst,
				   mem_rd_en,
				   mem_wr_en, 
				   mem_mux, 
				   MEM_WB_en,
				   wb_data_mux,
				   wb_jal_data_mux,
				   RF_write);


end struct;