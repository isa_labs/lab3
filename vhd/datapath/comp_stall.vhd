library ieee;
use ieee.std_logic_1164.all;

entity comp_stall is
	generic(N : integer := 32;
		 			add_size : integer := 5);
	port (IRAM_output : in std_logic_vector( N-1 downto 0);
				f_d_IR : in std_logic_vector(N-1 downto 0);
				d_e_IR : in std_logic_vector(N-1 downto 0);
				
				rd2_ID_EX_eq_rs1_IF_ID : out std_logic; -- output of comparators 
				rd2_ID_EX_eq_rs2_IF_ID : out std_logic;
				rd2_ID_EX_eq_rs1_IR : out std_logic;
				rd2_IF_ID_eq_rs1_IR : out std_logic;
				rd3_IF_ID_eq_rs1_IR : out std_logic);
end comp_stall;

architecture behav of comp_stall is

	signal rd2_ID_EX_eq_rs1_IF_ID_vect : std_logic_vector( add_size - 1 downto 0);
	signal rd2_ID_EX_eq_rs2_IF_ID_vect : std_logic_vector( add_size - 1 downto 0);
	signal rd2_ID_EX_eq_rs1_IR_vect :	std_logic_vector( add_size - 1 downto 0);
	signal rd2_IF_ID_eq_rs1_IR_vect : std_logic_vector(add_size - 1 downto 0);
	signal rd3_IF_ID_eq_rs1_IR_vect :	std_logic_vector(add_size - 1 downto 0);

begin

	xnor_label : for i in 0 to add_size - 1 generate

		rd2_ID_EX_eq_rs1_IF_ID_vect(i) <= not (d_e_IR(i+7) xor f_d_IR(i+15));
		rd2_ID_EX_eq_rs2_IF_ID_vect(i) <= not (d_e_IR(i+7) xor f_d_IR(i+20));
		rd2_ID_EX_eq_rs1_IR_vect(i) <= not (d_e_IR(i+7) xor IRAM_output(i+15));
		rd2_IF_ID_eq_rs1_IR_vect(i) <= not (f_d_IR(i+7) xor IRAM_output(i+15));
		rd3_IF_ID_eq_rs1_IR_vect(i) <= not (f_d_IR(i+7) xor IRAM_output(i+15));

	end generate;

	rd2_ID_EX_eq_rs1_IF_ID <=((((rd2_ID_EX_eq_rs1_IF_ID_vect(0) and rd2_ID_EX_eq_rs1_IF_ID_vect(1)) and rd2_ID_EX_eq_rs1_IF_ID_vect(2)) and rd2_ID_EX_eq_rs1_IF_ID_vect(3)) and rd2_ID_EX_eq_rs1_IF_ID_vect(4));
	rd2_ID_EX_eq_rs2_IF_ID <=((((rd2_ID_EX_eq_rs2_IF_ID_vect(0) and rd2_ID_EX_eq_rs2_IF_ID_vect(1)) and rd2_ID_EX_eq_rs2_IF_ID_vect(2)) and rd2_ID_EX_eq_rs2_IF_ID_vect(3)) and rd2_ID_EX_eq_rs2_IF_ID_vect(4));
	rd2_ID_EX_eq_rs1_IR <=((((rd2_ID_EX_eq_rs1_IR_vect(0) and rd2_ID_EX_eq_rs1_IR_vect(1)) and rd2_ID_EX_eq_rs1_IR_vect(2)) and rd2_ID_EX_eq_rs1_IR_vect(3)) and rd2_ID_EX_eq_rs1_IR_vect(4));
	rd2_IF_ID_eq_rs1_IR <=((((rd2_IF_ID_eq_rs1_IR_vect(0) and rd2_IF_ID_eq_rs1_IR_vect(1)) and rd2_IF_ID_eq_rs1_IR_vect(2)) and rd2_IF_ID_eq_rs1_IR_vect(3)) and rd2_IF_ID_eq_rs1_IR_vect(4));
	rd3_IF_ID_eq_rs1_IR <=((((rd3_IF_ID_eq_rs1_IR_vect(0) and rd3_IF_ID_eq_rs1_IR_vect(1)) and rd3_IF_ID_eq_rs1_IR_vect(2)) and rd3_IF_ID_eq_rs1_IR_vect(3)) and rd3_IF_ID_eq_rs1_IR_vect(4));

end behav;