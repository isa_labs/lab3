library IEEE;
use IEEE.std_logic_1164.all;

entity MUX21_GENERIC is
	generic(NBIT : integer := 32);
	port (A   : In  std_logic_vector(NBIT-1 downto 0);
				B   : In  std_logic_vector(NBIT-1 downto 0);
				SEL : In  std_logic;
				Y   : Out std_logic_vector(NBIT-1 downto 0));
end MUX21_GENERIC;

architecture STRUCTURAL of MUX21_GENERIC is
	
	component MUX21
	port (A : In  std_logic;
				B : In  std_logic;
				S : In  std_logic;
				Y : Out std_logic);
	end component;

	begin
	
	GEN_MUX : for i in 0 to NBIT-1 generate				-- the structural mux uses a generate to create an array of 1-bit muxes
		MUX_ARR : MUX21 port map (A(i), B(i), SEL, Y(i));
	end generate;
			 
end STRUCTURAL;


