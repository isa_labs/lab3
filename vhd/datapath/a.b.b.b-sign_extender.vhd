LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY SIGN_EXTENDED IS
	GENERIC(N : INTEGER := 32);
	PORT (IR_F_D : IN STD_LOGIC_VECTOR(N-1 downto 0);  
				SEL : IN STD_LOGIC_VECTOR(2 downto 0);  
				OUT_SIGN_EXT : OUT STD_LOGIC_VECTOR(2*N-1 DOWNTO 0));
END SIGN_EXTENDED;

ARCHITECTURE STRUCT OF SIGN_EXTENDED IS

	COMPONENT MUX61 is
	generic(n : integer := 64);
	port (      IN1 :	in	std_logic_vector(n-1 downto 0);
				IN2 :	in	std_logic_vector(n-1 downto 0);
				IN3 :	in	std_logic_vector(n-1 downto 0);
				IN4 :	in	std_logic_vector(n-1 downto 0);
				IN5 : in	std_logic_vector(n-1 downto 0);
				IN6 : in	std_logic_vector(n-1 downto 0);
				S :	in	std_logic_vector(2 downto 0);
				Y :	out	std_logic_vector(n-1 downto 0));
	END COMPONENT;

	signal srai_imm : std_logic_vector(63 downto 0);
	signal i_lw_imm : std_logic_vector(63 downto 0); --for alu imm and lw
	signal auipc_lui_imm : std_logic_vector(63 downto 0);
	signal beq_imm : std_logic_vector(63 downto 0);
	signal jal_imm : std_logic_vector(63 downto 0);
	signal sw_imm : std_logic_vector(63 downto 0);
	
	signal srai_imm_ext : std_logic_vector(58 downto 0);--extended sign
	signal i_lw_imm_ext : std_logic_vector(51 downto 0); 
	signal auipc_lui_imm_ext : std_logic_vector(43 downto 0);
	signal beq_imm_ext : std_logic_vector(51 downto 0);
	signal jal_imm_ext : std_logic_vector(43 downto 0);
	signal sw_imm_ext : std_logic_vector(51 downto 0);
	
	
	begin
	
	srai_imm(4 downto 0)     <= IR_F_D(24 downto 20); --5 bit shift
	ext1:  for i in 2*N-1 downto 5 generate
	            srai_imm_ext(i) <=IR_F_D(24);
				end generate;
	i_lw_imm(11 downto 0)   <= IR_F_D(31 downto 20);
	ext2:  for i in 2*N-1 downto 12 generate
	            i_lw_imm_ext(i) <=IR_F_D(31);
				end generate;
	auipc_lui_imm (31 downto 12)  <= IR_F_D(31 downto 12)  ;
	 ext3: for i in 2*N-1 downto 32 generate
	            auipc_lui_imm_ext(i) <=IR_F_D(31);
				end generate;
				auipc_lui_imm (11 downto 0) <= "000000000000";
	beq_imm	(11 downto 0)   <= IR_F_D(31) & IR_F_D(7) & IR_F_D(30 downto 25) & IR_F_D(11 downto 8) ;
	ext4:   for i in 2*N-1 downto 12 generate
	            beq_imm_ext(i) <=IR_F_D(31);
				end generate;
    jal_imm (19 downto 0)    <= IR_F_D(31) & IR_F_D(19 downto 12) & IR_F_D(20) & IR_F_D(30 downto 21);
	 ext5:  for i in 2*N-1 downto 20 generate
	            jal_imm_ext(i) <=IR_F_D(31);
				end generate;
	sw_imm (11 downto 0)    <= IR_F_D(31 downto 25) & IR_F_D(11 downto 7);
	 ext6:  for i in 2*N-1 downto 12 generate
	            sw_imm_ext(i) <=IR_F_D(31);
				end generate;
	
	mux6_1: MUX61 generic map(64)
	              port map(srai_imm,  --SEL=110
				           i_lw_imm,    --101
                           auipc_lui_imm,  --100
						   beq_imm, --011
	                       jal_imm, -- 010
						   sw_imm,  --001
						   SEL,  
						   OUT_SIGN_EXT);

END STRUCT;