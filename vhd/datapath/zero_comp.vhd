library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity zero_comp is
	generic (N : integer := 64);
	port (RF_OUT_A : in	std_logic_vector(N-1 downto 0);
	        RF_OUT_B : in	std_logic_vector(N-1 downto 0);
				ZERO_OUT : out std_logic); --1 if result is zero ,0 if result different from 0
end zero_comp;

architecture struct of zero_comp is

	type matrix is array (0 to log2(N)-1) of std_logic_vector(N/2-1 downto 0);
	signal wires : matrix;
	signal res: std_logic_vector(N-1 downto 0);

begin

  res <= RF_OUT_A xnor RF_OUT_B;

	tree: for layer in 0 to log2(N)-1 generate
		layers: for elem in 0 to N/2**(layer+1)-1 generate
			first_layer: if layer = 0 generate
				or_row0: wires(0)(elem) <= res(elem*2) and res(elem*2+1);
			end generate; -- first layer
			other_layers: if layer /= 0 generate
				or_rows: wires(layer)(elem) <= wires(layer-1)(elem*2) and wires(layer-1)(elem*2+1);
			end generate; -- other layers
		end generate; -- layers
	end generate; -- tree

	ZERO_OUT <= wires(log2(N)-1)(0); 

end struct;