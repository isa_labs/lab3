library IEEE;
use IEEE.std_logic_1164.all;
use WORK.myTypes.all;

entity execute_stage is
	generic(regsize : integer := 32;
					opcode_length : integer := 7);
	port (     ID_EX_jal_out : in std_logic_vector(2*regsize - 1 downto 0);
	            ID_EX_PC_out : in std_logic_vector(2*regsize - 1 downto 0);
				OUT_d_e_IR : in std_logic_vector( regsize -1 downto 0);
				out_A : in std_logic_vector(2*regsize -1 downto 0);
				out_B : in std_logic_vector(2*regsize -1 downto 0);
				out_Imm : in std_logic_vector(2*regsize -1 downto 0);

				EX_MEM_ALU_OUT : in std_logic_vector(regsize-1 downto 0);
				MEM_WB_ALU_OUT : in std_logic_vector(regsize-1 downto 0);
				MEM_WB_LMD : in std_logic_vector(regsize-1 downto 0);
                EX_MEM_jal_out :in std_logic_vector(2*regsize-1 downto 0);
				MEM_WB_jal_out : in std_logic_vector(2*regsize-1 downto 0);
				
				ALU_opcode : in std_logic_vector(opcode_length -1 downto 0);
				ALU_rst : in std_logic;

				ex_mux : in std_logic;
				ALU_mux_bottom : in std_logic_vector(2 downto 0);
				ALU_mux_top : in std_logic_vector(2 downto 0);
				lui_auipc_mux : in std_logic_vector(1 downto 0);

				IN_ex_mem_jal : out std_logic_vector(2*regsize - 1 downto 0);
				IN_e_m_IR : out std_logic_vector(regsize -1 downto 0);
				out_ALU : out std_logic_vector(2*regsize - 1 downto 0);
				in_e_m_B : out std_logic_vector(2*regsize - 1 downto 0));

end execute_stage;

architecture struct of execute_stage is

	component MUX21_GENERIC is
		generic(NBIT : integer := 32);
		port (A   : in  std_logic_vector(NBIT-1 downto 0);
					B   : in  std_logic_vector(NBIT-1 downto 0);
					SEL : in  std_logic;
					Y   : out std_logic_vector(NBIT-1 downto 0));
	end component;

	component MUX31 is
		generic (n: integer := 32);
		port (IN1 :	in std_logic_vector(n-1 downto 0);
					IN2 :	in std_logic_vector(n-1 downto 0);
					IN3 :	in std_logic_vector(n-1 downto 0);
					S :	in std_logic_vector(1 downto 0);
					Y :	out	std_logic_vector(n-1 downto 0));
	end component;
	
	component MUX61 is
	generic(n : integer := 64);
	port (IN1 :	in	std_logic_vector(n-1 downto 0);
				IN2 :	in	std_logic_vector(n-1 downto 0);
				IN3 :	in	std_logic_vector(n-1 downto 0);
				IN4 :	in	std_logic_vector(n-1 downto 0);
				IN5 : in	std_logic_vector(n-1 downto 0);
				IN6 : in	std_logic_vector(n-1 downto 0);
				
				S :	in	std_logic_vector(2 downto 0);
				Y :	out	std_logic_vector(n-1 downto 0));
end component;

	component alu is
		generic(NBIT : integer := 64;
						OPCD : integer := 7);
		port (rst			: in 	std_logic;
					opcode	: in 	std_logic_vector(OPCD-1 downto 0);  -- if 0 add, if 1 sub
					A 		  : in 	std_logic_vector(NBIT-1 downto 0);
					B 		  : in 	std_logic_vector(NBIT-1 downto 0);
					result	: out std_logic_vector(NBIT-1 downto 0));
	end component;

	signal out_ex_mux : std_logic_vector(2*regsize-1 downto 0);
	signal IN_A_ALU : std_logic_vector(2*regsize-1 downto 0);
	signal IN_lui_auipc_mux : std_logic_vector(2*regsize-1 downto 0);
	signal IN_B_ALU : std_logic_vector(2*regsize-1 downto 0);
	signal zero_num: std_logic_vector(2*regsize-1 downto 0); 
	signal in_e_m_jal: std_logic_vector(2*regsize-1 downto 0); 
	signal in_ex_mem_B: std_logic_vector(2*regsize-1 downto 0); 

begin


 zero_num <= x"00000000";
 
	mux21 : MUX21_GENERIC
		generic map (2*regsize)
		port map (out_B, out_Imm, ex_mux, out_ex_mux);

	mux61_top : MUX61
		generic map (2*regsize)
		port map (EX_MEM_ALU_OUT, MEM_WB_ALU_OUT, MEM_WB_LMD,EX_MEM_jal_out,MEM_WB_jal_out, out_A, ALU_mux_top, IN_lui_auipc_mux);

	mux61_bottom : MUX61
		generic map (2*regsize)
		port map (out_ex_mux, EX_MEM_ALU_OUT, MEM_WB_ALU_OUT, MEM_WB_LMD,EX_MEM_jal_out,MEM_WB_jal_out, ALU_mux_bottom, IN_B_ALU);

    mux3_1: MUX31
		generic map (2*regsize)
		port map (zero_num,  ID_EX_PC_out, IN_lui_auipc_mux, lui_auipc_mux , IN_B_ALU);

	ALU_unit : alu
		generic map (2*regsize, opcode_length)
		port map (ALU_rst, ALU_opcode, IN_A_ALU, IN_B_ALU, out_ALU);

	
	IN_e_m_IR <=  OUT_d_e_IR;
	in_e_m_jal <= ID_EX_jal_out;
	IN_ex_mem_jal <= in_e_m_jal;
	in_e_m_B <= in_ex_mem_B;
	in_ex_mem_B <= out_B;

end struct;
