library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity fetch_stage is
	generic(N : integer := 32;
					IRAM_depth : integer := 64);
	port (clk : in std_logic;
				PC_rst : in std_logic;
				PC_en : in std_logic;
                ADD_CALCULATED_PC : in std_logic_vector(N-1 downto 0);
				if_mux_1 : in std_logic;

                PC: out std_logic_vector(2*N-1 downto 0);
				PC_plus_four : out std_logic_vector(N-1 downto 0);
				out_PC  :	out std_logic_vector(N-1 downto 0));  --goes into the input of the Instruction Ram	
end fetch_stage;

architecture arch of fetch_stage is

	component MUX21_GENERIC is
		generic(NBIT : integer := 32);
		port (A   : In  std_logic_vector(NBIT-1 downto 0);
					B   : In  std_logic_vector(NBIT-1 downto 0);
					SEL : In  std_logic;
					Y   : Out std_logic_vector(NBIT-1 downto 0));
	end component;


	component general_register is   --asynchronous N bit register,PC
		generic(N : integer := 8);
		Port (D :	in std_logic_vector(N-1 downto 0);
					clk :	in std_logic;
					rst :	in std_logic;
					en : in std_logic;
					Q :	out	std_logic_vector(N-1 downto 0));
	end component;

	component adder is
		generic(NBIT : integer := 32);
		port (A   :	In	std_logic_vector(NBIT-1 downto 0);
					B   :	In	std_logic_vector(NBIT-1 downto 0);
					Cin :	In	std_logic;
					S   : Out std_logic_vector(NBIT-1 downto 0);
					Co  : Out std_logic);
	end component;


 signal IN_PC: std_logic_vector(N-1 downto 0);
 signal OUT_PC_sig: std_logic_vector(N-1 downto 0);
signal OUT_adder: std_logic_vector(N-1 downto 0);
signal OUT_PC_extended: std_logic_vector(2*N-1 downto 0);
 signal four : std_logic_vector(N-1 downto 0);
signal cout: std_logic;

begin

	
	ProgCnt : general_register
		generic map (N)
		port map (IN_PC, clk, PC_rst, PC_en, out_PC_sig);
  	
	out_pc_extended <= x"00000000" & out_pc_sig;
	four <= x"00000004";


	fetch_adder : adder
		generic map (N)
		port map (four, out_PC_sig, '0', out_adder, Cout);

	if_mux_21 : MUX21_GENERIC
		generic map (N)
		port map (ADD_CALCULATED_PC, out_adder, if_mux_1, IN_PC);

	   PC <= out_PC_extended;
	   PC_plus_four <= out_adder;
	   out_PC <= out_PC_sig;

end arch;
