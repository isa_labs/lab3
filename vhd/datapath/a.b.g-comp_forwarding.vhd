library ieee;
use ieee.std_logic_1164.all;

entity comp_forwarding is
	generic(N : integer := 32;
					add_size : integer := 5);
	port (f_d_IR : in std_logic_vector(0 to N-1);
				d_e_IR : in std_logic_vector( N-1 downto 0);
				e_m_IR : in std_logic_vector(N-1 downto 0);
				m_w_IR : in std_logic_vector(N-1 downto 0);

				EM_Rd3_eq_FD_Rs1 : out std_logic;
				MW_Rd1_eq_FD_Rs1 : out std_logic;
				MW_Rd3_eq_FD_Rs1 : out std_logic;
				EM_Rd2_eq_FD_Rs1 : out std_logic;
				MW_Rd2_eq_FD_Rs1 : out std_logic;
				MW_Rd3_eq_EM_Rs2 : out std_logic;
				MW_Rd2_eq_EM_Rs2 : out std_logic;
				EM_Rd3_eq_DE_Rs1 : out std_logic; -- 1 if ex_mem_IR11..7 = id_ex_IR19..15
				EM_Rd3_eq_DE_Rs2 : out std_logic; -- 1 if ex_mem_IR11..7 = id_ex_IR24..20
				MW_Rd3_eq_DE_Rs1 : out std_logic;
				MW_Rd3_eq_DE_Rs2 : out std_logic;
				EM_Rd2_eq_DE_Rs1 : out std_logic;
				EM_Rd2_eq_DE_Rs2 : out std_logic;
				MW_Rd2_eq_DE_Rs1 : out std_logic;
				MW_Rd2_eq_DE_Rs2 : out std_logic;
				MW_Rd3_eq_EM_Rs1 : out std_logic;
				MW_Rd2_eq_EM_Rs1 : out std_logic;
				MW_Rd1_eq_EM_Rs1 : out std_logic; 
				
				EM_Rd3_eq_FD_Rs2 : out std_logic; 
				EM_Rd2_eq_FD_Rs2 : out std_logic;
				MW_Rd1_eq_FD_Rs2 : out std_logic; 
				MW_Rd3_eq_FD_Rs2 : out std_logic;
				MW_Rd2_eq_FD_Rs2 : out std_logic;
				DE_Rd3_eq_FD_Rs1: out std_logic;
				DE_Rd3_eq_FD_Rs2: out std_logic);
				
end comp_forwarding;

architecture behav of comp_forwarding is

	signal EM_Rd3_eq_FD_Rs1_vect : std_logic_vector( add_size - 1 downto 0);
	signal MW_Rd1_eq_FD_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd3_eq_FD_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal EM_Rd2_eq_FD_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd2_eq_FD_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd3_eq_EM_Rs2_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd2_eq_EM_Rs2_vect : std_logic_vector(add_size - 1 downto 0);
	signal EM_Rd3_eq_DE_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal EM_Rd3_eq_DE_Rs2_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd3_eq_DE_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd3_eq_DE_Rs2_vect : std_logic_vector(add_size - 1 downto 0);
	signal EM_Rd2_eq_DE_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal EM_Rd2_eq_DE_Rs2_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd2_eq_DE_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd2_eq_DE_Rs2_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd3_eq_EM_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd2_eq_EM_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd1_eq_EM_Rs1_vect : std_logic_vector(add_size - 1 downto 0);
	
	signal EM_Rd3_eq_FD_Rs2_vect :  std_logic_vector(add_size - 1 downto 0);
	signal EM_Rd2_eq_FD_Rs2_vect :  std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd1_eq_FD_Rs2_vect :  std_logic_vector(add_size - 1 downto 0); 
	signal MW_Rd3_eq_FD_Rs2_vect :  std_logic_vector(add_size - 1 downto 0);
	signal MW_Rd2_eq_FD_Rs2_vect :  std_logic_vector(add_size - 1 downto 0);
    signal DE_Rd3_eq_FD_Rs1_vect:   std_logic_vector(add_size - 1 downto 0);
	signal DE_Rd3_eq_FD_Rs2_vect:   std_logic_vector(add_size - 1 downto 0);

begin

	xnor_label : for i in 0 to add_size - 1 generate
		EM_Rd3_eq_FD_Rs1_vect(i) <= not(e_m_IR(i+7) xor f_d_IR(i+15));
		MW_Rd1_eq_FD_Rs1_vect(i) <= not(m_w_IR(i+7) xor f_d_IR(i+15));
		MW_Rd3_eq_FD_Rs1_vect(i) <= not(m_w_IR(i+7) xor f_d_IR(i+15));
		EM_Rd2_eq_FD_Rs1_vect(i) <= not(e_m_IR(i+7) xor f_d_IR(i+15));
		MW_Rd2_eq_FD_Rs1_vect(i) <= not(m_w_IR(i+7) xor f_d_IR(i+15));
		MW_Rd3_eq_EM_Rs2_vect(i) <= not(m_w_IR(i+7) xor e_m_IR(i+20));
		MW_Rd2_eq_EM_Rs2_vect(i) <= not(m_w_IR(i+7) xor e_m_IR(i+20));
		EM_Rd3_eq_DE_Rs1_vect(i) <= not(e_m_IR(i+7) xor d_e_IR(i+15));
		EM_Rd3_eq_DE_Rs2_vect(i) <= not(e_m_IR(i+7) xor d_e_IR(i+20));
		MW_Rd3_eq_DE_Rs1_vect(i) <= not(m_w_IR(i+7) xor d_e_IR(i+15));
		MW_Rd3_eq_DE_Rs2_vect(i) <= not(m_w_IR(i+7) xor d_e_IR(i+20));
		EM_Rd2_eq_DE_Rs1_vect(i) <= not(e_m_IR(i+7) xor d_e_IR(i+15));
		EM_Rd2_eq_DE_Rs2_vect(i) <= not(e_m_IR(i+7) xor d_e_IR(i+20));
		MW_Rd2_eq_DE_Rs1_vect(i) <= not(m_w_IR(i+7) xor d_e_IR(i+15));
		MW_Rd2_eq_DE_Rs2_vect(i) <= not(m_w_IR(i+7) xor d_e_IR(i+20));
		MW_Rd3_eq_EM_Rs1_vect(i) <= not(m_w_IR(i+7) xor e_m_IR(i+15));
		MW_Rd2_eq_EM_Rs1_vect(i) <= not(m_w_IR(i+7) xor e_m_IR(i+15));
		MW_Rd1_eq_EM_Rs1_vect(i) <= not(m_w_IR(i+7) xor e_m_IR(i+15));
       
	   EM_Rd3_eq_FD_Rs2_vect(i) <= not(e_m_IR(i+7) xor f_d_IR(i+20));
		EM_Rd2_eq_FD_Rs2_vect(i) <= not(e_m_IR(i+7) xor f_d_IR(i+20));
		MW_Rd1_eq_FD_Rs2_vect(i) <= not(m_w_IR(i+7) xor f_d_IR(i+20));
		MW_Rd3_eq_FD_Rs2_vect(i) <= not(m_w_IR(i+7) xor f_d_IR(i+20));
		MW_Rd2_eq_FD_Rs2_vect(i) <= not(m_w_IR(i+7) xor f_d_IR(i+20));
		DE_Rd3_eq_FD_Rs1_vect(i) <= not(d_e_IR(i+7) xor f_d_IR(i+20));
		DE_Rd3_eq_FD_Rs2_vect(i) <= not(d_e_IR(i+7) xor f_d_IR(i+20));
		
	end generate;


	EM_Rd3_eq_FD_Rs1 <= (((((EM_Rd3_eq_FD_Rs1_vect(0) and EM_Rd3_eq_FD_Rs1_vect(1)) and EM_Rd3_eq_FD_Rs1_vect(2)) and EM_Rd3_eq_FD_Rs1_vect(3)) and EM_Rd3_eq_FD_Rs1_vect(4)));
	MW_Rd1_eq_FD_Rs1 <= (((((MW_Rd1_eq_FD_Rs1_vect(0) and MW_Rd1_eq_FD_Rs1_vect(1)) and MW_Rd1_eq_FD_Rs1_vect(2)) and MW_Rd1_eq_FD_Rs1_vect(3)) and MW_Rd1_eq_FD_Rs1_vect(4)));
	MW_Rd3_eq_FD_Rs1 <= (((((MW_Rd3_eq_FD_Rs1_vect(0) and MW_Rd3_eq_FD_Rs1_vect(1)) and MW_Rd3_eq_FD_Rs1_vect(2)) and MW_Rd3_eq_FD_Rs1_vect(3)) and MW_Rd3_eq_FD_Rs1_vect(4)));
	EM_Rd2_eq_FD_Rs1 <= (((((EM_Rd2_eq_FD_Rs1_vect(0) and EM_Rd2_eq_FD_Rs1_vect(1)) and EM_Rd2_eq_FD_Rs1_vect(2)) and EM_Rd2_eq_FD_Rs1_vect(3)) and EM_Rd2_eq_FD_Rs1_vect(4)));
	MW_Rd2_eq_FD_Rs1 <= (((((MW_Rd2_eq_FD_Rs1_vect(0) and MW_Rd2_eq_FD_Rs1_vect(1)) and MW_Rd2_eq_FD_Rs1_vect(2)) and MW_Rd2_eq_FD_Rs1_vect(3)) and MW_Rd2_eq_FD_Rs1_vect(4)));
	MW_Rd3_eq_EM_Rs2 <= (((((MW_Rd3_eq_EM_Rs2_vect(0) and MW_Rd3_eq_EM_Rs2_vect(1)) and MW_Rd3_eq_EM_Rs2_vect(2)) and MW_Rd3_eq_EM_Rs2_vect(3)) and MW_Rd3_eq_EM_Rs2_vect(4)));
	MW_Rd2_eq_EM_Rs2 <= (((((MW_Rd2_eq_EM_Rs2_vect(0) and MW_Rd2_eq_EM_Rs2_vect(1)) and MW_Rd2_eq_EM_Rs2_vect(2)) and MW_Rd2_eq_EM_Rs2_vect(3)) and MW_Rd2_eq_EM_Rs2_vect(4)));
	EM_Rd3_eq_DE_Rs1 <= (((((EM_Rd3_eq_DE_Rs1_vect(0) and EM_Rd3_eq_DE_Rs1_vect(1)) and EM_Rd3_eq_DE_Rs1_vect(2)) and EM_Rd3_eq_DE_Rs1_vect(3)) and EM_Rd3_eq_DE_Rs1_vect(4)));
	EM_Rd3_eq_DE_Rs2 <= (((((EM_Rd3_eq_DE_Rs2_vect(0) and EM_Rd3_eq_DE_Rs2_vect(1)) and EM_Rd3_eq_DE_Rs2_vect(2)) and EM_Rd3_eq_DE_Rs2_vect(3)) and EM_Rd3_eq_DE_Rs2_vect(4)));
	MW_Rd3_eq_DE_Rs1 <= (((((MW_Rd3_eq_DE_Rs1_vect(0) and MW_Rd3_eq_DE_Rs1_vect(1)) and MW_Rd3_eq_DE_Rs1_vect(2)) and MW_Rd3_eq_DE_Rs1_vect(3)) and MW_Rd3_eq_DE_Rs1_vect(4)));
	MW_Rd3_eq_DE_Rs2 <= (((((MW_Rd3_eq_DE_Rs2_vect(0) and MW_Rd3_eq_DE_Rs2_vect(1)) and MW_Rd3_eq_DE_Rs2_vect(2)) and MW_Rd3_eq_DE_Rs2_vect(3)) and MW_Rd3_eq_DE_Rs2_vect(4)));
	EM_Rd2_eq_DE_Rs1 <= (((((EM_Rd2_eq_DE_Rs1_vect(0) and EM_Rd2_eq_DE_Rs1_vect(1)) and EM_Rd2_eq_DE_Rs1_vect(2)) and EM_Rd2_eq_DE_Rs1_vect(3)) and EM_Rd2_eq_DE_Rs1_vect(4)));
	EM_Rd2_eq_DE_Rs2 <= (((((EM_Rd2_eq_DE_Rs2_vect(0) and EM_Rd2_eq_DE_Rs2_vect(1)) and EM_Rd2_eq_DE_Rs2_vect(2)) and EM_Rd2_eq_DE_Rs2_vect(3)) and EM_Rd2_eq_DE_Rs2_vect(4)));
	MW_Rd2_eq_DE_Rs1 <= (((((MW_Rd2_eq_DE_Rs1_vect(0) and MW_Rd2_eq_DE_Rs1_vect(1)) and MW_Rd2_eq_DE_Rs1_vect(2)) and MW_Rd2_eq_DE_Rs1_vect(3)) and MW_Rd2_eq_DE_Rs1_vect(4)));
	MW_Rd2_eq_DE_Rs2 <= (((((MW_Rd2_eq_DE_Rs2_vect(0) and MW_Rd2_eq_DE_Rs2_vect(1)) and MW_Rd2_eq_DE_Rs2_vect(2)) and MW_Rd2_eq_DE_Rs2_vect(3)) and MW_Rd2_eq_DE_Rs2_vect(4)));
	MW_Rd3_eq_EM_Rs1 <= (((((MW_Rd3_eq_EM_Rs1_vect(0) and MW_Rd3_eq_EM_Rs1_vect(1)) and MW_Rd3_eq_EM_Rs1_vect(2)) and MW_Rd3_eq_EM_Rs1_vect(3)) and MW_Rd3_eq_EM_Rs1_vect(4)));
	MW_Rd2_eq_EM_Rs1 <= (((((MW_Rd2_eq_EM_Rs1_vect(0) and MW_Rd2_eq_EM_Rs1_vect(1)) and MW_Rd2_eq_EM_Rs1_vect(2)) and MW_Rd2_eq_EM_Rs1_vect(3)) and MW_Rd2_eq_EM_Rs1_vect(4)));
	MW_Rd1_eq_EM_Rs1 <= (((((MW_Rd1_eq_EM_Rs1_vect(0) and MW_Rd1_eq_EM_Rs1_vect(1)) and MW_Rd1_eq_EM_Rs1_vect(2)) and MW_Rd1_eq_EM_Rs1_vect(3)) and MW_Rd1_eq_EM_Rs1_vect(4)));
    
	EM_Rd3_eq_FD_Rs2 <= (((((EM_Rd3_eq_FD_Rs2_vect(0) and EM_Rd3_eq_FD_Rs2_vect(1)) and EM_Rd3_eq_FD_Rs2_vect(2)) and EM_Rd3_eq_FD_Rs2_vect(3)) and EM_Rd3_eq_FD_Rs2_vect(4)));
	EM_Rd2_eq_FD_Rs2 <= (((((EM_Rd2_eq_FD_Rs2_vect(0) and EM_Rd2_eq_FD_Rs2_vect(1)) and EM_Rd2_eq_FD_Rs2_vect(2)) and EM_Rd2_eq_FD_Rs2_vect(3)) and EM_Rd2_eq_FD_Rs2_vect(4)));
	MW_Rd1_eq_FD_Rs2 <= (((((MW_Rd1_eq_FD_Rs2_vect(0) and MW_Rd1_eq_FD_Rs2_vect(1)) and MW_Rd1_eq_FD_Rs2_vect(2)) and MW_Rd1_eq_FD_Rs2_vect(3)) and MW_Rd1_eq_FD_Rs2_vect(4)));
    MW_Rd3_eq_FD_Rs2 <= (((((MW_Rd3_eq_FD_Rs2_vect(0) and MW_Rd3_eq_FD_Rs2_vect(1)) and MW_Rd3_eq_FD_Rs2_vect(2)) and MW_Rd3_eq_FD_Rs2_vect(3)) and MW_Rd3_eq_FD_Rs2_vect(4)));
	MW_Rd2_eq_FD_Rs2 <= (((((MW_Rd2_eq_FD_Rs2_vect(0) and MW_Rd2_eq_FD_Rs2_vect(1)) and MW_Rd2_eq_FD_Rs2_vect(2)) and MW_Rd2_eq_FD_Rs2_vect(3)) and MW_Rd2_eq_FD_Rs2_vect(4)));
    DE_Rd3_eq_FD_Rs1 <= (((((DE_Rd3_eq_FD_Rs1_vect(0) and DE_Rd3_eq_FD_Rs1_vect(1)) and DE_Rd3_eq_FD_Rs1_vect(2)) and DE_Rd3_eq_FD_Rs1_vect(3)) and DE_Rd3_eq_FD_Rs1_vect(4)));
    DE_Rd3_eq_FD_Rs2 <= (((((DE_Rd3_eq_FD_Rs2_vect(0) and DE_Rd3_eq_FD_Rs2_vect(1)) and DE_Rd3_eq_FD_Rs2_vect(2)) and DE_Rd3_eq_FD_Rs2_vect(3)) and DE_Rd3_eq_FD_Rs2_vect(4))); 
end behav;