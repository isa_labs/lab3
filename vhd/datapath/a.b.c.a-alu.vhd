library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity alu is
	generic(NBIT : integer := 64;
					OPCD : integer := 4); --alu opcode size
	port (rst			: in 	std_logic; -- active low
		opcode	: in 	std_logic_vector(OPCD-1 downto 0);  -- if 0 add, if 1 sub
		A 		  : in 	std_logic_vector(NBIT-1 downto 0);
		B 		  : in 	std_logic_vector(NBIT-1 downto 0);
				result	: out std_logic_vector(NBIT-1 downto 0));
end alu;

architecture struct of alu is

	component log_shifter is
		generic (N : integer := 64);
		port (A     : in  std_logic_vector(N-1 downto 0);
					shift : in  std_logic_vector(log2(N)-1 downto 0);
					LR    : in  std_logic; -- 1 for left shifts; 0 for right shifts
					AL    : in  std_logic; -- 1 for arithmetic shifts; 0 for logic shifts
					res   : out std_logic_vector(N-1 downto 0));
	end component;

	component enabler_array is
		generic (N : integer :=64);
		port (D   :	In	std_logic_vector(N-1 downto 0);
					rst : in  std_logic; -- active low
					en  :	In	std_logic;
					Q   :	Out	std_logic_vector(N-1 downto 0));
	end component;

	component LOGIC_UNIT IS 
		GENERIC(N : INTEGER := 64);
		     PORT (IN1: IN STD_LOGIC_VECTOR (N-1 DOWNTO 0);
					IN2:IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
					CHOOSE:IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
					OUTPUT: OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0));
	end component;
	
	component adder_comparator is
		generic (N : integer :=64);
		port (add_comp			: in 	std_logic;	-- if 0 add, if 1 compare
					sign				: in	std_logic; 	-- if 1 the numbers are signed, 0 if unsigned
					bigger				: in 	std_logic;
					smaller				: in 	std_logic;
					equal					: in 	std_logic;
					sub 					: in 	std_logic;  -- if 0 add, if 1 sub
					in1 		: in 	std_logic_vector(N-1 downto 0);
					in2 		: in 	std_logic_vector(N-1 downto 0);
					result	: out std_logic_vector(N-1 downto 0));
	end component;
	

	signal en_AC, en_LU, en_SH : std_logic; -- enable signals sent to the enablers

	signal A_AC, B_AC : std_logic_vector(NBIT-1 downto 0); -- inputs to the adder_comparator from the enablers
	signal oc_AC : std_logic_vector(OPCD-3 downto 0);
	signal out_AC : std_logic_vector(NBIT-1 downto 0); -- output from the adder_comparator to the muxes

	signal AC, sub, bg, eq, lo, su : std_logic; -- control signals for the adder_comparator

	signal A_LU, B_LU : std_logic_vector(NBIT-1 downto 0); -- inputs to the logic unit from the enablers
	signal oc_LU : std_logic_vector(OPCD-5 downto 0);
	signal out_LU : std_logic_vector(NBIT-1 downto 0); -- output from the logic unit to the muxes

	signal A_SH : std_logic_vector(NBIT-1 downto 0);
	signal B_SH : std_logic_vector(log2(NBIT)-1 downto 0); -- inputs to the shifter from the enablers
	signal oc_SH : std_logic_vector(OPCD-5 downto 0);
	signal out_SH : std_logic_vector(NBIT-1 downto 0); -- output from the shifter to the muxes

	signal sel: std_logic_vector(1 downto 0);
	
	constant zero : std_logic_vector( NBIT-1 downto 0) 	:= x"0000000000000000";

	BEGIN 

	en_AC <= not opcode(OPCD-1) and opcode(OPCD-2);     -- the enables are obtained directly from the 2 MSBs of the alu opcode
	en_LU <= opcode(OPCD-1) and opcode(OPCD-2);
	en_SH <= opcode(OPCD-1) and not opcode(OPCD-2);

	-- adder/comparator
	
	enabler_AC_A: enabler_array 
		generic map (NBIT)
		port map (A, rst, en_AC, A_AC);

	enabler_AC_B: enabler_array 
		generic map (NBIT)
		port map (B, rst, en_AC, B_AC);
													
	enabler_AC_oc: enabler_array 
		generic map (OPCD-2)
		port map (opcode(OPCD-3 downto 0), rst, en_AC, oc_AC);

	AC <= not oc_AC(1); -- 0 for adder/subtractor, 1 for comparator
	bg <= '0';
	lo <= not oc_AC(1) and oc_AC(0);
	eq <= '0';

	add_cmp: adder_comparator 
		generic map (NBIT)
		port map (AC, '1', bg, lo, eq, oc_AC(0), A_AC, B_AC, out_AC); -- only signed operations considered

	-- logic unit
	
	enabler_LU_A: enabler_array 
		generic map (NBIT)
		port map (A, rst, en_LU, A_LU);

	enabler_LU_B: enabler_array 
		generic map (NBIT)
		port map (B, rst, en_LU, B_LU);

	enabler_LU_oc: enabler_array 
		generic map (OPCD-2)
		port map (opcode(OPCD-3 downto 0), rst, en_LU, oc_LU); -- choose is obtained from the 2 LSBs of the opcode

	log_un: LOGIC_UNIT 	
		generic map (NBIT)
		port map (A_LU, B_LU, oc_LU, out_LU); -- choose is obtained from the 2 LSBs of the opcode

	-- logarithmic shifter
	
	enabler_SH_A: enabler_array 
		generic map (NBIT)
		port map (A, rst, en_SH, A_SH);

	enabler_SH_B: enabler_array generic map (log2(NBIT)-1)
													port map (B(log2(NBIT)-2 downto 0), rst, en_SH, B_SH);

--	enabler_SH_oc: enabler_array 
--		generic map (OPCD-2)
--		port map (opcode(OPCD-3 downto 0), rst, en_SH, oc_SH);

	log_sh: log_shifter 
		generic map (NBIT)
		port map (A_SH, B_SH, '0' , '1' , out_SH); -- only right arithmetic shift of 32 positions max

	-- exit selection
	
	WITH sel SELECT
			          result <= out_LU WHEN "11",
								out_AC WHEN "01",
								out_SH WHEN "10",
								zero WHEN OTHERS;
			



END struct;
