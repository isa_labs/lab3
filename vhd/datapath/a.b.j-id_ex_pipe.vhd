library IEEE;
use IEEE.std_logic_1164.all;

entity id_ex_pipe is
	generic(regsize : integer := 32;
					opcode_size : integer := 7;
					funct3_size : integer := 3;
					funct7_size : integer := 7);
					
	port (
				in_d_e_IR : in std_logic_vector(regsize - 1 downto 0);
				IN_REG_A : in std_logic_vector(2*regsize-1 downto 0);
				IN_REG_B : in std_logic_vector(2*regsize-1 downto 0);
				IN_REG_IMM : in std_logic_vector(2*regsize-1 downto 0);
				in_d_e_PC: in std_logic_vector(2*regsize-1 downto 0);
				in_d_e_jal: in std_logic_vector(2*regsize-1 downto 0);
				
				clk : in std_logic;
				d_e_rst : in std_logic;
				d_e_en : in std_logic;

                out_d_e_PC: out std_logic_vector(2*regsize-1 downto 0);
				out_d_e_jal: out std_logic_vector(2*regsize-1 downto 0);
				out_d_e_IR : out std_logic_vector( regsize - 1 downto 0);
				OUT_REG_A : out std_logic_vector(2*regsize-1 downto 0);
				OUT_REG_B : out std_logic_vector(2*regsize-1 downto 0);
				OUT_REG_IMM : out std_logic_vector(2*regsize-1 downto 0);
				out_id_ex_opcode : out std_logic_vector( opcode_size -1 downto 0);
				out_id_ex_funct3 : out std_logic_vector( funct3_size -1 downto 0);
				out_id_ex_funct7 : out std_logic_vector( funct7_size -1 downto 0)
				);
	 end id_ex_pipe;

 architecture arch of id_ex_pipe is
			 
	component general_register  is   --asynchronous N bit register
		generic(N : integer := 32);
		port (D :	in std_logic_vector(N-1 downto 0);
			clk :	in std_logic;
			rst :	in std_logic;
			en : in	std_logic;
			Q :	out	std_logic_vector(N-1 downto 0));
	end component;

	signal out_id_ex_IR : std_logic_vector( regsize - 1 downto 0);

begin 
		 
	d_e_PC : general_register  
		generic map (2*regsize)
		port map (in_d_e_PC,	clk, d_e_rst, d_e_en, out_d_e_PC);
	
	d_e_jal : general_register  
		generic map (2*regsize)
		port map (in_d_e_jal,	clk, d_e_rst, d_e_en, out_d_e_jal);
		
	d_e_IR : general_register  
		generic map (regsize)
		port map (in_d_e_IR, clk, d_e_rst, d_e_en, out_id_ex_IR);

	regA : general_register  
		generic map (2*regsize)
		port map (IN_REG_A, clk, d_e_rst, d_e_en, OUT_REG_A);
	 
	regB : general_register  
		generic map (2*regsize)
		port map (IN_REG_B, clk, d_e_rst, d_e_en, OUT_REG_B);

	Immediate : general_register  
		generic map (2*regsize)
		port map (IN_REG_IMM, clk, d_e_rst, d_e_en, OUT_REG_IMM);  
			
	out_d_e_IR <= out_id_ex_IR;  
	out_id_ex_opcode <= out_id_ex_IR(opcode_size -1 downto 0);
	out_id_ex_funct3 <= out_id_ex_IR(14 downto 12);
	out_id_ex_funct7 <= out_id_ex_IR(31 downto 25);
	

end arch;