library ieee; 
use ieee.std_logic_1164.all; 
 
entity csb is
	generic(NBIT : integer := 4);
	port (A  : In	 std_logic_vector(NBIT-1 downto 0);
				B  : In	 std_logic_vector(NBIT-1 downto 0);
				Ci : In	 std_logic;
				S  : Out std_logic_vector(NBIT-1 downto 0));
end csb; 

architecture STRUCTURAL of csb is

	component RCA_GEN
		generic(NBIT  : integer := 4);
		port (A  : In	 std_logic_vector(NBIT-1 downto 0);
					B  : In	 std_logic_vector(NBIT-1 downto 0);
					Ci : In	 std_logic;
					S  : Out std_logic_vector(NBIT-1 downto 0);
					Co : Out std_logic);
	end component;

	component MUX21_GENERIC
		generic(NBIT : integer := 4);
		port (A   : In  std_logic_vector(NBIT-1 downto 0);
					B   : In  std_logic_vector(NBIT-1 downto 0);
					SEL : In  std_logic;
					Y   : Out std_logic_vector(NBIT-1 downto 0));
	end component;

	signal cout : std_logic_vector(1 downto 0);
	signal S0   : std_logic_vector(NBIT-1 downto 0);
	signal S1   : std_logic_vector(NBIT-1 downto 0);

 begin

	RCA0 : RCA_GEN
		generic map (NBIT)
		port map (A, B, '0', S0, cout(0));
	
	RCA1 : RCA_GEN
		generic map (NBIT)
		port map (A, B, '1', S1, cout(1));
	
	MUX : MUX21_GENERIC
		generic map (NBIT)
		port map (S1, S0, Ci, S);
	 
end STRUCTURAL;

