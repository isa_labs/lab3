library ieee; 
use ieee.std_logic_1164.all; 
use WORK.myTypes.all;

entity CG is
	generic(NBIT : integer := 32;
					DIV  : integer := 4);
	port (A   :	In	std_logic_vector(NBIT-1 downto 0);
				B   :	In	std_logic_vector(NBIT-1 downto 0);
				Cin : in  std_logic;
				C   :	Out	std_logic_vector(NBIT/4-1 downto 0));
end CG; 

architecture STRUCTURAL of CG is

	type matrix is array (0 to log2(NBIT)) of std_logic_vector(NBIT-1 downto 0);

	signal p, g : matrix := (others => (others => '0'));

	component G_block 
		port (Pik   : in  std_logic;
					Gik   : in  std_logic;
					Gk_1j : in  std_logic;
					Gij   : out std_logic);
	end component;

	component PG_block
		port (Pik   : in  std_logic;
					Gik   : in  std_logic;
					Pk_1j : in  std_logic;
					Gk_1j : in  std_logic;
					Pij   : out std_logic;
					Gij   : out std_logic);
	end component;

	component PG_gate 
		port (A	: in  std_logic;
					B	: in  std_logic;
					P : out std_logic;
					G	: out std_logic);
	end component;

	component PG_cin_gate 
		port (A   : in  std_logic;
					B   : in  std_logic;
					cin : in  std_logic;
					G   : out std_logic);
	end component;

	begin

		sparse_tree: for row in 0 to log2(NBIT) generate
			-- the first part of the sparse tree is the row of gates used to generate the p and g signals
			-- the first gate also contains a G block in order to propagate Cin
			gate_row: if row = 0 generate
				first_gate: PG_cin_gate port map (A(0), B(0), cin, g(0)(0));
				other_gates: for column in 1 to NBIT-1 generate
					gate_row: PG_gate port map (A(column), B(column), p(0)(column), g(0)(column));
				end generate; -- other_gates
			end generate; -- gate_row

			-- in the first part of the tree, the step (computed as 2^row-2^(row-1) = 2^(row-1)) is lower than the 
			-- required step, DIV. Here there is only one G block and afterwards a PG block is added every 2^row columns.
			-- when 2^row is bigger than DIV, the P and G signals from the previous row must be propagated where the PG 
			-- blocks are missing; this is done in the top_holes section. Carries coming from the G block must be selected
			-- in the connection section.
			top_tree: if row > 0 and 2**(row-1) <= DIV generate
				first_G: G_block port map (p(row-1)(2**row-1), g(row-1)(2**row-1), g(row-1)(2**(row-1)-1), g(row)(2**row-1));
				other_PG: for column in 2**row to NBIT-1 generate
					con1: if ((column+1) mod (2**row)) = 0 generate
						PG_row: PG_block port map (p(row-1)(column), g(row-1)(column), p(row-1)(column-2**(row-1)), g(row-1)(column-2**(row-1)), p(row)(column), g(row)(column));
					end generate; -- con1
					con1_1: if ((column+1) mod (2**row)) /= 0 and ((column+1) mod DIV) = 0 generate
						top_holes: p(row)(column) <= p(row-1)(column);
											 g(row)(column) <= g(row-1)(column);
					end generate; -- con1_1
				end generate; -- other_PG
				top_carry: if ((2**row) mod DIV) = 0 generate
					connection: C(row-2) <= g(row)(2**row-1);
				end generate; -- top_carry
			end generate; -- top_tree

			-- when the step is higher than DIV, the second part of the tree is generated. Here there are fewer PG blocks and 
			-- more G blocks. PG blocks are still generated once every DIV columns but starting from the 2^row+2^(row-1)+DIVth column,
			-- while G blocks are generated once every DIV columns from the 2^(row-1)th column to the 2^(row)+2^(row-1)th column.
			-- here too the P and G signals must be propagated where there are no blocks. this is done in the bottom_holes section.
			-- the connections from the G blocks to the carry are done in the bottom_carry section
			bottom_tree: if row > 0 and 2**(row-1) > DIV generate
				bottom_rows: for column in 0 to NBIT-1 generate
					con2: if column mod DIV = 3 generate
						Gcon: if column > 2**(row-1) and column < 2**row generate
							G_bottom: G_block port map (p(row-1)(column), g(row-1)(column), g(row-1)(2**(row-1)-1), g(row)(column));
							bottom_carry: C((column+1)/DIV-1) <= g(row)(column);
						end generate; -- Gcon
						Pcon: if column > (2**row + 2**(row-1)) generate
							PG_bottom: PG_block port map (p(row-1)(column), g(row-1)(column), p(row-1)(2**row + 2**(row-1) - 1), g(row-1)(2**row + 2**(row-1) - 1), p(row)(column), g(row)(column));
						end generate; -- Pcon
						Hcon: if column > 2**row and column < (2**row + 2**(row-1)) generate
							bottom_holes: p(row)(column) <= p(row-1)(column);
														g(row)(column) <= g(row-1)(column);
						end generate; -- Hcon
					end generate; -- con2
				end generate; -- bottom_rows
			end generate; -- bottom_tree

		end generate; -- sparse_tree

end STRUCTURAL;
