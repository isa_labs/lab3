library ieee;
use ieee.std_logic_1164.all;

entity adder_comparator is
	generic (N : integer := 32);
	port (add_comp			: in 	std_logic;	-- if 0 add, if 1 compare
				sign				: in	std_logic; 	-- if 1 the numbers are signed, 0 if unsigned
				bigger				: in 	std_logic;
				smaller				: in 	std_logic;
				equal					: in 	std_logic;
				sub 					: in 	std_logic;  -- if 0 add, if 1 sub
				in1 		: in 	std_logic_vector(N-1 downto 0);
				in2 		: in 	std_logic_vector(N-1 downto 0);
				result	: out std_logic_vector(N-1 downto 0));
end adder_comparator;

architecture struct of adder_comparator is

	component AS is
		generic(NBIT : integer := 32);
		port (A   :	In	std_logic_vector(NBIT-1 downto 0);
					B   :	In	std_logic_vector(NBIT-1 downto 0);
					sub :	In	std_logic;
					S   : Out std_logic_vector(NBIT-1 downto 0);
					Co  : Out std_logic);
	end component; 

	component zero_comp is
		generic (N : integer := 32 );
		port( RF_OUT_A : in	std_logic_vector(N-1 downto 0);
					ZERO_OUT	: out std_logic); --1 if result is zero ,0 if result different from 0
	end component;

	component MUX21_GENERIC is
		generic(NBIT : integer := 32);
		port (A   : In  std_logic_vector(NBIT-1 downto 0);
					B   : In  std_logic_vector(NBIT-1 downto 0);
					SEL : In  std_logic;
					Y   : Out std_logic_vector(NBIT-1 downto 0));
	end component;

	component MUX21 is
		port (A :	In	std_logic;
					B :	In	std_logic;
					S :	In	std_logic;
					Y :	Out	std_logic);
	end component;

	signal c				: std_logic;
	signal cin			: std_logic;
	signal cout 		: std_logic;
	signal sum 			: std_logic_vector(N-1 downto 0);
	signal response	: std_logic_vector(N-1 downto 0);
	signal zero 		: std_logic;
	signal big, sml, eq 	: std_logic;

	BEGIN 

		add_sub: AS generic map(N)
								port map(IN1, IN2, cin, SUM, COUT);

		comparator: zero_comp generic map(N)
													port map(SUM, ZERO);

		mux_cin: MUX21 port map('1', sub, add_comp, cin);

		mux_res: MUX21_GENERIC 	generic map(N)
														port map(response, sum, add_comp, result);
		
		c <= (not sign and cout) or (sign and cout and not in1(N-1) and not in2(N-1)) or (sign and cout and in1(N-1) and in2(N-1)) or (sign and not cout and not in1(N-1) and in2(N-1)) or (sign and not cout and in1(N-1) and not in2(N-1));
				
		sml <= not c;
		big  <= not zero and c;
		eq <= zero;

		response <= (0 => ((sml and smaller) or (big and bigger) or (eq and equal)), others => '0');

END struct;
