library ieee; 
use ieee.std_logic_1164.all; 

entity SG is
	generic(NBperblock : integer := 4;
					Nblock 		 : integer := 8);
	port (A  : In	 std_logic_vector(NBperblock*Nblock-1 downto 0);
				B  : In	 std_logic_vector(NBperblock*Nblock-1 downto 0);
				Ci : In	 std_logic_vector(Nblock-1 downto 0);
				S  : Out std_logic_vector(NBperblock*Nblock-1 downto 0));
end SG; 

architecture STRUCTURAL of SG is

	component csb
		generic(NBIT : integer := 4);
		port (A  : In	 std_logic_vector(NBIT-1 downto 0);
					B  : In	 std_logic_vector(NBIT-1 downto 0);
					Ci : In	 std_logic;
					S  : Out std_logic_vector(NBIT-1 downto 0));
	end component;

	begin

		sum_gen : for i in 0 to Nblock-1 generate
			csb_arr : csb port map (A((i+1)*NBperblock-1 downto i*NBperblock), B((i+1)*NBperblock-1 downto i*NBperblock), Ci(i), S((i+1)*NBperblock-1 downto i*NBperblock));
		end generate;

end STRUCTURAL;
