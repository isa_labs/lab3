library ieee; 
use ieee.std_logic_1164.all; 

entity PG_block is
	port (Pik   : in  std_logic;
				Gik   : in  std_logic;
				Pk_1j : in  std_logic;
				Gk_1j : in  std_logic;
				Pij   : out std_logic;
				Gij   : out std_logic);
end PG_block;

architecture BEHAVIORAL of PG_block is
	begin
		
		Pij <= Pik AND Pk_1j;
		Gij <= Gik OR (Pik AND Gk_1j);

end BEHAVIORAL;
