library ieee; 
use ieee.std_logic_1164.all; 

entity PG_gate is
	port (A   : in  std_logic;
				B   : in  std_logic;
				P   : out std_logic;
				G   : out std_logic);
end PG_gate;

architecture BEHAVIORAL of PG_gate is
	begin
		
		P <= A XOR B;
		G <= A AND B;

end BEHAVIORAL;

