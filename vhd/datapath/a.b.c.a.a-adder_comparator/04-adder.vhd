library ieee; 
use ieee.std_logic_1164.all; 

entity adder is
	generic(NBIT : integer := 32);
	port (A   :	In	std_logic_vector(NBIT-1 downto 0);
				B   :	In	std_logic_vector(NBIT-1 downto 0);
				Cin :	In	std_logic;
				S   : Out std_logic_vector(NBIT-1 downto 0);
				Co  : Out std_logic);
end adder; 

architecture STRUCTURAL of adder is

	component CG 
		generic(NBIT : integer := 32);
		port (A   :	In	std_logic_vector(NBIT-1 downto 0);
					B   :	In	std_logic_vector(NBIT-1 downto 0);
					Cin : In  std_logic;
					C   :	Out	std_logic_vector(NBIT/4-1 downto 0));
	end component;

	component SG 
		generic(NBperblock : integer := 4;
						Nblock     : integer := 8);
		port (A  : In	 std_logic_vector(NBperblock*Nblock-1 downto 0);
					B  : In	 std_logic_vector(NBperblock*Nblock-1 downto 0);
					Ci : In	 std_logic_vector(Nblock-1 downto 0);
					S  : Out std_logic_vector(NBperblock*Nblock-1 downto 0));
	end component;

	signal carry_out, carry_in : std_logic_vector (NBIT/4-1 downto 0);
	
	begin

		CLA_CG : CG port map (A, B, Cin, carry_out);
		Co <= carry_out(NBIT/4-1);
		carry_in <= carry_out(NBIT/4-2 downto 0) & Cin;
		RCA_SG : SG port map (A, B, carry_in, S);
 
end STRUCTURAL;


