library ieee; 
use ieee.std_logic_1164.all; 

entity G_block is
	port (Pik   : in  std_logic;
				Gik   : in  std_logic;
				Gk_1j : in  std_logic;
				Gij   : out std_logic);
end G_block;

architecture BEHAVIORAL of G_block is
	begin
		
		Gij <= Gik OR (Pik AND Gk_1j);
		
end BEHAVIORAL;

