library ieee; 
use ieee.std_logic_1164.all; 

entity PG_cin_gate is
 	port (A   : in  std_logic;
				B   : in  std_logic;
				cin : in  std_logic;
				G   : out std_logic);
end PG_cin_gate;

architecture BEHAVIORAL of PG_cin_gate is
	begin
		
	G <= (A AND B) OR ((A XOR B) AND cin);

end BEHAVIORAL;
