library ieee; 
use ieee.std_logic_1164.all; 

entity AS is
	generic(NBIT : integer := 32);
	port (A   :	In	std_logic_vector(NBIT-1 downto 0);
				B   :	In	std_logic_vector(NBIT-1 downto 0);
				sub :	In	std_logic;
				S   : Out std_logic_vector(NBIT-1 downto 0);
				Co  : Out std_logic);
end AS; 

architecture STRUCTURAL of AS is

	component adder 
		generic(NBIT : integer := 32);
		port (A   :	In	std_logic_vector(NBIT-1 downto 0);
					B   :	In	std_logic_vector(NBIT-1 downto 0);
					Cin :	In	std_logic;
					S   : Out std_logic_vector(NBIT-1 downto 0);
					Co  : Out std_logic);
	end component;

	component MUX21_GENERIC
		generic(NBIT : integer := 4);
		port (A   : In  std_logic_vector(NBIT-1 downto 0);
					B   : In  std_logic_vector(NBIT-1 downto 0);
					SEL : In  std_logic;
					Y   : Out std_logic_vector(NBIT-1 downto 0));
	end component;

	signal Bnew : std_logic_vector(NBIT-1 downto 0);
	signal Bnot : std_logic_vector(NBIT-1 downto 0);

	begin

		Bnot <= not B;

		MUX_B: MUX21_GENERIC	
			generic map (NBIT)
			port map (Bnot, B, sub, Bnew);

		ADD: adder	
			generic map (NBIT)
			port map (A, Bnew, sub, S, Co);
 
end STRUCTURAL;
