library IEEE;
use IEEE.std_logic_1164.all;

entity MUX51 is
	generic(n : integer := 32);
	port (IN1 :	in	std_logic_vector(n-1 downto 0);
				IN2 :	in	std_logic_vector(n-1 downto 0);
				IN3 :	in	std_logic_vector(n-1 downto 0);
				IN4 :	in	std_logic_vector(n-1 downto 0);
				IN5 : in	std_logic_vector(n-1 downto 0);
				S :	in	std_logic_vector(2 downto 0);
				Y :	out	std_logic_vector(n-1 downto 0));
end MUX51;

architecture BEHAV of MUX51 is
	BEGIN
		WITH S SELECT
			Y <= 	IN1 WHEN "000",
						IN2 WHEN "001",
						IN3 WHEN "010",
						IN4 WHEN "011",
						IN5 WHEN "111",
						(OTHERS => '0') WHEN OTHERS;
		
end BEHAV;
