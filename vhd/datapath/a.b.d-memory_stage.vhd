library ieee;
use ieee.std_logic_1164.all;

entity memory_stage is
	generic(regsize : integer := 32);
	port (
				OUT_e_m_IR : in std_logic_vector( regsize - 1 downto 0);
				out_ALUOUT : in std_logic_vector( 2*regsize - 1 downto 0);
				out_e_m_B : in std_logic_vector( 2*regsize - 1 downto 0);
				EX_MEM_jal_out :in std_logic_vector(2*regsize-1 downto 0);
				
			 
				MEM_WB_ALU_OUT : in std_logic_vector(regsize - 1 downto 0);
				MEM_WB_LMD : in std_logic_vector(regsize-1 downto 0);
				MEM_WB_jal_out : in std_logic_vector(2*regsize-1 downto 0);

				mem_mux : in std_logic_vector(1 downto 0);

				
				IN_m_wb_IR : out std_logic_vector( regsize - 1 downto 0);
			     IN_m_wb_jal : out std_logic_vector( 2*regsize - 1 downto 0);
				 in_m_wb_aluout : out std_logic_vector( 2*regsize - 1 downto 0);
				data_in_mem : out std_logic_vector(2*regsize - 1 downto 0);
				add_in_mem : out std_logic_vector( 2*regsize - 1 downto 0));
end memory_stage;

architecture arch of memory_stage is

	component mux41 IS
		generic (N : INTEGER := 64 );
		port (SEL : IN std_logic_vector(1 DOWNTO 0);    
					IN1 : IN STD_LOGIC_VECTOR(N-1 downto 0);
					IN2 : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
					IN3 : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
					IN4 : IN STD_LOGIC_VECTOR(N-1 DOWNTO 0);
					OUTPUT : OUT STD_LOGIC_VECTOR(N-1 DOWNTO 0));
	end component;
		
	signal out_mux : std_logic_vector(2*regsize - 1 downto 0);
	signal in_mem_wb_alu: std_logic_vector(2*regsize - 1 downto 0);

begin

	add_in_mem <= out_ALUOUT;
	data_in_mem <= out_mux;

	mux_in : mux41
		generic map (2*regsize)
		port map (MEM_WB_jal_out, out_e_m_B, MEM_WB_LMD, MEM_WB_ALU_OUT,mem_mux, out_mux);
							
	
	IN_m_wb_IR <= OUT_e_m_IR;
	IN_m_wb_jal <= EX_MEM_jal_out;
	in_m_wb_aluout <= in_mem_wb_alu;
	in_mem_wb_alu <= out_ALUOUT;
							
end arch;