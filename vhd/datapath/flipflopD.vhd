library ieee;
use ieee.std_logic_1164.all; 

entity flipflopD is   
	port (D :	in std_logic;
				clk :	in std_logic;
				rst :	in std_logic; -- reset active low
				en : in std_logic;
				Q :	out	std_logic);
end flipflopD;

architecture behav of flipflopD is
begin	
	process(clk,rst)
	begin
		if clk'event and clk = '1' then
			if rst = '0' then            -- synchronous reset
				Q <= '0';
			elsif en = '1' then
				Q <= D; 
			end if;
		end if;
	end process;
end behav;