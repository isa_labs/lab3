library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use WORK.myTypes.all;

entity decode_stage is
		generic(regsize : integer := 32;
						regdepth : integer := 32);
		port (clk : in std_logic;

					OUT_f_d_PC : in std_logic_vector(2*regsize - 1 downto 0);
					OUT_f_d_PC_plus_four : in std_logic_vector(regsize - 1 downto 0);
					OUT_f_d_IR : in std_logic_vector(regsize -1 downto 0);

					zero_mux_1 : in std_logic_vector(2 downto 0);  --for forwarding
					zero_mux_2 : in std_logic_vector(2 downto 0);  --for forwarding
					EX_MEM_ALU_OUT : in std_logic_vector(2*regsize-1 downto 0);
					MEM_WB_ALU_OUT : in std_logic_vector(2*regsize-1 downto 0);
					MEM_WB_LMD : in std_logic_vector(2*regsize-1 downto 0);
					ID_EX_jal_out : in std_logic_vector(2*regsize-1 downto 0);
                    EX_MEM_jal_out :in std_logic_vector(2*regsize-1 downto 0);
					MEM_WB_jal_out : in std_logic_vector(2*regsize-1 downto 0);
					
					SEL : in std_logic_vector(2 downto 0);    --for sign extension
					
					RF_rd1 : in std_logic;
					RF_rd2 : in std_logic;
					RF_wr : in std_logic;  --register file control signals
					RF_en : in std_logic;
					RF_rst : in std_logic;
					IN_data_RF : in std_logic_vector(2*regsize - 1 downto 0);
					IN_add_wr_RF : in std_logic_vector(log2(regsize) - 1 downto 0);

					zero : out std_logic;
					data_in_A : out std_logic_vector(2*regsize - 1 downto 0);
					data_in_B : out std_logic_vector(2*regsize - 1 downto 0);
					data_in_Imm : out std_logic_vector(2*regsize - 1 downto 0);
					IN_d_e_IR : out std_logic_vector(regsize - 1 downto 0);
					in_ID_EX_jal : out std_logic_vector(2*regsize - 1 downto 0);
					in_d_e_PC : out std_logic_vector(2*regsize - 1 downto 0);
					ADD_CALCULATED_PC: out std_logic_vector(regsize - 1 downto 0));
					
 end decode_stage;

architecture arch of decode_stage is

   component adder is
	generic(NBIT : integer := 32);
	port (A   :	In	std_logic_vector(NBIT-1 downto 0);
				B   :	In	std_logic_vector(NBIT-1 downto 0);
				Cin :	In	std_logic;
				S   : Out std_logic_vector(NBIT-1 downto 0);
				Co  : Out std_logic);
end component; 

	component SIGN_EXTENDED IS
	GENERIC(N : INTEGER := 32);
	PORT (IR_F_D : IN STD_LOGIC_VECTOR(N-1 downto 0);  
				SEL : IN STD_LOGIC_VECTOR(2 downto 0);  
				OUT_SIGN_EXT : OUT STD_LOGIC_VECTOR(2*N-1 DOWNTO 0));
	END component;

	component zero_comp is
		generic (N : integer := 64 );
		port (RF_OUT_A : in	std_logic_vector(N-1 downto 0);
		         RF_OUT_B : in	std_logic_vector(N-1 downto 0);
					ZERO_OUT : out std_logic); --1 if result is zero ,0 if result different from 0
	end component;

	component register_file is
		generic(dataBit : integer := 64; -- number of bits in each register
						addrBit : integer := 5);
		port( CLK 		: in 	std_logic;
					RESET 	:	in 	std_logic;
					ENABLE	: in 	std_logic;
					RD1			:	in 	std_logic;
					RD2			: in 	std_logic;
					WR			: in 	std_logic;
					ADD_WR	: in 	std_logic_vector(addrBit-1 downto 0);
					ADD_RD1	: in 	std_logic_vector(addrBit-1 downto 0);
					ADD_RD2	: in 	std_logic_vector(addrBit-1 downto 0);
					DATAIN	: in 	std_logic_vector(dataBit-1 downto 0);
					OUT1		: out std_logic_vector(dataBit-1 downto 0);
					OUT2		: out std_logic_vector(dataBit-1 downto 0));
	end component;

	component MUX71 is
		generic (n : integer := 64);
		port (IN1 :	In	std_logic_vector(n-1 downto 0);
					IN2 :	In	std_logic_vector(n-1 downto 0);
					IN3 :	In	std_logic_vector(n-1 downto 0);
					IN4 :	In	std_logic_vector(n-1 downto 0);
					IN5 :	In	std_logic_vector(n-1 downto 0);
					IN6 :	In	std_logic_vector(n-1 downto 0);
					IN7 :	In	std_logic_vector(n-1 downto 0);
					S :	In	std_logic_vector(2 downto 0);
					Y :	Out	std_logic_vector(n-1 downto 0));
	end component;

	signal RF_out_A : std_logic_vector(2*regsize - 1 downto 0);
	signal RF_out_B : std_logic_vector(2*regsize - 1 downto 0);
	signal IR_19_15 : std_logic_vector(log2(regsize) - 1 downto 0 );
	signal IR_24_20 : std_logic_vector(log2(regsize) - 1 downto 0 );
	signal OUT_SIGN_EXT : std_logic_vector(2*regsize - 1 downto 0);
	signal OUT_SIGN_EXT_shifted_left : std_logic_vector(regsize - 1 downto 0);
	signal mux_out_d_1 : std_logic_vector(2*regsize - 1 downto 0);
	signal mux_out_d_2 : std_logic_vector(2*regsize - 1 downto 0);
	signal PC: std_logic_vector(2*regsize - 1 downto 0);
	signal PC_plus_four: std_logic_vector(regsize - 1 downto 0);
	signal co: std_logic;

begin

	IR_19_15 <= OUT_f_d_IR (19 downto 15);  --address read1
	IR_24_20 <= OUT_f_d_IR (24 downto 20);  --address read2

	regFile : register_file
		generic map (regsize, log2(regdepth))
		port map (clk, RF_rst, RF_en, RF_rd1, RF_rd2, RF_wr, IN_add_wr_RF, IR_19_15, IR_24_20, IN_data_RF, RF_out_A, data_in_B);


	zero_unit : zero_comp
		generic map (regsize)
		port map (mux_out_d_1,mux_out_d_2, zero);

	sign_unit : SIGN_EXTENDED
		GENERIC MAP (regsize)
		PORT MAP (OUT_f_d_IR, SEL, OUT_SIGN_EXT);

	mux1_before_zero : MUX71
		generic map (regsize)
		port map (EX_MEM_ALU_OUT, MEM_WB_LMD, MEM_WB_ALU_OUT,ID_EX_jal_out,EX_MEM_jal_out,MEM_WB_jal_out, RF_out_A, zero_mux_1, mux_out_d_1);
	
	mux2_before_zero : MUX71
		generic map (regsize)
		port map (EX_MEM_ALU_OUT, MEM_WB_LMD, MEM_WB_ALU_OUT, ID_EX_jal_out,EX_MEM_jal_out,MEM_WB_jal_out,RF_out_B, zero_mux_2, mux_out_d_2);

    adder_sub: adder
	              generic map(regsize)
				   port map(PC_plus_four,OUT_SIGN_EXT_shifted_left,'0',ADD_CALCULATED_PC,co);
   

	data_in_A <= RF_out_A;
	data_in_B <= RF_out_B;
	PC <= out_f_d_PC;
	in_d_e_PC <= PC;
	PC_plus_four <= OUT_f_d_PC_plus_four;
	in_ID_EX_jal<= x"00000000" & PC_plus_four;
	 OUT_SIGN_EXT_shifted_left <=  OUT_SIGN_EXT(regsize-1 downto 1) & '0'; --shift left by 1 the immediate
	data_in_Imm <= OUT_SIGN_EXT;
	IN_d_e_IR <= OUT_f_d_IR;
	

end arch;