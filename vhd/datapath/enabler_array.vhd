library IEEE;
use IEEE.std_logic_1164.all;

entity enabler_array is
	generic (N : integer :=32);
	port (D   :	In	std_logic_vector(N-1 downto 0);
				rst : in  std_logic; -- active low
				en  :	In	std_logic;
				Q   :	Out	std_logic_vector(N-1 downto 0));
end enabler_array;

architecture struct of enabler_array is

	component enabler is
		port (D   :	In	std_logic;
					rst : in  std_logic; -- active low
					en  :	In	std_logic;
					Q   :	Out	std_logic);
	end component;

	begin

		latch_gen: for i in 0 to N-1 generate
			latch_row: enabler port map (D(i), rst, en, Q(i));
		end generate;

end struct;
