library IEEE;
use IEEE.std_logic_1164.all;

entity MUX71 is
	generic(n : integer := 64);
	port (IN1 :	in	std_logic_vector(n-1 downto 0);
				IN2 :	in	std_logic_vector(n-1 downto 0);
				IN3 :	in	std_logic_vector(n-1 downto 0);
				IN4 :	in	std_logic_vector(n-1 downto 0);
				IN5 : in	std_logic_vector(n-1 downto 0);
				IN6 : in	std_logic_vector(n-1 downto 0);
				IN7 : in	std_logic_vector(n-1 downto 0);
				S :	in	std_logic_vector(2 downto 0);
				Y :	out	std_logic_vector(n-1 downto 0));
end MUX71;

architecture BEHAV of MUX71 is
	BEGIN
		WITH S SELECT
			Y <= 	IN1 WHEN "111",
					IN2 WHEN "110",
					IN3 WHEN "101",
					IN4 WHEN "100",
					IN5 WHEN "011",
					IN6 WHEN "010",
					IN7 WHEN "001",
						(OTHERS => '0') WHEN OTHERS;
		
end BEHAV;
