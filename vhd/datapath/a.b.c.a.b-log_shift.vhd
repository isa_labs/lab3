library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity log_shifter is
	generic(N : integer := 32);
	port (A     : in  std_logic_vector(N-1 downto 0);
				shift : in  std_logic_vector(log2(N)-1 downto 0);
				LR    : in  std_logic; -- 1 for left shifts; 0 for right shifts
				AL    : in  std_logic; -- 1 for arithmetic shifts; 0 for logic shifts
				res   : out std_logic_vector(N-1 downto 0));
end log_shifter;

architecture struct of log_shifter is

	component MUX21_GENERIC is
		generic(NBIT : integer := 32);
		port (A   : In  std_logic_vector(NBIT-1 downto 0);
					B   : In  std_logic_vector(NBIT-1 downto 0);
					SEL : In  std_logic;
					Y   : Out std_logic_vector(NBIT-1 downto 0));
	end component;
	
	type shift_arr is array (0 to log2(N)) of std_logic_vector(N-1 downto 0);
	
	signal l_stage, r_stage : shift_arr;
	signal l_shift, r_shift : shift_arr;

	begin

		l_stage(0) <= A;
		r_stage(0) <= A;

		shift_gen: for stage in 1 to log2(N) generate
			-- the muxes are used to select between the shifted and unshifted input
			MUX_LEFT:		MUX21_GENERIC generic map(N)
																port map(l_shift(stage), l_stage(stage-1), shift(log2(N)-stage), l_stage(stage));
			MUX_RIGHT:	MUX21_GENERIC generic map(N)
																port map(r_shift(stage), r_stage(stage-1), shift(log2(N)-stage), r_stage(stage));

			l_shift(stage)(N-1 downto 2**(log2(N)-stage)) <= l_stage(stage-1)(N-1-2**(log2(N)-stage) downto 0); -- the input is shifted of 2**(log2(N)-stage) spots.
			l_shift(stage)(2**(log2(N)-stage)-1 downto 0) <= (others => '0'); -- there is no difference in the added LSBs between logical and arithmetic shifts
			r_shift(stage)(N-1-2**(log2(N)-stage) downto 0) <= r_stage(stage-1)(N-1 downto 2**(log2(N)-stage));
			r_shift(stage)(N-1 downto N-2**(log2(N)-stage)) <= (others => r_stage(stage-1)(N-1) and AL); -- the sign is extended depending on the AL signal

		end generate;

		MAIN_MUX: MUX21_GENERIC generic map(N)
														port map(l_stage(log2(N)), r_stage(log2(N)), LR, res);

end struct;