library IEEE;
use IEEE.std_logic_1164.all;

entity if_id_pipe is
	generic(regsize : integer := 32;
				 	opcode_size : integer := 7);
	port (in_f_d_PC : in std_logic_vector(2*regsize-1 downto 0);
		 in_f_d_PC_plus_four : in std_logic_vector(regsize-1 downto 0); -- direct output from the fetch adder
				in_f_d_IR : in std_logic_vector(regsize - 1 downto 0);
				clk : in std_logic;
				f_d_rst : in std_logic;
				f_d_en : in std_logic;

				out_f_d_PC : out std_logic_vector(2*regsize-1 downto 0);
		        out_f_d_PC_plus_four : out std_logic_vector(regsize-1 downto 0); -- direct output from the fetch adder
				out_f_d_IR : out std_logic_vector(regsize - 1 downto 0);
				out_f_d_opcode : out std_logic_vector(opcode_size - 1 downto 0));
end if_id_pipe;

architecture arch of if_id_pipe is
				
	component general_register is   -- synchronous N bit register
		generic(N : integer := 32);
		Port (D : in std_logic_vector(N-1 downto 0);
					clk : in std_logic;
					rst : in std_logic;
					en : in std_logic;
					Q : out	std_logic_vector(N-1 downto 0));
	end component;

 	signal out_if_id_IR : std_logic_vector(regsize - 1 downto 0);
begin 
		
	f_d_PC: general_register  
		generic map(2*regsize)
		Port map (in_f_d_PC,clk, f_d_rst, f_d_en, out_f_d_PC);

	f_d_PC_plus_four: general_register  
		generic map(regsize)
		Port map (in_f_d_PC_plus_four,	clk, f_d_rst, f_d_en, out_f_d_PC_plus_four);
	 
	f_d_IR: general_register  
		generic map(regsize)
		Port map (in_f_d_IR, clk, f_d_rst, f_d_en, out_if_id_IR);

	out_f_d_opcode <=  out_if_id_IR(opcode_size - 1 downto 0);
	out_f_d_IR <=  out_if_id_IR;

end arch;