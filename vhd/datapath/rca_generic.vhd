library ieee; 
use ieee.std_logic_1164.all; 
 
entity RCA_GEN is 
	generic(NBIT : integer := 4);
	port (A  : In	 std_logic_vector(NBIT-1 downto 0);
				B  : In	 std_logic_vector(NBIT-1 downto 0);
				Ci : In	 std_logic;
				S  : Out std_logic_vector(NBIT-1 downto 0);
				Co : Out std_logic);
end RCA_GEN; 

architecture STRUCTURAL of RCA_GEN is

	signal STMP : std_logic_vector(NBIT-1 downto 0);
	signal CTMP : std_logic_vector(NBIT downto 0);

	component FA 
		port (A  : In	 std_logic;
					B  : In	 std_logic;
					Ci : In	 std_logic;
					S  : Out std_logic;
					Co : Out std_logic);
	end component; 

	begin

	CTMP(0) <= Ci;
	S <= STMP;
	Co <= CTMP(NBIT);
	
	ADDER1: for I in 0 to NBIT-1 generate
		FAI : FA port Map (A(I), B(I), CTMP(I), STMP(I), CTMP(I+1)); 
	end generate;

end STRUCTURAL;
