library IEEE;
use IEEE.std_logic_1164.all;

entity ex_mem_pipe is
	generic(regsize : integer := 32);
	port (
				in_e_m_IR : in std_logic_vector(regsize - 1 downto 0);
		 		IN_REG_ALUOUT : in std_logic_vector(2*regsize-1 downto 0);
		 		IN_REG_B_EX_MEM : in std_logic_vector(2*regsize-1 downto 0);
				in_ex_mem_jal : in std_logic_vector(2*regsize-1 downto 0);

				clk : in std_logic;
				e_m_rst : in std_logic;
				e_m_en : in std_logic;

				out_e_m_IR : out std_logic_vector( regsize - 1 downto 0);
				OUT_REG_ALUOUT : out std_logic_vector(2*regsize-1 downto 0);
				OUT_REG_B_EX_MEM : out std_logic_vector(2*regsize-1 downto 0);
				EX_MEM_jal_out : out std_logic_vector(2*regsize-1 downto 0)
				);
end ex_mem_pipe;

architecture arch of ex_mem_pipe is

component general_register  is   --asynchronous N bit register
	generic(N : integer :=32);
	port (D :	in std_logic_vector(N-1 downto 0);
				clk :	in std_logic;
				rst :	in std_logic;
				en : in	std_logic;
				Q :	out	std_logic_vector(N-1 downto 0));
end component;

begin

	e_m_jal : general_register
		generic map (regsize)
		port map (in_ex_mem_jal,clk, e_m_rst, e_m_en, EX_MEM_jal_out);

	e_m_IR : general_register
	 	generic map (regsize)
	 	port map (in_e_m_IR,	clk, e_m_rst, e_m_en, out_e_m_IR);

	REG_ALUOUT : general_register
		generic map (regsize)
		port map (IN_REG_ALUOUT, clk, e_m_rst, e_m_en, OUT_REG_ALUOUT);

	regB_IN_EX_MEM_PIPE : general_register
		generic map (regsize)
		port map (IN_REG_B_EX_MEM, clk, e_m_rst, e_m_en, OUT_REG_B_EX_MEM);

end arch;