library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;


entity register_file is
	generic(dataBit : integer := 64; -- number of bits in each register
					addrBit : integer := 5);
	port (CLK			: in 	std_logic;
				RESET 	:	in 	std_logic;
				ENABLE	: in 	std_logic;
				RD1			:	in 	std_logic;
				RD2			: in 	std_logic;
				WR			: in 	std_logic;
				ADD_WR	: in 	std_logic_vector(addrBit-1 downto 0);
				ADD_RD1	: in 	std_logic_vector(addrBit-1 downto 0);
				ADD_RD2	: in 	std_logic_vector(addrBit-1 downto 0);
				DATAIN	: in 	std_logic_vector(dataBit-1 downto 0);
				OUT1		: out std_logic_vector(dataBit-1 downto 0);
				OUT2		: out std_logic_vector(dataBit-1 downto 0));
end register_file;

architecture A of register_file is
	constant regNum : integer := (2**addrBit)-1;
	subtype REG_ADDR is natural range 0 to regNum; -- using natural type
	type REG_ARRAY is array(REG_ADDR) of std_logic_vector(dataBit-1 downto 0); 
	signal REGISTERS : REG_ARRAY;
	signal out1_sig, out2_sig : std_logic_vector(dataBit - 1 downto 0);

	begin 


		out1_sig <= REGISTERS(conv_integer(ADD_RD1));
		out2_sig <= REGISTERS(conv_integer(ADD_RD2));


		writeproc : process (RESET, CLK)
		begin
			if RESET = '0' then
				for i in regNum downto 0 loop
					REGISTERS(i) <= (others => '0');
				end loop;

			elsif falling_edge(CLK) then
				if WR = '1' then
					--WRITE COMMAND
					if ADD_WR /= "00000" then
						REGISTERS(conv_integer(ADD_WR)) <= DATAIN;
					end if;
				end if;
			end if;

		end process writeproc;

		readproc : process (RD1, RD2, out1_sig, out2_sig)
		begin
			if (RD1='1') then
				--read1 operation
				OUT1<= out1_sig;
			else
				OUT1<= (others => '-');
			end if;
			if (RD2='1') then
				--read2 operation
				OUT2<= out2_sig; 
			else
				OUT2<= (others => '-');
			end if;
		end process readproc;

end A;
