library IEEE;
use IEEE.std_logic_1164.all;

entity mem_wb_pipe is
	generic(regsize : integer := 32);
	port (in_m_wb_jal : in std_logic_vector(2*regsize-1 downto 0);
				in_m_wb_IR : in std_logic_vector(regsize - 1 downto 0);
				IN_ALUOUT_M_WB : in std_logic_vector(2*regsize-1 downto 0);
				IN_LMD : in std_logic_vector(2*regsize-1 downto 0);
				
				clk : in std_logic;
				m_WB_rst : in std_logic;
				m_wb_en : in std_logic;

				MEM_WB_jal_out : out std_logic_vector(2*regsize-1 downto 0);
				out_m_wb_IR : out std_logic_vector( regsize - 1 downto 0);
				OUT_ALUOUT_M_WB : out std_logic_vector(regsize-1 downto 0);
				OUT_LMD : out std_logic_vector(regsize-1 downto 0));
end mem_wb_pipe;

architecture arch of mem_wb_pipe is
			
	component general_register is   --asynchronous N bit register
		generic(N : integer := 32);
		port (D :	in std_logic_vector(N-1 downto 0);
					clk :	in std_logic;
					rst :	in std_logic;
					en : in std_logic;
					Q :	out	std_logic_vector(N-1 downto 0));
	end component;

begin 
	 
	m_wb_jal : general_register  
		generic map (2*regsize)
		port map (in_m_wb_jal, clk, m_wb_rst, m_wb_en, MEM_WB_jal_out);
	
	m_wb_IR : general_register  
		generic map (2*regsize)
		port map (in_m_wb_IR, clk, m_wb_rst, m_wb_en, out_m_wb_IR);

	reg_ALUOUT_M_WB : general_register  
		generic map (2*regsize)
		port map (IN_ALUOUT_M_WB, clk, m_wb_rst, m_wb_en, OUT_ALUOUT_M_WB);
	 
	LMD : general_register  
		generic map (2*regsize)
		port map (IN_LMD, clk, m_wb_rst, m_wb_en, OUT_LMD);

end arch;