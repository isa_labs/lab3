library IEEE;
use IEEE.std_logic_1164.all;
use WORK.myTypes.all;

entity writeBack_stage is
	generic(regsize : integer := 32;
					regdepth : integer := 32);
	port (MEM_WB_jal_out : in std_logic_vector(2*regsize - 1 downto 0);
				OUT_m_wb_IR : in std_logic_vector( regsize -1 downto 0);
				OUT_m_wb_ALUOUT : in std_logic_vector(2*regsize - 1 downto 0);
				OUT_m_wb_LMD : in std_logic_vector(2*regsize - 1 downto 0);

				wb_data_mux : in std_logic; --selection signals
				wb_jal_data_mux : in std_logic;
			
				out_wb_data_mux : out std_logic_vector(regsize - 1 downto 0);
				out_wb_add_mux : out std_logic_vector(log2(regdepth)-1 downto 0));
end writeBack_stage;

architecture arch of writeBack_stage is

	component MUX21_GENERIC is
	generic(NBIT : integer := 64);
	port (A   : In  std_logic_vector(NBIT-1 downto 0);
				B   : In  std_logic_vector(NBIT-1 downto 0);
				SEL : In  std_logic;
				Y   : Out std_logic_vector(NBIT-1 downto 0));
end component;

	signal IR_m_wb_11_7 : std_logic_vector(11 downto 7);
    signal out_mux1: std_logic_vector(2*regsize -1 downto 0);
begin

	IR_m_wb_11_7 <= OUT_m_wb_IR(11 downto 7);
	out_wb_add_mux <= IR_m_wb_11_7;
	
	mux_wb1 : MUX21_GENERIC 
		generic map (regsize)
		port map ( OUT_m_wb_ALUOUT, OUT_m_wb_LMD, wb_data_mux ,out_mux1);
											
	mux_wb2 : MUX21_GENERIC 
		generic map (log2(regdepth))
		port map (MEM_WB_jal_out , out_mux1, wb_jal_data_mux, out_wb_data_mux);
		
											
end arch;
