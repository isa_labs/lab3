library IEEE;
use IEEE.std_logic_1164.all;

entity MUX41 is
	generic(n : integer := 32);
	port (IN1 :	in	std_logic_vector(n-1 downto 0);
				IN2 :	in	std_logic_vector(n-1 downto 0);
				IN3 :	in	std_logic_vector(n-1 downto 0);
				IN4 :	in	std_logic_vector(n-1 downto 0);
				S :	in	std_logic_vector(1 downto 0);
				Y :	out	std_logic_vector(n-1 downto 0));
end MUX41;

architecture BEHAV of MUX41 is
	BEGIN
		WITH S SELECT
			Y <= 	IN1 WHEN "00",
						IN2 WHEN "01",
						IN3 WHEN "10",
						IN4 WHEN "11",
						(OTHERS => '0') WHEN OTHERS;

end BEHAV;
