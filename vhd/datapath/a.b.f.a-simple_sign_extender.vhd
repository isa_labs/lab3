library ieee;
use ieee.std_logic_1164.all;

entity simple_extender is
	generic (N : integer := 32);
	port (input : in std_logic_vector( N-1 downto 0);
				extended_out : out std_logic_vector(2*N-1 downto 0));
end simple_extender;

architecture struct of simple_extender is

begin

	SIGN_EXT: for i in 2*N-1 downto 32 generate
		extended_out(i) <= input(31);
	end generate SIGN_EXT;
           extended_out(31 downto 0) <= input(31 downto 0);
	

end struct;