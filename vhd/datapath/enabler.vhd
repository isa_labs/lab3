library IEEE;
use IEEE.std_logic_1164.all;

entity enabler is
	port (D   :	In	std_logic;
				rst : in  std_logic; -- active low
				en  :	In	std_logic; -- active high
				Q   :	Out	std_logic);
end enabler;

architecture behav of enabler is
	begin
		latch: process(rst, en, D)
		begin
			if rst = '0' then

				Q <= '0';

			elsif en = '1' then

				Q <= D;

			end if;
		end process latch;
end behav;
