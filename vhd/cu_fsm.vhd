library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity cu_fsm is
	generic(opcode_size : integer := 7;  -- Op Code Size
					InstrType_size : integer := 8; -- Number of bits of the instruction pipeline
					reg_size : integer := 64;
					funct3_size: integer := 3;
			        funct7_size: integer := 7;
					alu_opcode_size : integer := 4);
	port (-- INPUTS
				clk : in std_logic;
				rst : in std_logic;

				instruction_opcode : in std_logic_vector( opcode_size-1 downto 0); -- output of IRAM
				IF_ID_opcode : in std_logic_vector( opcode_size-1 downto 0); -- from the IF/ID instruction register
				ID_EX_opcode : in std_logic_vector( opcode_size-1 downto 0); -- from the ID/EX instruction register
				ID_EX_funct3  : in std_logic_vector( funct3_size -1  downto 0);
		        ID_EX_funct7  : in std_logic_vector( funct7_size - 1 downto 0);

				-- for branches
				zero : in std_logic; -- output of the  zero comparator in decode stage

				-- for forwarding
				EM_Rd3_eq_DE_Rs1	: in std_logic;
				EM_Rd3_eq_DE_Rs2	: in std_logic;
				MW_Rd3_eq_DE_Rs1	: in std_logic;
				MW_Rd3_eq_DE_Rs2	: in std_logic;
				EM_Rd2_eq_DE_Rs1	: in std_logic;
				EM_Rd2_eq_DE_Rs2	: in std_logic;
				MW_Rd2_eq_DE_Rs1	: in std_logic;
				MW_Rd2_eq_DE_Rs2	: in std_logic;
				MW_Rd3_eq_EM_Rs1	: in std_logic;
				MW_Rd2_eq_EM_Rs1  : in std_logic;
				MW_Rd1_eq_EM_Rs1	: in std_logic;
				EM_Rd3_eq_FD_Rs1	: in std_logic;
				MW_Rd1_eq_FD_Rs1	: in std_logic;
				MW_Rd3_eq_FD_Rs1	: in std_logic;
				EM_Rd2_eq_FD_Rs1 	: in std_logic;
				MW_Rd2_eq_FD_Rs1 	: in std_logic;
				MW_Rd3_eq_EM_Rs2 	: in std_logic;
				MW_Rd2_eq_EM_Rs2 	: in std_logic;
				EM_Rd3_eq_FD_Rs2 : in std_logic; 
				EM_Rd2_eq_FD_Rs2 : in std_logic;
				MW_Rd1_eq_FD_Rs2 : in std_logic; 
				MW_Rd3_eq_FD_Rs2 : in std_logic;
				MW_Rd2_eq_FD_Rs2 : in std_logic;
				DE_Rd3_eq_FD_Rs1: in std_logic;
				DE_Rd3_eq_FD_Rs2: in std_logic;

				-- for stalling
				rd2_ID_EX_eq_rs1_IF_ID : in std_logic; -- output of comparators
				rd2_ID_EX_eq_rs2_IF_ID : in std_logic;
				rd2_ID_EX_eq_rs1_IR :	in std_logic;
				rd2_IF_ID_eq_rs1_IR : in std_logic;
				rd3_IF_ID_eq_rs1_IR : in std_logic;

				-- all pipeline rst and program counter enable
				PC_rst : out std_logic;
				f_d_rst	: out std_logic;
				d_e_rst	: out std_logic;
				e_m_rst	: out std_logic;
				m_wb_rst : out std_logic;
				PC_en : out std_logic;

				-- branch outputs
			  if_mux_1 : out std_logic;

				-- fetch outputs
				
				IF_ID_en : out std_logic;

				-- decode outputs
				RF_read1 : out std_logic; 	-- 1 if read at port 1 of the register file
				RF_read2 : out std_logic; 	-- 1 if read at port 2 of the register file
				RF_en : out std_logic; 			-- reg file enable
				RF_rst : out std_logic;
				SEL : out STD_LOGIC_VECTOR(2 downto 0);   --select which operation in the sign extender
				zero_mux1 : out std_logic_vector(2 downto 0); -- for forwarding
				zero_mux2 : out std_logic_vector(2 downto 0); -- for forwarding
				ID_EX_en : out std_logic;

				-- execute outputs
				ex_mux : out std_logic;
				ALU_rst : out std_logic;
				ALU_OPCODE : out	std_logic_vector( alu_opcode_size - 1 downto 0);
				ALU_MUX_top : out std_logic_vector(2 downto 0); -- for forwarding
				ALU_MUX_bottom : out std_logic_vector(2 downto 0);
				lui_auipc_mux:out std_logic_vector(1 downto 0);
				EX_MEM_en : out std_logic;

				-- memory outputs
				mem_rst : out std_logic;
				mem_rd_en	: out std_logic; -- 1 if read data memory
				mem_wr_en : out std_logic; -- 1 if write data memory
				mem_mux : out std_logic_vector(1 downto 0);
				MEM_WB_en : out std_logic;

				-- write back outputs
				wb_data_mux : out std_logic;
				wb_jal_data_mux : out std_logic; --the second mux
				RF_write : out std_logic); -- 1 if write the register file
end cu_fsm;

architecture arch of cu_fsm is

	component InstructionDec is 
	generic(OP_CODE_SIZE : integer := 7;  -- Op Code Size
					InstrType_size : integer := 8);
	port (  instruction_opcode : in std_logic_vector( OP_CODE_SIZE-1 downto 0); --exiting from the output of the InstrMEM
				instruction_type : out std_logic_vector(InstrType_size-1 downto 0)); --type of instruction to be checked
	end component;


	component InstructionTypePipelined is
	generic (InstrType_size : integer := 8);
	port (clk : in std_logic;
				rst : in std_logic;
				if_id_rst : in std_logic; 
				instruction_type : in std_logic_vector(InstrType_size-1 downto 0);
				-- instruction_type bits:
	--7 => auipc
	--6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type    
				IF_ID_en : in std_logic;
				ID_EX_en : in std_logic;
				EX_MEM_en : in std_logic;
				MEM_WB_en : in std_logic;

				IF_ID_IN : out std_logic_vector(InstrType_size-1 downto 0); --the signal exiting from instruction decoder in the fetch stage
				ID_EX_IN : out std_logic_vector(InstrType_size-1 downto 0);-- the signal exiting from if_id pipe reg and entering in the id_ex pipe reg
				EX_MEM_IN : out std_logic_vector(InstrType_size-1 downto 0);
				MEM_WB_IN : out std_logic_vector(InstrType_size-1 downto 0);
				MEM_WB_OUT : out std_logic_vector(InstrType_size-1 downto 0));
	end component;

	component fsm IS 
	generic(OP_CODE_SIZE : integer := 7;  -- Op Code Size
					InstrType_size : integer := 7);
 	port (-- INPUTS
				CLOCK : in std_logic;
				RST : in std_logic; -- Active Low

				IF_ID_IN : in std_logic_vector(InstrType_size - 1 downto 0); 
				ID_EX_IN : in std_logic_vector(InstrType_size - 1 downto 0);
				EX_MEM_IN : in std_logic_vector(InstrType_size - 1 downto 0);          
				MEM_WB_IN : in std_logic_vector(InstrType_size - 1 downto 0); 

				-- bits showing the type of instruction to be propagated in the pipeline:
	 --7 => auipc
	 --6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type
																					
				IF_ID_en : in std_logic;
				ID_EX_en : in std_logic;  
				EX_MEM_en : in std_logic;
				MEM_WB_en : in std_logic;

				PC_rst : out std_logic; 	-- reset input register in fetch stage (rst of the program counter, active low)
				f_d_rst	: out std_logic; 	-- reset  fetch_decode register    -- active low
				d_e_rst	: out std_logic; 	-- reset decode_execute register  
				e_m_rst	: out std_logic; 	-- reset  execute_memory register 		
				m_wb_rst : out std_logic;	-- reset  memory_writeback register 

				-- fetch outputs
				
				-- decode outputs
				RF_en : out std_logic; -- reg file enable
				RF_rst : out std_logic;  
				RF_read1 : out std_logic; -- 1 if read at port 1 of the register file
				RF_read2 : out std_logic; -- 1 if read at port 2 of the register file
				SEL : out std_logic_vector(2 downto 0); --selection for sign extender block
				
				-- execute outputs
				ex_mux : out std_logic;
				lui_auipc_mux:out std_logic_vector(1 downto 0);
				ALU_rst : out std_logic;
				
							
				-- memory outputs
				mem_rd_en	: out std_logic; -- 1 if read data memory
				mem_wr_en : out std_logic; -- 1 if write data memory
				mem_rst	: out std_logic;

				-- write back outputs
				wb_data_mux : out std_logic; --the first mux
				wb_jal_data_mux : out std_logic; --the second mux
				RF_write : out std_logic); -- 1 if write the register file
	end component;

	component branch_unit is
	generic(OP_CODE_SIZE : integer := 6;  -- Op Code Size
					REG_SIZE : integer := 32); --registers size
	port (      clk : in std_logic;
				IF_ID_opcode : in std_logic_vector(OP_CODE_SIZE-1 downto 0);
				ZERO_COMP	: in std_logic;
				f_d_en : in std_logic;
				f_d_reset : out std_logic;
				if_mux_1 : out std_logic);
	end component;

	component alu_encoder is
    generic(opcode_size : integer := 7;
	         funct3_size: integer := 3;
			 funct7_size: integer := 7;
			 alu_control_size : integer := 4); 
	port(
		opcode  : in std_logic_vector( opcode_size-1 downto 0);
		funct3  : in std_logic_vector( funct3_size -1  downto 0);
		funct7  : in std_logic_vector( funct7_size - 1 downto 0);

		-- ALU operation:
		alu_op : out std_logic_vector(alu_control_size - 1 downto 0)
	);
	end component;

	component forwarding_unit is
	
	-- instruction_type bits:
	--7 => auipc
	--6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type                                                                 		
																				
	port (      branch_if_id : in std_logic;  --ID_EX_IN(4)
		        jump_link_if_id : in std_logic;   --ID_EX_IN(5)
				rtype_id_ex : in std_logic;      --EX_MEM_IN(2)
				alu_imm_ex_mem : in std_logic;  --MEM_WB_IN(0)
				rtype_ex_mem : in std_logic;        --MEM_WB_IN(1)
				store_ex_mem : in std_logic;        --MEM_WB_IN(3)
				alu_imm_mem_wb : in std_logic;       --MEM_WB_OUT(0)
				rtype_mem_wb : in std_logic;        --MEM_WB_OUT(1)
				load_mem_wb : in std_logic;		     --MEM_WB_OUT(2)
				lui_ex_mem : in std_logic;    --MEM_WB_IN(6)
				auipc_ex_mem : in std_logic; --MEM_WB_IN(7)
				lui_mem_wb : in std_logic; --MEM_WB_OUT(6)
				auipc_mem_wb : in std_logic; --MEM_WB_OUT(7)
				jal_id_ex: in std_logic;-- EX_MEM_IN(5)
				jal_ex_mem: in std_logic;-- MEM_WB_IN(5)
				jal_mem_wb: in std_logic;-- MEM__WB_OUT(5)
				
				EM_Rd3_eq_FD_Rs1 : in std_logic;
				MW_Rd1_eq_FD_Rs1 : in std_logic;
				MW_Rd3_eq_FD_Rs1 : in std_logic;
				EM_Rd2_eq_FD_Rs1 : in std_logic;
				MW_Rd2_eq_FD_Rs1 : in std_logic;
				MW_Rd3_eq_EM_Rs2 : in std_logic;
				MW_Rd2_eq_EM_Rs2 : in std_logic;
				EM_Rd3_eq_DE_Rs1 : in std_logic; 
				EM_Rd3_eq_DE_Rs2 : in std_logic; 
				MW_Rd3_eq_DE_Rs1 : in std_logic;
				MW_Rd3_eq_DE_Rs2 : in std_logic;
				EM_Rd2_eq_DE_Rs1 : in std_logic;
				EM_Rd2_eq_DE_Rs2 : in std_logic;
				MW_Rd2_eq_DE_Rs1 : in std_logic;
				MW_Rd2_eq_DE_Rs2 : in std_logic;
				MW_Rd3_eq_EM_Rs1 : in std_logic;
				MW_Rd2_eq_EM_Rs1 : in std_logic;
				MW_Rd1_eq_EM_Rs1 : in std_logic; 
				
				
				EM_Rd3_eq_FD_Rs2 : in std_logic; 
				EM_Rd2_eq_FD_Rs2 : in std_logic;
				MW_Rd1_eq_FD_Rs2 : in std_logic; 
				MW_Rd3_eq_FD_Rs2 : in std_logic;
				MW_Rd2_eq_FD_Rs2 : in std_logic;
				DE_Rd3_eq_FD_Rs1: in std_logic;
				DE_Rd3_eq_FD_Rs2: in std_logic;
				
				
				zero_mux1 : out std_logic_vector(2 downto 0);
				zero_mux2 : out std_logic_vector(2 downto 0);
				ALU_MUX_top : out std_logic_vector(2 downto 0);
				ALU_MUX_bottom : out std_logic_vector(2 downto 0);
				mem_mux : out std_logic_vector(1 downto 0));
	end component;

	component stall_unit is
		port (clk : 					in std_logic;
					rst : 					in std_logic;
					load_ID_EX :   	in std_logic; -- instruction signals from the pipeline unit
					r_ALU_IF_ID :  	in std_logic;
					load_IF_ID :   	in std_logic;
					store_IF_ID :  	in std_logic;
					i_ALU_IF_ID :  	in std_logic;
					branch_IR : 		in std_logic; -- IR marks the next instruction register line, not yet pipelined
					jump_IR :   		in std_logic;

					rd2_ID_EX_eq_rs1_IF_ID :	in std_logic; -- output of comparators
					rd2_ID_EX_eq_rs2_IF_ID :	in std_logic;
					rd2_ID_EX_eq_rs1_IR :	in std_logic;
					rd2_IF_ID_eq_rs1_IR : in std_logic;
					rd3_IF_ID_eq_rs1_IR :	in std_logic;

					PC_enable	:    		out std_logic;
					IF_ID_enable : 		out std_logic; -- stalls are implemented forcing some pipeline registers to keep the same value disabling them alltoghether
					ID_EX_enable : 		out std_logic;
					EX_MEM_enable : 	out std_logic;
					MEM_WB_enable : 	out std_logic);
	end component;

	signal IF_ID_en_sig : std_logic;
	signal ID_EX_en_sig : std_logic;
	signal EX_MEM_en_sig : std_logic;
	signal MEM_WB_en_sig : std_logic;

	signal instruction_type	: std_logic_vector(InstrType_size-1 downto 0); --type of instruction,output of instruction decoder in fetch stage

	signal IF_ID_IN : std_logic_vector(InstrType_size-1 downto 0); --the signal exiting from instruction decoder in the fetch stage
	signal ID_EX_IN : std_logic_vector(InstrType_size-1 downto 0);-- the signal exiting from if_id pipe reg and entering in the id_ex pipe reg
	signal EX_MEM_IN : std_logic_vector(InstrType_size-1 downto 0);
	signal MEM_WB_IN : std_logic_vector(InstrType_size-1 downto 0);
	signal MEM_WB_OUT : std_logic_vector(InstrType_size-1 downto 0);

	signal f_d_rst_fsm, f_d_rst_bu : std_logic;

begin

	f_d_rst <= f_d_rst_bu and f_d_rst_fsm;
	IF_ID_en <= IF_ID_en_sig;
	ID_EX_en <= ID_EX_en_sig;
	EX_MEM_en <= EX_MEM_en_sig;
	MEM_WB_en <= MEM_WB_en_sig;

	

	-- Instruction decoder
	decoder : InstructionDec
		generic map (opcode_size, InstrType_size)
		port map (instruction_opcode, instruction_type);

	-- Instruction type pipelined
	instruction_pipelined : InstructionTypePipelined
		generic map (InstrType_size)
		port map (clk, rst, f_d_rst_bu, instruction_type, IF_ID_en_sig, ID_EX_en_sig, EX_MEM_en_sig, MEM_WB_en_sig, IF_ID_IN, ID_EX_IN, EX_MEM_IN, MEM_WB_IN, MEM_WB_OUT);

	-- FSM
	fsm1 : fsm
		generic map (opcode_size, InstrType_size )
 		port map (clk, rst, IF_ID_IN, ID_EX_IN, EX_MEM_IN, MEM_WB_IN, IF_ID_en_sig, ID_EX_en_sig, EX_MEM_en_sig, MEM_WB_en_sig, PC_rst, f_d_rst_fsm, d_e_rst, e_m_rst, m_wb_rst, RF_en, RF_rst, RF_read1, RF_read2,SEL, ex_mux, lui_auipc_mux ,ALU_rst, mem_rd_en, mem_wr_en, mem_rst, wb_data_mux, wb_jal_data_mux, RF_write);

	-- Branch Unit
	branch_jump_unit : branch_unit
		generic map (opcode_size, reg_size)
		port map (clk, IF_ID_opcode, zero, IF_ID_en_sig, f_d_rst_bu, if_mux_1);


	-- ALU opcode generator
	ALU_opcd : alu_encoder
		generic map (opcode_size, funct3_size, funct7_size, alu_opcode_size)
		port map (ID_EX_opcode, ID_EX_funct3,ID_EX_funct7, ALU_OPCODE);

	-- Forwarding Unit
	Forw_unit : forwarding_unit
		port map (ID_EX_IN(4), ID_EX_IN(5), EX_MEM_IN(2), MEM_WB_IN(0), MEM_WB_IN(1), MEM_WB_IN(3), MEM_WB_OUT(0),	MEM_WB_OUT(1), MEM_WB_OUT(2),MEM_WB_IN(6),MEM_WB_IN(7),MEM_WB_OUT(6),MEM_WB_OUT(7),EX_MEM_IN(5),MEM_WB_IN(5),MEM_WB_OUT(5),
		          EM_Rd3_eq_FD_Rs1, MW_Rd1_eq_FD_Rs1, MW_Rd3_eq_FD_Rs1, EM_Rd2_eq_FD_Rs1, MW_Rd2_eq_FD_Rs1, MW_Rd3_eq_EM_Rs2, MW_Rd2_eq_EM_Rs2, EM_Rd3_eq_DE_Rs1, EM_Rd3_eq_DE_Rs2, MW_Rd3_eq_DE_Rs1, MW_Rd3_eq_DE_Rs2, EM_Rd2_eq_DE_Rs1, EM_Rd2_eq_DE_Rs2, MW_Rd2_eq_DE_Rs1, MW_Rd2_eq_DE_Rs2, MW_Rd3_eq_EM_Rs1, MW_Rd2_eq_EM_Rs1, MW_Rd1_eq_EM_Rs1,EM_Rd3_eq_FD_Rs2,EM_Rd2_eq_FD_Rs2,MW_Rd1_eq_FD_Rs2,MW_Rd3_eq_FD_Rs2,MW_Rd2_eq_FD_Rs2,DE_Rd3_eq_FD_Rs1,DE_Rd3_eq_FD_Rs2, 
				   zero_mux1,zero_mux2, ALU_MUX_top, ALU_MUX_bottom, mem_mux);

	-- Stall unit
	Stalling_unit : stall_unit
		port map (clk, rst, EX_MEM_IN(2), ID_EX_IN(1), ID_EX_IN(2), ID_EX_IN(3), ID_EX_IN(0), IF_ID_IN(4), IF_ID_IN(5), rd2_ID_EX_eq_rs1_IF_ID, rd2_ID_EX_eq_rs2_IF_ID, rd2_ID_EX_eq_rs1_IR, rd2_IF_ID_eq_rs1_IR, rd3_IF_ID_eq_rs1_IR, PC_en, IF_ID_en_sig, ID_EX_en_sig, EX_MEM_en_sig, MEM_WB_en_sig);

end arch;