LIBRARY IEEE;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.myTypes.ALL;

entity InstructionTypePipelined is
	generic (InstrType_size : integer := 8);
	port (clk : in std_logic;
				rst : in std_logic;
				if_id_rst : in std_logic; 
				instruction_type : in std_logic_vector(InstrType_size-1 downto 0);
				-- instruction_type bits:
	--7 => auipc
	--6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type    
				IF_ID_en : in std_logic;
				ID_EX_en : in std_logic;
				EX_MEM_en : in std_logic;
				MEM_WB_en : in std_logic;

				IF_ID_IN : out std_logic_vector(InstrType_size-1 downto 0); --the signal exiting from instruction decoder in the fetch stage
				ID_EX_IN : out std_logic_vector(InstrType_size-1 downto 0);-- the signal exiting from if_id pipe reg and entering in the id_ex pipe reg
				EX_MEM_IN : out std_logic_vector(InstrType_size-1 downto 0);
				MEM_WB_IN : out std_logic_vector(InstrType_size-1 downto 0);
				MEM_WB_OUT : out std_logic_vector(InstrType_size-1 downto 0));
end InstructionTypePipelined;

architecture arch of InstructionTypePipelined is

	component general_register is
		generic(N:positive:=32);
		Port (D :	in std_logic_vector(N-1 downto 0);
					clk :	in std_logic;
					rst :	in std_logic;
					en : in	std_logic;
					Q :	out	std_logic_vector(N-1 downto 0));
	end component;

	type vector_instr_type is array (0 to 3) of std_logic_vector(InstrType_size-1 downto 0);
	signal instr_type : vector_instr_type;
	signal f_d_rst : std_logic;

begin

	f_d_rst <= rst and if_id_rst;

	if_id_pipe_reg : general_register
		generic map (InstrType_size)
		port map (instruction_type, clk, f_d_rst, IF_ID_en, instr_type(0));
	id_ex_pipe_reg : general_register
		generic map (InstrType_size)
		port map (instr_type(0), clk, rst, ID_EX_en, instr_type(1));
	ex_mem_pipe_reg : general_register
		generic map (InstrType_size)
		port map (instr_type(1), clk, rst, EX_MEM_en, instr_type(2));
	mem_wb_pipe_reg : general_register
		generic map (InstrType_size)
		port map (instr_type(2), clk, rst, MEM_WB_en, instr_type(3));

		--outputs
		IF_ID_IN <= instruction_type;
		ID_EX_IN <= instr_type(0);
		EX_MEM_IN <= instr_type(1);
		MEM_WB_IN <= instr_type(2);  --the signal containing the type of instruction in the memory stage
		MEM_WB_OUT <=instr_type(3); --the signal containing the type of instruction in the writeback stage

end arch;