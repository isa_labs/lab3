library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity forwarding_unit is
	
	-- instruction_type bits:
	--7 => auipc
	--6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type                                                                 		
																				
	port (      branch_if_id : in std_logic;  --ID_EX_IN(4)
		        jump_link_if_id : in std_logic;   --ID_EX_IN(5)
				rtype_id_ex : in std_logic;      --EX_MEM_IN(2)
				alu_imm_ex_mem : in std_logic;  --MEM_WB_IN(0)
				rtype_ex_mem : in std_logic;        --MEM_WB_IN(1)
				store_ex_mem : in std_logic;        --MEM_WB_IN(3)
				alu_imm_mem_wb : in std_logic;       --MEM_WB_OUT(0)
				rtype_mem_wb : in std_logic;        --MEM_WB_OUT(1)
				load_mem_wb : in std_logic;		     --MEM_WB_OUT(2)
				lui_ex_mem : in std_logic;    --MEM_WB_IN(6)
				auipc_ex_mem : in std_logic; --MEM_WB_IN(7)
				lui_mem_wb : in std_logic; --MEM_WB_OUT(6)
				auipc_mem_wb : in std_logic; --MEM_WB_OUT(7)
				jal_id_ex: in std_logic;-- EX_MEM_IN(5)
				jal_ex_mem: in std_logic;-- MEM_WB_IN(5)
				jal_mem_wb: in std_logic;-- MEM__WB_OUT(5)
				
				EM_Rd3_eq_FD_Rs1 : in std_logic;
				MW_Rd1_eq_FD_Rs1 : in std_logic;
				MW_Rd3_eq_FD_Rs1 : in std_logic;
				EM_Rd2_eq_FD_Rs1 : in std_logic;
				MW_Rd2_eq_FD_Rs1 : in std_logic;
				MW_Rd3_eq_EM_Rs2 : in std_logic;
				MW_Rd2_eq_EM_Rs2 : in std_logic;
				EM_Rd3_eq_DE_Rs1 : in std_logic; 
				EM_Rd3_eq_DE_Rs2 : in std_logic; 
				MW_Rd3_eq_DE_Rs1 : in std_logic;
				MW_Rd3_eq_DE_Rs2 : in std_logic;
				EM_Rd2_eq_DE_Rs1 : in std_logic;
				EM_Rd2_eq_DE_Rs2 : in std_logic;
				MW_Rd2_eq_DE_Rs1 : in std_logic;
				MW_Rd2_eq_DE_Rs2 : in std_logic;
				MW_Rd3_eq_EM_Rs1 : in std_logic;
				MW_Rd2_eq_EM_Rs1 : in std_logic;
				MW_Rd1_eq_EM_Rs1 : in std_logic; 
				
				
				EM_Rd3_eq_FD_Rs2 : in std_logic; 
				EM_Rd2_eq_FD_Rs2 : in std_logic;
				MW_Rd1_eq_FD_Rs2 : in std_logic; 
				MW_Rd3_eq_FD_Rs2 : in std_logic;
				MW_Rd2_eq_FD_Rs2 : in std_logic;
				DE_Rd3_eq_FD_Rs1: in std_logic;
				DE_Rd3_eq_FD_Rs2: in std_logic;
				
				
				zero_mux1 : out std_logic_vector(2 downto 0);
				zero_mux2 : out std_logic_vector(2 downto 0);
				ALU_MUX_top : out std_logic_vector(2 downto 0);
				ALU_MUX_bottom : out std_logic_vector(2 downto 0);
				mem_mux : out std_logic_vector(1 downto 0));
end forwarding_unit;

architecture beh of forwarding_unit is
	
	signal forw_b1 : std_logic_vector(2 downto 0);
	signal forw_b2 : std_logic_vector(2 downto 0);

begin	
 
	--the logic to detect forwarding in case of branch or jump register and jump and link register.
	forw_b_decode1 : process (rtype_ex_mem, rtype_mem_wb, load_mem_wb, alu_imm_ex_mem, alu_imm_mem_wb,jal_id_ex,jal_ex_mem,jal_mem_wb,lui_ex_mem,lui_mem_wb,auipc_ex_mem,auipc_mem_wb, EM_Rd3_eq_FD_Rs1, MW_Rd1_eq_FD_Rs1, MW_Rd3_eq_FD_Rs1, EM_Rd2_eq_FD_Rs1, MW_Rd2_eq_FD_Rs1)
	begin 			
		if ((rtype_ex_mem = '1' AND EM_Rd3_eq_FD_Rs1 = '1') OR (( alu_imm_ex_mem = '1') AND EM_Rd2_eq_FD_Rs1 = '1') OR (( lui_ex_mem = '1') AND EM_Rd2_eq_FD_Rs1 = '1') OR (( auipc_ex_mem = '1') AND EM_Rd2_eq_FD_Rs1 = '1')) then
			forw_b1 <= "000"; --data from EX_MEM_ALU_OUT
		elsif (load_mem_wb = '1' AND MW_Rd1_eq_FD_Rs1 = '1') then
			forw_b1 <= "001"; --data from MEM_WB_LMD
		elsif ((rtype_mem_wb = '1' AND MW_Rd3_eq_FD_Rs1 = '1') OR ((alu_imm_mem_wb = '1' AND MW_Rd2_eq_FD_Rs1 = '1')) OR (lui_mem_wb = '1' AND MW_Rd3_eq_FD_Rs1 = '1') OR (auipc_mem_wb = '1' AND MW_Rd3_eq_FD_Rs1 = '1')) then
			forw_b1 <= "010"; --data from MEM_WB_ALU_OUT
		elsif(jal_id_ex='1'  AND DE_Rd3_eq_FD_Rs1='1') THEN 
		     forw_b1 <= "011"; --data from ID_EX_JAL_OUT
		elsif (jal_ex_mem='1'  AND EM_Rd3_eq_FD_Rs1='1') then  
             forw_b1 <="100";	 --data from EX_MEM_JAL_OUT
		elsif (jal_mem_wb='1'  AND MW_Rd3_eq_FD_Rs1='1') then		
		     forw_b1 <="101";      --data from MEM_WB_JAL_OUT
		else 
			forw_b1 <= "111";  --data from RF_out_A
		end if;
	end process forw_b_decode1;
	
	forw_b_decode2 : process (rtype_ex_mem, rtype_mem_wb, load_mem_wb, alu_imm_ex_mem, alu_imm_mem_wb,jal_id_ex,jal_ex_mem,jal_mem_wb,lui_ex_mem,lui_mem_wb,auipc_ex_mem,auipc_mem_wb, EM_Rd3_eq_FD_Rs2, MW_Rd1_eq_FD_Rs2, MW_Rd3_eq_FD_Rs2, EM_Rd2_eq_FD_Rs2, MW_Rd2_eq_FD_Rs2)
	begin 			
		if ((rtype_ex_mem = '1' AND EM_Rd3_eq_FD_Rs2 = '1') OR (alu_imm_ex_mem = '1'  AND EM_Rd2_eq_FD_Rs2 = '1') OR (( lui_ex_mem = '1') AND EM_Rd2_eq_FD_Rs1 = '1') OR (( auipc_ex_mem = '1') AND EM_Rd2_eq_FD_Rs1 = '1')) then
			forw_b2 <= "000"; --data from EX_MEM_ALU_OUT
		elsif (load_mem_wb = '1' AND MW_Rd1_eq_FD_Rs2 = '1') then
			forw_b2 <= "001"; --data from MEM_WB_LMD
		elsif ((rtype_mem_wb = '1' AND MW_Rd3_eq_FD_Rs2 = '1') OR ((alu_imm_mem_wb = '1'  AND MW_Rd2_eq_FD_Rs2 = '1')) OR (lui_mem_wb = '1' AND MW_Rd3_eq_FD_Rs1 = '1') OR (auipc_mem_wb = '1' AND MW_Rd3_eq_FD_Rs1 = '1')) then
			forw_b2 <= "010"; --data from MEM_WB_ALU_OUT
		elsif((jal_id_ex='1'  AND DE_Rd3_eq_FD_Rs2='1') )THEN 
		     forw_b2 <= "011"; --data from ID_EX_JAL_OUT
		elsif (jal_ex_mem='1'  AND EM_Rd3_eq_FD_Rs2='1') then  
             forw_b2 <="100";	 --data from EX_MEM_JAL_OUT
		elsif (jal_mem_wb='1'  AND MW_Rd3_eq_FD_Rs2='1') then		
		     forw_b2 <="101";      --data from MEM_WB_JAL_OUT
		else
			forw_b2 <= "111";  --data from RF_out_B
		end if;
	end process forw_b_decode2;

	forw_b_out1 : process (branch_if_id, forw_b1)
	begin 
		if (branch_if_id = '1') then
			zero_mux1 <= forw_b1;
		else
			zero_mux1 <= "111";
		end if;
	end process forw_b_out1;
	
	forw_b_out2 : process (branch_if_id, forw_b2)
	begin 
		if (branch_if_id = '1') then
			zero_mux2 <= forw_b2;
		else
			zero_mux2 <= "111";
		end if;
	end process forw_b_out2;
	
	--forwarding at the bottom input of the ALU
	forw_ALU_MUX_bottom : process(rtype_id_ex, alu_imm_ex_mem, rtype_ex_mem, alu_imm_mem_wb, rtype_mem_wb, load_mem_wb,jal_id_ex,jal_ex_mem,jal_mem_wb,lui_ex_mem,lui_mem_wb,auipc_ex_mem,auipc_mem_wb, EM_Rd2_eq_DE_Rs2, EM_Rd3_eq_DE_Rs2, MW_Rd3_eq_DE_Rs2, MW_Rd2_eq_DE_Rs2)
	begin
		if (rtype_id_ex = '1') then		--in case of immediate 	ALU_MUX_bottom has to remain "000",forwarding not possible
			if ((rtype_ex_mem = '1' AND EM_Rd3_eq_DE_Rs2 = '1') OR (alu_imm_ex_mem = '1' AND EM_Rd2_eq_DE_Rs2 = '1')) then
				ALU_MUX_bottom <= "001";
			elsif ((rtype_mem_wb = '1' AND MW_Rd3_eq_DE_Rs2 = '1') OR (alu_imm_mem_wb = '1'  AND MW_Rd2_eq_DE_Rs2 = '1')) then
				ALU_MUX_bottom <= "010";
			elsif (load_mem_wb = '1' AND MW_Rd2_eq_DE_Rs2 = '1')	then
				ALU_MUX_bottom <= "011";
			elsif (jal_ex_mem='1'  AND EM_Rd3_eq_FD_Rs2='1') then  
             ALU_MUX_bottom <="100";	 --data from EX_MEM_JAL_OUT
		elsif (jal_mem_wb='1'  AND MW_Rd3_eq_FD_Rs2='1') then		
		     ALU_MUX_bottom <="101";      --data from MEM_WB_JAL_OUT
			else 
				ALU_MUX_bottom <= "000";
			end if;
		else
			ALU_MUX_bottom <= "000"; --data from register file
		end if;
	end process forw_ALU_MUX_bottom;

		
	--forwarding at the top input of the ALU
	forw_ALU_MUX_top : process(alu_imm_ex_mem, rtype_ex_mem, alu_imm_mem_wb, rtype_mem_wb, load_mem_wb,jal_id_ex,jal_ex_mem,jal_mem_wb,lui_ex_mem,lui_mem_wb,auipc_ex_mem,auipc_mem_wb, EM_Rd3_eq_DE_Rs1, MW_Rd3_eq_DE_Rs1, EM_Rd2_eq_DE_Rs1, MW_Rd2_eq_DE_Rs1)
	begin
		if ((rtype_ex_mem = '1' AND EM_Rd3_eq_DE_Rs1 = '1') OR (alu_imm_ex_mem = '1' AND EM_Rd2_eq_DE_Rs1 = '1')) then
			ALU_MUX_top <= "001";
		elsif ((rtype_mem_wb = '1' AND MW_Rd3_eq_DE_Rs1 = '1') OR (alu_imm_mem_wb = '1' AND MW_Rd2_eq_DE_Rs1 = '1')) then
			ALU_MUX_top <= "010";
		elsif (load_mem_wb = '1' AND MW_Rd2_eq_DE_Rs1 = '1') then
			ALU_MUX_top <= "011";
		elsif (jal_ex_mem='1'  AND EM_Rd3_eq_FD_Rs1='1') then  
             ALU_MUX_bottom <="100";	 --data from EX_MEM_JAL_OUT
		elsif (jal_mem_wb='1'  AND MW_Rd3_eq_FD_Rs1='1') then		
		     ALU_MUX_bottom <="101";      --data from MEM_WB_JAL_OUT	
		else
			ALU_MUX_top <= "000";
		end if;
	end process forw_ALU_MUX_top;
		

	forw_mem_mux : process (store_ex_mem, alu_imm_mem_wb, rtype_mem_wb, load_mem_wb,jal_id_ex,jal_ex_mem,jal_mem_wb,lui_mem_wb,auipc_mem_wb, MW_Rd3_eq_EM_Rs2, MW_Rd2_eq_EM_Rs2)
	begin
		if (store_ex_mem = '1') then
			if ((rtype_mem_wb = '1' AND MW_Rd3_eq_EM_Rs2 = '1') OR (alu_imm_mem_wb = '1' AND MW_Rd2_eq_EM_Rs2 = '1') OR (lui_mem_wb = '1' AND MW_Rd2_eq_EM_Rs2 = '1') OR (auipc_mem_wb = '1' AND MW_Rd2_eq_EM_Rs2 = '1'))then
				mem_mux <= "11";
		elsif (load_mem_wb = '1' AND MW_Rd2_eq_EM_Rs2 = '1') then
				mem_mux <= "01";
		elsif (jal_mem_wb='1'  AND MW_Rd3_eq_FD_Rs2='1') then		
		     mem_mux <= "10";      --data from MEM_WB_JAL_OUT
			else
				mem_mux <= "00";
			end if;					
		else 
			mem_mux <= "00";
		end if;
	end process forw_mem_mux;		
end beh;