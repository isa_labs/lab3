library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.myTypes.all;

entity alu_encoder is
    generic(opcode_size : integer := 7;
	         funct3_size: integer := 3;
			 funct7_size: integer := 7;
			 alu_control_size : integer := 4); 
	port(
		opcode  : in std_logic_vector( opcode_size-1 downto 0);
		funct3  : in std_logic_vector( funct3_size -1  downto 0);
		funct7  : in std_logic_vector( funct7_size - 1 downto 0);

		-- ALU operation:
		alu_op : out std_logic_vector(alu_control_size - 1 downto 0)
	);
end entity alu_encoder;

architecture behaviour of alu_encoder is
begin

	encoder_alu: process(opcode, funct3, funct7)
	  begin
		case to_integer(unsigned(opcode)) is
			when to_integer(unsigned(lui_itype)) => -- Load upper immediate
				             alu_op <= ALU_ADD;
		    when to_integer(unsigned(jal_jumpandlink)) => -- Jump and link
				                alu_op <= ALU_NOP;
		    when to_integer(unsigned(beq_branch )) => -- Branch operations
				              alu_op <= ALU_NOP;
			when to_integer(unsigned(lw_load )) => -- Load instruction
				              alu_op <= ALU_ADD;
			when to_integer(unsigned(sw_store)) => -- Store instruction
				               alu_op <= ALU_ADD;
			when to_integer(unsigned(addi_itype)) => -- Register-immediate operations
                 case to_integer(unsigned(funct3)) is
					 when to_integer(unsigned(funct3_add)) =>  -- addi
						     alu_op <= ALU_ADD; 
					 when to_integer(unsigned(funct3_srai ))=> --srai
							  alu_op <= ALU_SRA;
					 when to_integer(unsigned(funct3_andi)) =>   --andi
						      alu_op <= ALU_AND;
					 when others =>
						       alu_op <= ALU_NOP;
				end case; 
			when to_integer(unsigned(add_rtype)) => -- Register-register operations
				 case to_integer(unsigned(funct3 )) is
					when to_integer(unsigned(funct3_add)) =>
					    case to_integer(unsigned( funct7)) is 
						    when to_integer(unsigned(funct7_add)) =>
						        alu_op <= ALU_ADD;
							when to_integer(unsigned(funct7_sub)) =>
						        alu_op <= ALU_SUB;
                            when others =>
                                 alu_op <= ALU_NOP;   							
								 end case;
					when to_integer(unsigned(funct3_slt)) =>
						alu_op <= ALU_SLT;
					when to_integer(unsigned(funct3_xor)) =>
						alu_op <= ALU_XOR;
					when others =>
						alu_op <= ALU_NOP;
				
			             end case;
			when others =>
						alu_op <= ALU_NOP;
		end case;
	end process encoder_alu;

end architecture behaviour;
