library ieee;
use ieee.std_logic_1164.all;
use work.myTypes.all;

entity branch_unit is
	generic(OP_CODE_SIZE : integer := 6;  -- Op Code Size
					REG_SIZE : integer := 32); --registers size
	port (      clk : in std_logic;
				IF_ID_opcode : in std_logic_vector(OP_CODE_SIZE-1 downto 0);
				ZERO	: in std_logic;
				f_d_en : in std_logic;
				f_d_reset : out std_logic;
				if_mux_1 : out std_logic);
end branch_unit;

architecture structure of branch_unit is

begin
	BU_proc : process (IF_ID_opcode, ZERO)
	begin		
		if (IF_ID_opcode = beq_branch) then
			if ZERO = '1' then
				if_mux_1 <='1';
			else
				if_mux_1 <='0';
			end if;
		elsif (IF_ID_opcode = jal_jumpandlink) then
				if_mux_1 <='1';
		   end if;
	end process BU_proc;	
end structure;	