LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE work.myTypes.ALL;

ENTITY fsm IS 
	generic(OP_CODE_SIZE : integer := 7;  -- Op Code Size
					InstrType_size : integer := 7);
 	port (-- INPUTS
				CLOCK : in std_logic;
				RST : in std_logic; -- Active Low

				IF_ID_IN : in std_logic_vector(InstrType_size - 1 downto 0); 
				ID_EX_IN : in std_logic_vector(InstrType_size - 1 downto 0);
				EX_MEM_IN : in std_logic_vector(InstrType_size - 1 downto 0);          
				MEM_WB_IN : in std_logic_vector(InstrType_size - 1 downto 0); 

				-- bits showing the type of instruction to be propagated in the pipeline:
	 --7 => auipc
	 --6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type
																					
				IF_ID_en : in std_logic;
				ID_EX_en : in std_logic;  
				EX_MEM_en : in std_logic;
				MEM_WB_en : in std_logic;

				PC_rst : out std_logic; 	-- reset input register in fetch stage (rst of the program counter, active low)
				f_d_rst	: out std_logic; 	-- reset  fetch_decode register    -- active low
				d_e_rst	: out std_logic; 	-- reset decode_execute register  
				e_m_rst	: out std_logic; 	-- reset  execute_memory register 		
				m_wb_rst : out std_logic;	-- reset  memory_writeback register 

				-- fetch outputs
				
				-- decode outputs
				RF_en : out std_logic; -- reg file enable
				RF_rst : out std_logic;  
				RF_read1 : out std_logic; -- 1 if read at port 1 of the register file
				RF_read2 : out std_logic; -- 1 if read at port 2 of the register file
				SEL : out std_logic_vector(2 downto 0); --selection for sign extender block
				
				-- execute outputs
				ex_mux : out std_logic;
				lui_auipc_mux:out std_logic_vector(1 downto 0);
				ALU_rst : out std_logic;
				
							
				-- memory outputs
				mem_rd_en	: out std_logic; -- 1 if read data memory
				mem_wr_en : out std_logic; -- 1 if write data memory
				mem_rst	: out std_logic;

				-- write back outputs
				wb_data_mux : out std_logic; --the first mux
				wb_jal_data_mux : out std_logic; --the second mux
				RF_write : out std_logic); -- 1 if write the register file
end fsm;

architecture behav of fsm is
	type STATE_FETCH is (
		reset,
		-- fetch states
		fetch
	);

	type STATE_DECODE is (
		reset,
		-- decode states
		dec_srai,	
		dec_jal,  -- immediate signed jump 
		dec_imm_lw, 		-- immediate unsigned and load word 
		dec_r,
        dec_sw,
		dec_b, --decode branch
        dec_auipc_lui		
	);

	type STATE_EXECUTE is (
		reset,
		-- execute states
		ex_i,
		ex_r,
        ex_auipc,
        ex_lui		
	);

	type STATE_MEMORY is (
		reset,
		-- memory states
		mem_l, -- load
		mem_s, -- store
		mem_i_r
		
	);

	type STATE_WRITEBACK is (
		reset,
		-- write back states
		wb_l, 	-- load
		wb_imm_r, 	-- i type operations and r type and jal
		wb_jal,
		wb_nop -- store,branch, j, 
		
	);
	
	signal F_S : STATE_FETCH; 			-- fetch state
	signal D_S : STATE_DECODE; 			-- decode state
	signal E_S : STATE_EXECUTE; 		-- execute state
	signal M_S : STATE_MEMORY; 			-- memory  state
	signal WB_S : STATE_WRITEBACK; 	-- write back state
	
	begin

		PC_rst <= RST;
		f_d_rst <= RST; 
		d_e_rst <= RST;
		e_m_rst <= RST;
		m_wb_rst <= RST;
		--rst_IRAM <= RST;
		RF_rst <= RST;
		ALU_rst <= RST;
		mem_rst <= RST;

		FETCH_STATE : process(CLOCK, RST)
		begin 
			if RST = '0' then
				F_S <= reset;
			elsif (CLOCK = '1' and CLOCK'EVENT) then 
				case WB_S is
					when reset | wb_imm_r | wb_l | wb_nop | wb_jal =>
						F_S <= fetch;		
					when others =>
						F_S <= reset;
				end case;
			end if;
		end process;

		FETCH_OUT : process(F_S)
		begin
			case F_S is	
				when reset => 
				when fetch =>
				when others => 	
				end case; 	
		end process FETCH_OUT;
		
	
		
		DECODE_STATE : process(CLOCK, RST)		
		begin
			if RST = '0' then
				D_S <= reset;
			elsif (CLOCK = '1' and CLOCK'EVENT) then
				if (IF_ID_en = '1') then
					case F_S is
						when fetch | reset =>
							if (IF_ID_IN(0) = '1') OR (IF_ID_IN(2) = '1')  then					
								D_S <= dec_imm_lw;
							
							elsif	(IF_ID_IN(1) = '1')  then
								D_S <= dec_r;
							
							elsif	(IF_ID_IN(3) = '1')  then
								D_S <= dec_sw;
								
							elsif	(IF_ID_IN(4) = '1')  then
								D_S <= dec_b;

							elsif (IF_ID_IN(5) = '1') then
								D_S <= dec_jal;

							elsif	(IF_ID_IN(6) = '1')  then
								D_S <= dec_auipc_lui;

							else
								D_S <= reset;

							end if;			
						when others =>
							D_S <= reset;
					end case;
				end if;
			end if;
		end process DECODE_STATE;
		
				
		DECODE_OUT : process(D_S)
		begin
			case D_S is
				when reset =>
					RF_en <= '1';
					RF_read1 <= '1';
					RF_read2 <= '1';
					SEL <= "000";
							
				when dec_srai => -- immediate signed no jump
					RF_en <= '1';
					RF_read1 <= '1';
					RF_read2 <= '1'; 
					SEL <= "110";
			
                when dec_r => -- immediate signed no jump
					RF_en <= '1';
					RF_read1 <= '1';
					RF_read2 <= '1'; 
					SEL <= "000";			
				
                when  dec_auipc_lui =>	
                    RF_en <= '1';
					RF_read1 <= '1';
					RF_read2 <= '1'; 
					SEL <= "100";				

					when dec_b=> -- immediate signed branch
					RF_en <= '1';
					RF_read1 <= '1';
					RF_read2 <= '1'; 
					SEL <= "011";

				when dec_jal=>   -- immediate signed jump
					RF_en <= '1';
					RF_read1 <= '1';	
					RF_read2 <= '1';	
					SEL <= "010";
							
				when dec_imm_lw =>   --immediate unsigned
					RF_en <= '1';
					RF_read1 <= '1';	
					RF_read2 <= '1';
					SEL <= "101";
						
				when dec_sw =>
					RF_en <= '1';
					RF_read1 <= '1';	
					RF_read2 <= '1';
				    SEL <= "001";
				 	
				when others => 	
					RF_en <= '0';
					RF_read1 <= '0';
					RF_read2 <= '0';
					SEL <= "000";

			end case; 	
		end process DECODE_OUT;
		
		
		
		EXECUTE_STATE : process(CLOCK, RST)		
		begin
			if RST = '0' then
				E_S <= reset;
			elsif (CLOCK = '1' and CLOCK'EVENT) then 
				if (ID_EX_en = '1') then
					case D_S is
						when dec_imm_lw | dec_srai | dec_jal | dec_sw | dec_b  =>
							E_S <= ex_i;
						when dec_r =>
							E_S <= ex_r;
                        when dec_auipc_lui  =>
                           if	(ID_EX_IN(6)='1') then
                             	E_S <= ex_lui;
						   elsif(ID_EX_IN(7)='1') then
                             	E_S <= ex_auipc;
                                       end if;
									   
						when others =>
							E_S <= reset;
					end case;
				end if;
			end if;
		end process EXECUTE_STATE;
		
		EXECUTE_OUT : process(E_S)
		begin		
			case E_S is
				when reset =>
					ex_mux <= '1';
			        lui_auipc_mux  <= "10";
				when ex_i =>
					ex_mux <= '0';   --immediate
				     lui_auipc_mux  <= "11";
				when ex_r =>
					ex_mux <= '1'; --r type
				     lui_auipc_mux  <= "11";
					 
                when ex_lui =>
                    ex_mux <= '1'; 
				     lui_auipc_mux  <= "10";	
                when ex_auipc =>
                    ex_mux <= '1'; 
				     lui_auipc_mux  <= "01";						 
				
				when others => 	
					ex_mux <= '1';
			        lui_auipc_mux  <= "10";
			end case; 	
		end process EXECUTE_OUT;
		

		
		MEMORY_STATE : process(CLOCK, RST)		
		begin
			if RST = '0' then
				M_S <= reset;
			elsif (CLOCK = '1' and CLOCK'EVENT) then 
				if (EX_MEM_en = '1') then
					case E_S is
						when ex_i =>
							if(EX_MEM_IN(2) = '1') then
								M_S <= mem_l;
							elsif(EX_MEM_IN(3) = '1') then
								M_S <= mem_s;   
							elsif(EX_MEM_IN(0) = '1' or EX_MEM_IN(5) = '1' or EX_MEM_IN(6) = '1' or EX_MEM_IN(4) = '1' or EX_MEM_IN(1) = '1' ) then
								M_S <= mem_i_r; 
							else
								M_S <= reset;
							end if;	
						when ex_r | ex_lui | ex_auipc =>
							M_S <= mem_i_r;		
						when others =>
							M_S <= reset;
					end case;
				end if;
			end if;
		end process MEMORY_STATE;
		
		MEMORY_OUT : process(M_S)
		begin
			case M_S is
				when reset =>
					mem_rd_en <= '0';	
					mem_wr_en <= '0';
				when mem_i_r =>
					mem_rd_en <= '0';	
					mem_wr_en <= '0';
				when mem_l =>
					mem_rd_en <= '1';	
					mem_wr_en <= '0';
				when mem_s =>
					mem_rd_en <= '0';	
					mem_wr_en <= '1';
				when others => 		
					mem_rd_en <= '0';	
					mem_wr_en <= '0';
			end case; 	
		end process MEMORY_OUT;
	
    --7 => auipc	
	--6 => lui        
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type
		
		WRITEBACK_STATE : process(CLOCK, RST)		
		begin
			if RST = '0' then
				WB_S <= reset;
			elsif (CLOCK = '1' and CLOCK'EVENT) then 
				if (MEM_WB_en = '1') then
					case M_S is
						when mem_i_r =>
							if 	(MEM_WB_IN(0) = '1') OR (MEM_WB_IN(1) = '1') then 
								WB_S <= wb_imm_r;	
							elsif(MEM_WB_IN(5) = '1')  then
							       WB_S <= wb_jal;
							elsif	(MEM_WB_IN(3) = '1') OR (MEM_WB_IN(4) = '1') then	
								WB_S <= wb_nop;
							else
								WB_S <= reset;
							end if;	
						when mem_l =>
							WB_S <= wb_l;
						when mem_s =>
							WB_S <= wb_nop;
						when others =>
							WB_S <= reset;
					end case;
				end if;
			end if;
		end process WRITEBACK_STATE;
		
		WRITEBACK_OUT : process(WB_S)
		begin			
			case WB_S is
				when reset =>
					wb_data_mux <= '0';
					wb_jal_data_mux <= '1';
					RF_write <= '0';	
				when wb_imm_r =>
					wb_data_mux <='0' ;
					wb_jal_data_mux <= '1';
					RF_write <= '1';
				when wb_jal =>
					wb_data_mux <= '0';
					wb_jal_data_mux <= '0';
					RF_write <= '1';	
				when wb_l =>
					wb_data_mux <='1' ;
					wb_jal_data_mux <= '1';
					RF_write <= '1';
				when wb_nop =>
					wb_data_mux <= '0';
					wb_jal_data_mux <= '0';
					RF_write <= '0';
				when others =>
					wb_data_mux <='0' ;
					wb_jal_data_mux <= '1';
					RF_write <= '0';			
			end case; 	
		end process WRITEBACK_OUT;	

end behav;
