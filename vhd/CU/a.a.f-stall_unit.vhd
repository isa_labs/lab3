library ieee;
use ieee.std_logic_1164.all;

entity stall_unit is
	port(	clk: 					in std_logic;
				rst: 					in std_logic;
				load_ID_EX:   in std_logic; -- instruction signals from the pipeline unit,EX_MEM_IN(2) 
				r_ALU_IF_ID:  in std_logic; -- ID_EX_IN(1)
				load_IF_ID:   in std_logic; -- ID_EX_IN(2)
				store_IF_ID:  in std_logic; -- ID_EX_IN(3)
				i_ALU_IF_ID:  in std_logic; -- ID_EX_IN(0) 
				branch_IR: 		in std_logic; -- IR marks the next instruction register line, not yet pipelined, IF_ID_IN(4)
				jump_IR:   		in std_logic; -- IF_ID_IN(5)

				rd2_ID_EX_eq_rs1_IF_ID:	in std_logic; -- output of comparators 
				rd2_ID_EX_eq_rs2_IF_ID:	in std_logic;
				rd2_ID_EX_eq_rs1_IR:		in std_logic;
				rd2_IF_ID_eq_rs1_IR:   	in std_logic;
				rd3_IF_ID_eq_rs1_IR:		in std_logic;

				PC_enable:    	out std_logic;
				IF_ID_enable: 	out std_logic; -- stalls are implemented forcing some pipeline registers to keep the same value disabling them alltoghether
				ID_EX_enable: 	out std_logic;
				EX_MEM_enable: 	out std_logic;
				MEM_WB_enable: 	out std_logic);
				

end stall_unit;

-- bits showing the type of instruction to be propagated in the pipeline:
	--7 => auipc
	--6 => lui     
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type
	
architecture behav of stall_unit is

	type state is (reset, no_stall, loadEX_bjIF, bjIF_stall_0, bjIF_stall_1, loadEX_aluID, aluID_bjIF, loadEX_aluID_bjIF, loadEX_loadID_bjIF);

	signal stall_cs, stall_ns: state;

	signal brj_IF, load_EX_alu_ID, load_EX_brj_IF, load_ID_brj_IF, alu_st_ID_brj_IF, load_EX_alu_ID_brj_IF, load_EX_load_ID_brj_IF: std_logic;

begin

	brj_IF <= branch_IR or jump_IR;-- taking the signals exiting from InstrMem which show a branch or a jump
	load_EX_alu_ID <= load_ID_EX and ((r_ALU_IF_ID and (rd2_ID_EX_eq_rs1_IF_ID or rd2_ID_EX_eq_rs2_IF_ID)) or ((i_ALU_IF_ID or store_IF_ID or load_IF_ID) and rd2_ID_EX_eq_rs1_IF_ID));
	load_EX_brj_IF <= load_ID_EX and brj_IF and rd2_ID_EX_eq_rs1_IR;
	load_ID_brj_IF <= load_IF_ID and brj_IF and rd2_IF_ID_eq_rs1_IR;
	alu_st_ID_brj_IF <= brj_IF and ((r_ALU_IF_ID and rd3_IF_ID_eq_rs1_IR) or ((i_ALU_IF_ID or store_IF_ID) and rd2_IF_ID_eq_rs1_IR));
	load_EX_alu_ID_brj_IF <= load_EX_alu_ID and alu_st_ID_brj_IF; --the signal that is set when load is in execute,an alu op in decode and a branch or jump in fettch stage
	load_EX_load_ID_brj_IF <= load_EX_alu_ID and load_ID_brj_IF; 	
	
	UPDATE_STATE: process(clk, rst)
	begin
		if rst = '0' then

			stall_cs <= reset;
			
		elsif rising_edge(clk) then --updating the state of the fsm

			stall_cs <= stall_ns;
			
		end if;
	end process UPDATE_STATE;
	

	
	NEXT_STATE: process(stall_cs, brj_IF, load_EX_alu_ID, load_EX_brj_IF, load_ID_brj_IF, alu_st_ID_brj_IF, load_EX_alu_ID_brj_IF, load_EX_load_ID_brj_IF)
	begin
		case stall_cs is
			when reset =>	
				stall_ns <= no_stall;

			when no_stall => 
				if load_EX_alu_ID_brj_IF = '1' then
					stall_ns <= loadEX_aluID_bjIF;
				elsif load_EX_load_ID_brj_IF = '1' then
					stall_ns <= loadEX_loadID_bjIF;
				elsif load_EX_alu_ID = '1' then
					stall_ns <= loadEX_aluID;
				elsif load_EX_brj_IF = '1' then
					stall_ns <= bjIF_stall_1;
				elsif alu_st_ID_brj_IF = '1' then
					stall_ns <= bjIF_stall_1;
				elsif load_ID_brj_IF = '1' then
					stall_ns <= bjIF_stall_0;
				else
					stall_ns <= no_stall;
				end if;

			when loadEX_aluID_bjIF => 
				stall_ns <= bjIF_stall_1;

			when loadEX_loadID_bjIF => 
				stall_ns <= bjIF_stall_0;

			when loadEX_aluID => 
				stall_ns <= no_stall;

			when bjIF_stall_0 => 
				stall_ns <= bjIF_stall_1;

			when bjIF_stall_1 =>  
				stall_ns <= no_stall;
			
			when others => 
				stall_ns <= reset;

		end case;

	end process NEXT_STATE;

	OUTPUT: process(stall_cs)
	begin
			--signals at the output
		case stall_cs is
			when reset => 
				PC_enable <= '0';
				IF_ID_enable <= '0';
				ID_EX_enable <= '0';
				EX_MEM_enable <= '0';
				MEM_WB_enable <= '0';
			when no_stall => 
				PC_enable <= '1';
				IF_ID_enable <= '1';
				ID_EX_enable <= '1';
				EX_MEM_enable <= '1';
				MEM_WB_enable <= '1';
			when loadEX_aluID => 
				PC_enable <= '0';
				IF_ID_enable <= '0';
				ID_EX_enable <= '0';
				EX_MEM_enable <= '0';
				MEM_WB_enable <= '1';
			when loadEX_aluID_bjIF => 
				PC_enable <= '0';
				IF_ID_enable <= '0';
				ID_EX_enable <= '0';
				EX_MEM_enable <= '0';
				MEM_WB_enable <= '1';
			when loadEX_loadID_bjIF => 
				PC_enable <= '0';
				IF_ID_enable <= '0';
				ID_EX_enable <= '0';
				EX_MEM_enable <= '0';
				MEM_WB_enable <= '1';
			when bjIF_stall_0 => 
				PC_enable <= '0';
				IF_ID_enable <= '0';
				ID_EX_enable <= '0';
				EX_MEM_enable <= '1';
				MEM_WB_enable <= '1';
			when bjIF_stall_1 => 
				PC_enable <= '0';
				IF_ID_enable <= '0';
				ID_EX_enable <= '1';
				EX_MEM_enable <= '1';
				MEM_WB_enable <= '1';
			when others => 
				PC_enable <= '1';
				IF_ID_enable <= '1';
				ID_EX_enable <= '1';
				EX_MEM_enable <= '1';
				MEM_WB_enable <= '1';
		end case;

	end process OUTPUT;
				
end behav;