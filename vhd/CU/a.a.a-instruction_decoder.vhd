library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.myTypes.all;

entity InstructionDec is 
	generic(OP_CODE_SIZE : integer := 7;  -- Op Code Size
					InstrType_size : integer := 8);
	port (  instruction_opcode : in std_logic_vector( OP_CODE_SIZE-1 downto 0); --exiting from the output of the InstrMEM
				instruction_type : out std_logic_vector(InstrType_size-1 downto 0)); --type of instruction to be checked
end InstructionDec;

architecture behavioral of InstructionDec is
	signal alu_imm : std_logic; --ALU immediate 
	signal r_type : std_logic;
	signal load : std_logic; --load
	signal store : std_logic; --store
	signal branch : std_logic; --beq,bne
	signal jump_link : std_logic; --jal, jalr
	signal lui : std_logic; 
	signal auipc : std_logic; 
	

begin
	-- bits showing the type of instruction to be propagated in the pipeline:
	
	--7 => auipc
	--6 => lui     
	--5 => jump_link		
	--4 => branch				
	--3 => store			
	--2 => load				
	--1 => r_type
	--0 => alu_imm		--i type
	
														
	instruction_type <= auipc & lui & jump_link & branch  & store  & load  & r_type & alu_imm;

	InstructionDecoder_process : process (instruction_opcode)
	begin
		if (instruction_opcode = xor_rtype) OR 
            (instruction_opcode = slt_rtype) OR
            (instruction_opcode = add_rtype) OR	
			(instruction_opcode = sub_rtype) THEN 
			
			alu_imm <= '0';
			r_type <= '1';
			load <= '0';
			store <= '0';
			branch <= '0';
			jump_link <= '0';
			lui <= '0';
			auipc <='0';

		elsif	(instruction_opcode = srai_itype) OR
					(instruction_opcode = andi_itype) OR
                    (instruction_opcode = addi_itype) then
                    
			alu_imm <= '1';
			r_type<= '0';
			load <= '0';
			store <= '0';
			branch <= '0';
			jump_link <= '0';
			lui <= '0';
			auipc <='0';
			
		elsif(instruction_opcode = auipc_itype) THEN
		    alu_imm <= '0';
			r_type<= '0';
			load <= '0';
			store <= '0';
			branch <= '0';
			jump_link <= '0';
		    lui <= '0';
			auipc <='1';
	
           	elsif(instruction_opcode = lui_itype)  THEN 
					alu_imm <= '0';
			        r_type<= '0';
			        load <= '0';
			        store <= '0';
			        branch <= '0';
			        jump_link <= '0';
			        lui <= '1';
			        auipc <='0';
			
		elsif (instruction_opcode = lw_load) then
			
			alu_imm <= '0';
			r_type <= '0';
			load <= '1';
			store <= '0';
			branch <= '0';
			jump_link <= '0';
			lui <= '0';
			auipc <='0';
										
		elsif (instruction_opcode = sw_store) then
		
			alu_imm <= '0';
			r_type <= '0';
			load <= '0';
			store <= '1';
			branch <= '0';
			jump_link <= '0';
			lui <= '0';
			auipc <='0';
											
		elsif	(instruction_opcode = beq_branch) then
			
			alu_imm <= '0';
			r_type <= '0';
			load <= '0';
			store <= '0';
			branch <= '1';
			jump_link <= '0';
			lui <= '0';
			auipc <='0';
			
										 
		elsif	(instruction_opcode = jal_jumpandlink) then
			
			alu_imm <= '0';
			r_type <= '0';
			load <= '0';
			store <= '0';
			branch<= '0';
			jump_link <= '1';
			lui <= '0';
			auipc <='0';
										
		else
			alu_imm <= '0';
			r_type <= '0';
			load <= '0';
			store <= '0';
			branch <= '0';
			jump_link <= '0';
			lui <= '0';
			auipc <='0';
								
		end if;			
	end process InstructionDecoder_Process;
end behavioral;